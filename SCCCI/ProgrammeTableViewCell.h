//
//  ProgrammeTableViewCell.h
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ProgrammeTableViewCellDelegate;
@interface ProgrammeTableViewCell : UITableViewCell
@property id<ProgrammeTableViewCellDelegate> owner;
- (void) setUpTheViews;
- (void)loadTheViewWithName:(NSString *)strName andIsActive:(BOOL) isActive andSection:(NSInteger)section andLocation:(NSString *)strLocation andTime:(NSString *)strTime andLanguage:(NSString *)strLanguage;
@end

@protocol ProgrammeTableViewCellDelegate
- (void) onProgrammeTableViewCellFav:(ProgrammeTableViewCell *)cell andSection:(NSInteger)section andFav:(BOOL  )fav;
@end

