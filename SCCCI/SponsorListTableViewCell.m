//
//  AboutTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SponsorListTableViewCell.h"
#import "StringTable.h"
#import "UIImageView+WebCache.h"
@interface SponsorListTableViewCell()
@property (nonatomic,strong) UIImageView * imgCircleView;
@property (nonatomic,strong) UILabel * lblName;
@property (nonatomic,strong) UILabel * lblDescription;

@end
@implementation SponsorListTableViewCell

- (UIImageView *) imgCircleView{
    if (!_imgCircleView) {
        _imgCircleView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 10, 37, 37)];
        _imgCircleView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imgCircleView;
}

- (UILabel *)lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(self.imgCircleView.frame.origin.x + self.imgCircleView.frame.size.width + 10, 10, 230, 20)];
        _lblName.numberOfLines = 2;
        _lblName.font = [UIFont fontWithName:ROBOTO_BOLD size:15];
        _lblName.textColor = [UIColor colorWithHexString:@"01457b"];
        //_lblName.textInputMode = NSText
    }
    return _lblName;
}


- (UILabel *) lblDescription{
    if (!_lblDescription) {
        _lblDescription = [[UILabel alloc] initWithFrame:CGRectMake(self.imgCircleView.frame.origin.x + self.imgCircleView.frame.size.width + 10, self.lblName.frame.origin.y + self.lblName.frame.size.height, 320 -  self.imgCircleView.frame.origin.x + self.imgCircleView.frame.size.width + 20, 18)];
        _lblDescription.font = [UIFont fontWithName:ROBOTO_REGUALAR size:12];
        _lblDescription.textColor = [UIColor colorWithHexString:@"333333"];
    }
    return _lblDescription;
}

- (void) setUpViews{
    [self addSubview:self.imgCircleView];
    [self addSubview:self.lblName];
    [self addSubview:self.lblDescription];
}

- (void) viewLoadWithName:(NSString *)strName andDescription:(NSString *)strDescription andImageName:(NSString *)strImage{
    NSLog(@"str sponsor image link %@",strImage);
    [self.imgCircleView setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:nil];
    self.lblName.text = strName;
    [self setDescription:strDescription];
}

- (void)setDescription:(NSString *)str{
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:@"booth_point"];
    
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    NSAttributedString *strDescrip = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@",str]];
    
    NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithAttributedString:attachmentString];
    [myString appendAttributedString:strDescrip];
    
    self.lblDescription.attributedText = myString;
}

@end
