//
//  SessionPollViewController.m
//  SCCCI
//
//  Created by Zayar on 6/26/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SessionPollViewController.h"
#import "Utility.h"
#import "StringTable.h"
#import "SessionPollOptionTableViewCell.h"
@interface SessionPollViewController ()
@property (nonatomic,strong) UILabel * lblTitle;
@property (strong, nonatomic) NSArray * arrQuestionTitles;
@property (strong, nonatomic) NSArray * arrOptionTitles;
@property (strong, nonatomic) UIButton * btnSubmit;
@end

@implementation SessionPollViewController
@synthesize strSessionName;
- (UILabel *)lblTitle{
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.topBannerSilder.frame.origin.y + self.topBannerSilder.frame.size.height, self.view.frame.size.width, 44)];
        _lblTitle.backgroundColor = [UIColor colorWithHexString:@"ebeaf0"];
        _lblTitle.textAlignment = NSTextAlignmentCenter;
        _lblTitle.font = [UIFont fontWithName:ROBOTO_BOLD size:15];
        _lblTitle.textColor = [UIColor colorWithHexString:@"01457b"];
        [Utility makeBorder:_lblTitle andWidth:0.5 andColor:[UIColor colorWithHexString:@"bfbfbf"] andCornerRadius:0];
    }
    return _lblTitle;
}

- (UIButton *)btnSubmit{
    if (!_btnSubmit) {
        _btnSubmit = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnSubmit.frame = CGRectMake(42,self.tbl.frame.origin.y + self.tbl.frame.size.height + 15, 233, 35);
        _btnSubmit.backgroundColor = [UIColor colorWithHexString:@"01457b"];
        [_btnSubmit setTitle:@"Submit" forState:UIControlStateNormal];
        [Utility makeCornerRadius:_btnSubmit andRadius:5];
        [_btnSubmit addTarget:self action:@selector(onSubmit:) forControlEvents:UIControlEventTouchUpInside];
        _btnSubmit.titleLabel.font = [UIFont fontWithName:ROBOTO_LIGHT size:15];
    }
    return _btnSubmit;
}

- (NSArray *) arrQuestionTitles{
    if (!_arrQuestionTitles) {
        _arrQuestionTitles = [NSArray arrayWithObjects: @"Q1 : Question text question text question text", @"Q2 : Question text question text question text",nil];
    }
    return _arrQuestionTitles;
}

- (NSArray *) arrOptionTitles{
    if (!_arrOptionTitles) {
        _arrOptionTitles = [NSArray arrayWithObjects: @"Option 1", @"Option 2", @"Option 3", @"Option 4", nil];
    }
    return _arrOptionTitles;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.lblTitle];
    CGRect tblFrame = self.tbl.frame;
    tblFrame.origin.y += self.lblTitle.frame.size.height;
    tblFrame.size.height -= self.lblTitle.frame.size.height + 60;
    [self.tbl setFrame:tblFrame];
    [self.view addSubview:self.btnSubmit];
    
    [self setNavBarTitle:@"Session Poll"];
    
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
    [self.tbl reloadData];
    
    self.tbl.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)viewWillAppear:(BOOL)animated{
    self.lblTitle.text = strSessionName;
}

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ProgrammeTableViewCell";
	SessionPollOptionTableViewCell *cell = (SessionPollOptionTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[SessionPollOptionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    
    [cell loadWithName:[self.arrOptionTitles objectAtIndex:indexPath.row] andCheck:indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 32;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrOptionTitles count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return [self.arrQuestionTitles count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSLog(@"viewForHeaderInSection");
    NSString * strTitle;
    strTitle=@"";
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
    [v setBackgroundColor:[UIColor colorWithHexString:@"ebeaf0"]];
    UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-20, 35)];
    lblTitle.font = [UIFont fontWithName:ROBOTO_BOLD size:14];
    lblTitle.textColor = [UIColor colorWithHexString:@"2b2b2b"];
    lblTitle.text = [self.arrQuestionTitles objectAtIndex:section];
    [v addSubview:lblTitle];
    [Utility makeBorder:v andWidth:0.5 andColor:[UIColor colorWithHexString:@"bfbfbf"] andCornerRadius:0];
    
    return v;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 35;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //self.programDetailViewController.strTitle = [self.arrSessionTitles objectAtIndex:indexPath.row];
    //[self.navigationController pushViewController:self.programDetailViewController animated:YES];
}

@end
