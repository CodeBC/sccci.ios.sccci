//
//  BasedViewController.h
//  SCCCI
//
//  Created by Zayar on 6/13/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYCustomScrollSliderView.h"
@interface BasedViewController : UIViewController
- (void)setNavBarTitle:(NSString *)str;
@property (nonatomic,strong) NSArray * arrBannerSilder;// image of top banner
@property (nonatomic,strong) NSArray * arrBannerUrl;// url of top banner
@property (nonatomic,strong) ZYCustomScrollSliderView * topBannerSilder;
- (void) hideBasedTopBanner;
- (NSInteger) getCurrentLanguage;
@end
