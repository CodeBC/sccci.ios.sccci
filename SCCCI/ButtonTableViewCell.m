//
//  ButtonTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 7/21/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ButtonTableViewCell.h"
#import "StringTable.h"
@interface ButtonTableViewCell()
@property (nonatomic, strong) UIButton * btnSync;
@end
@implementation ButtonTableViewCell
@synthesize owner;
- (UIButton *) btnSync{
    if (!_btnSync) {
        _btnSync = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnSync.frame = CGRectMake(LEFT_MARGIN, 5, NORMAL_WIDTH, 30);
        
        _btnSync.titleLabel.font = [UIFont fontWithName:ROBOTO_BOLD size:16];
        _btnSync.titleLabel.textColor = [UIColor colorWithHexString:@"02457c"];
        [_btnSync addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _btnSync;
}

- (void) viewSetup{
    [self addSubview:self.btnSync];
    [self bringSubviewToFront:self.btnSync];
    [self.btnSync setTitle:@"Force Sync" forState:UIControlStateNormal];
    NSMutableAttributedString * title = [[NSMutableAttributedString alloc] initWithString:self.btnSync.currentTitle];
    [title addAttributes:@{ NSStrokeWidthAttributeName:@0,NSStrokeColorAttributeName:self.btnSync.tintColor } range:NSMakeRange(0, self.btnSync.currentTitle.length)];
    [self.btnSync setAttributedTitle:title forState:UIControlStateNormal];
}

- (void)onClick:(UIButton *)sender{
    [owner onForceSyncClick:sender];
}

@end
