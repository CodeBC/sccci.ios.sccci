//
//  OrganiserDetailTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "OrganiserDetailTableViewCell.h"
#import "StringTable.h"
#import "UIImageView+WebCache.h"
@interface OrganiserDetailTableViewCell()
@property (nonatomic, strong) UIImageView * imgLogoView;
@property (nonatomic,strong) UILabel * lblContactNo;
@property (nonatomic,strong) UILabel * lblContactNo2;
@property (nonatomic,strong) UILabel * lblContactNo3;
@end

@implementation OrganiserDetailTableViewCell
- (UIImageView *) imgLogoView{
    if (!_imgLogoView) {
        _imgLogoView = [[UIImageView alloc] initWithFrame:CGRectMake(320/2 - 180/2, 2, 180, 64)];
        _imgLogoView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imgLogoView;
}

- (UILabel *)lblContactNo{
    if (!_lblContactNo) {
        _lblContactNo = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.imgLogoView.frame.origin.y + self.imgLogoView.frame.size.height, 290, 23)];
        _lblContactNo.font = [UIFont fontWithName:ROBOTO_REGUALAR size:13];
        _lblContactNo.textColor = [UIColor colorWithHexString:@"333333"];
    }
    return _lblContactNo;
}

- (UILabel *)lblContactNo2{
    if (!_lblContactNo2) {
        _lblContactNo2 = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.lblContactNo.frame.origin.y + self.lblContactNo.frame.size.height, 290, 23)];
        _lblContactNo2.font = [UIFont fontWithName:ROBOTO_REGUALAR size:13];
        _lblContactNo2.textColor = [UIColor colorWithHexString:@"333333"];
    }
    return _lblContactNo2;
}

- (UILabel *)lblContactNo3{
    if (!_lblContactNo3) {
        _lblContactNo3 = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.lblContactNo2.frame.origin.y + self.lblContactNo2.frame.size.height, 290, 23)];
        _lblContactNo3.font = [UIFont fontWithName:ROBOTO_REGUALAR size:13];
        _lblContactNo3.textColor = [UIColor colorWithHexString:@"333333"];
    }
    return _lblContactNo3;
}

- (NSString *)setPhoneContent:(NSString *)str{
    return [NSString stringWithFormat:@"Phone :%@",str];
}

- (NSString *)setEmailContent:(NSString *)str{
    return [NSString stringWithFormat:@"Email :%@",str];
}

- (NSString *)setAddressContent:(NSString *)str{
    return [NSString stringWithFormat:@"Address :%@",str];
}

- (void)setUpViews{
    [self addSubview:self.imgLogoView];
    [self addSubview:self.lblContactNo];
    [self addSubview:self.lblContactNo2];
    [self addSubview:self.lblContactNo3];
}

- (void)loadWithContact:(NSString *)strContactNo andContactNo2:(NSString *)strContactNo2 andContactNo3:(NSString *)strContactNo3 andLogo:(NSString *)strLogo{
    [self.imgLogoView setImageWithURL:[NSURL URLWithString:strLogo] placeholderImage:[UIImage imageNamed:@"Logo_Sample"]];
    self.lblContactNo.text = [self setPhoneContent:strContactNo];
    self.lblContactNo2.text = [self setEmailContent:strContactNo2];
    self.lblContactNo3.text = [self setAddressContent:strContactNo3];
}
@end
