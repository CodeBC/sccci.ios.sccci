//
//  ProgramSpeakerTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ProgramSpeakerTableViewCell.h"
#import "StringTable.h"
@interface ProgramSpeakerTableViewCell()
@property (nonatomic,strong) UILabel * lblName;
@property (nonatomic,strong) UILabel * lblJob;
@end
@implementation ProgramSpeakerTableViewCell
- (UILabel *)lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 10, 290, 20)];
        _lblName.font = [UIFont fontWithName:ROBOTO_LIGHT size:13];
        _lblName.textColor = [UIColor colorWithHexString:@"333333"];
        _lblName.numberOfLines = 0;
    }
    return _lblName;
}

- (UILabel *)lblJob{
    if (!_lblJob) {
        _lblJob = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.lblName.frame.origin.y + self.lblName.frame.size.height, 290, 20)];
        _lblJob.font = [UIFont fontWithName:ROBOTO_LIGHT size:11];
        _lblJob.textColor = [UIColor colorWithHexString:@"7a7a7a"];
        _lblJob.numberOfLines = 0;
    }
    return _lblJob;
}

- (void) setUpView{
    [self addSubview:self.lblName];
    [self addSubview:self.lblJob];
}

- (void) viewLoadWithDescription:(NSString *)strName andJob:(NSString *)strJob{
    self.lblName.text = strName;
    self.lblJob.text = strJob;
}

@end
