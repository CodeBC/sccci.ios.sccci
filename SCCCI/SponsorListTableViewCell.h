//
//  AboutTableViewCell.h
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SponsorListTableViewCell: UITableViewCell
- (void) setUpViews;
- (void) viewLoadWithName:(NSString *)strName andDescription:(NSString *)strDescription andImageName:(NSString *)strImage;
@end
