//
//  SpeakerViewController.m
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SpeakerViewController.h"
#import "SpeakerTableViewCell.h"
#import "SpeakerDetailViewController.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "ServiceMappingClient.h"
#import "Speakers.h"
#import "StringTable.h"
@interface SpeakerViewController ()
@property (strong, nonatomic) NSArray * arrSpeakers;
@property (strong, nonatomic) SpeakerDetailViewController * speakerDetailViewController;
@end

@implementation SpeakerViewController
@synthesize strTitle;
- (SpeakerDetailViewController *) speakerDetailViewController{
    if (!_speakerDetailViewController) {
        //_speakerDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SpeakerDetail"];
        _speakerDetailViewController = [[SpeakerDetailViewController alloc] init];
    }
    return _speakerDetailViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
    
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([self.tbl respondsToSelector:@selector(separatorInset)]) {
            [self.tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [self setNavBarTitle:strTitle];
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    self.arrSpeakers = [client getSpeakers];
    [self.tbl reloadData];
}

/*- (NSArray *) arrSpeakerNames{
    //Scott Smith Ki-Ho Park Mimiwati
    if (!_arrSpeakerNames) {
        _arrSpeakerNames = [NSArray arrayWithObjects:@"Scott Smith",@"Ki-Ho Park",@"Mimiwati", nil];
    }
    return _arrSpeakerNames;
}*/

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"aboutCell";
	SpeakerTableViewCell *cell = (SpeakerTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[SpeakerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    Speakers * speaker = [self.arrSpeakers   objectAtIndex:indexPath.row];
    if ([self getCurrentLanguage] == LANGAUGE_EGN) {
        [cell viewLoadWithString:speaker.speaker_name];
    }
    else if ([self getCurrentLanguage] == LANGAUGE_CN){
        [cell viewLoadWithString:speaker.speaker_name_cn];
    }
    
    UIImageView * indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
    indicatorView.image = [UIImage imageNamed:@"cell_indicator"];
    cell.accessoryView = indicatorView;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrSpeakers count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.speakerDetailViewController.speaker = [self.arrSpeakers objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:self.speakerDetailViewController animated:YES];
}

@end
