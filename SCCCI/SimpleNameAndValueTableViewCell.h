//
//  SimpleNameAndValueTableViewCell.h
//  SCCCI
//
//  Created by Zayar on 7/7/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleNameAndValueTableViewCell : UITableViewCell
- (void)setUpViews;
- (void)viewLoadWithString:(NSString *)strName andValue:(NSString *)strValue;
@end
