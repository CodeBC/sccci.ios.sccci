//
//  Speakers.m
//  SCCCI
//
//  Created by Zayar on 6/27/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Speakers.h"


@implementation Speakers

#pragma mark - JSON serialization
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"speaker_id" : @"speaker_id",
             @"speaker_avatar" : @"speaker_avatar",
             @"speaker_company" : @"speaker_company",
             @"speaker_company_cn" : @"speaker_company_cn",
             @"speaker_description" : @"speaker_description",
             @"speaker_designation" : @"speaker_designation",
             @"speaker_designation_cn" : @"speaker_designation_cn",
             @"speaker_name" : @"speaker_name",
             @"speaker_name_cn" : @"speaker_name_cn",
             @"speaker_description_cn" : @"speaker_description_cn"
             };
}

#pragma mark - Managed object serialization
+ (NSString *)managedObjectEntityName {
    return @"Speakers";
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    return @{
             @"speaker_id" : @"speaker_id",
             @"speaker_avatar" : @"speaker_avatar",
             @"speaker_company" : @"speaker_company",
             @"speaker_description" : @"speaker_description",
             @"speaker_designation" : @"speaker_designation",
             @"speaker_name" : @"speaker_name",
             @"speaker_name_cn" : @"speaker_name_cn",
             @"speaker_description_cn" : @"speaker_description_cn",
             @"speaker_designation_cn" : @"speaker_designation_cn",
             @"speaker_company_cn" : @"speaker_company_cn"
             };
}

+ (NSSet *)propertyKeysForManagedObjectUniquing {
    return [NSSet setWithObject:@"speaker_id"];
}

+ (void) deleteAllWithContext: (NSManagedObjectContext *) context {
    NSFetchRequest * fectchRequest = [[NSFetchRequest alloc] init];
    [fectchRequest setEntity:[NSEntityDescription entityForName:@"Speakers" inManagedObjectContext:context]];
    [fectchRequest setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * objArr = [context executeFetchRequest:fectchRequest error:&error];
    //error handling goes here
    for (NSManagedObject * managedObj in objArr) {
        [context deleteObject:managedObj];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //more error handling here
}
@end
