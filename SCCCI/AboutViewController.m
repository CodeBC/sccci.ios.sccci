//
//  AboutViewController.m
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "AboutViewController.h"
#import "AboutTableViewCell.h"
#import "AboutDetailViewController.h"
#import "Utility.h"
#import "OrganiserDetailViewController.h"
#import "Info.h"
#import "AppDelegate.h"
#import "ServiceMappingClient.h"
#import "StringTable.h"
@interface AboutViewController ()
@property (nonatomic,strong) NSArray * arrTitles;// title of about list
@property (nonatomic,strong) UIImageView * cellIndicatorImageView;
@property (nonatomic,strong) AboutDetailViewController * detailViewController;
@property (strong, nonatomic) OrganiserDetailViewController * organiserDetailViewController;
@end

@implementation AboutViewController
@synthesize strTitle;
- (OrganiserDetailViewController *) organiserDetailViewController{
    if (!_organiserDetailViewController) {
        _organiserDetailViewController = [[OrganiserDetailViewController alloc] init];
    }
    return _organiserDetailViewController;
}

- (AboutDetailViewController *) detailViewController{
    if (!_detailViewController) {
        _detailViewController = [[AboutDetailViewController alloc] init];
    }
    return _detailViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:strTitle];
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
    [self.tbl reloadData];
    UIView * v = [[UIView alloc] init];
    self.tbl.tableFooterView = v;
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([self.tbl respondsToSelector:@selector(separatorInset)]) {
            [self.tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [self.topBannerSilder sliderPlay];
    
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    self.arrTitles = [client getAbouts];
    [self.tbl reloadData];
}

- (UIImageView *)cellIndicatorImageView{
    if (!_cellIndicatorImageView) {
        _cellIndicatorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
        _cellIndicatorImageView.image = [UIImage imageNamed:@"cell_indicator"];
    }
    return _cellIndicatorImageView;
}



#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"aboutCell";
	AboutTableViewCell *cell = (AboutTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[AboutTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    Info * info = [self.arrTitles   objectAtIndex:indexPath.row];
    NSInteger lang = [StringTable getCurrentLangage];
    if (lang == LANGAUGE_EGN)
        [cell viewLoadWithString:info.title];
    else if (lang == LANGAUGE_CN)
        [cell viewLoadWithString:info.title_cn];
    
    UIImageView * indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
    indicatorView.image = [UIImage imageNamed:@"cell_indicator"];
    cell.accessoryView = indicatorView;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrTitles count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*if (indexPath.row == 3) {
        [self.navigationController pushViewController:self.organiserDetailViewController animated:YES];
    }
    else if(indexPath.row == 4){
        [self.navigationController pushViewController:self.organiserDetailViewController animated:YES];
    }
    else if(indexPath.row == 5){
        [self.navigationController pushViewController:self.organiserDetailViewController animated:YES];
    }
    else{
        self.detailViewController.strTitle = [self.arrTitles objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:self.detailViewController animated:YES];
    }*/
    self.detailViewController.info = [self.arrTitles objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:self.detailViewController animated:YES];
}

@end
