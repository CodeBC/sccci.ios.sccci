//
//  OrganiserDetailViewController.h
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sponsor.h"
@interface OrganiserDetailViewController : BasedWithTableViewController
@property (nonatomic,strong)  Sponsor * sponsor;

@end
