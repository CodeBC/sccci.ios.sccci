//
//  AboutTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SpeakerTableViewCell.h"
#import "StringTable.h"
@interface SpeakerTableViewCell()
@property (nonatomic, strong) UILabel * lblName;
@end
@implementation SpeakerTableViewCell
- (UILabel *)lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 5, 270, 45)];
        _lblName.numberOfLines = 2;
        _lblName.font = [UIFont fontWithName:ROBOTO_BOLD size:16];
        _lblName.textColor = [UIColor colorWithHexString:@"01457b"];
        //_lblName.textInputMode = NSText
    }
    return _lblName;
}

- (void)setUpViews{
    [self addSubview:self.lblName];
}

- (void)viewLoadWithString:(NSString *)strName{
    [self.lblName setText:strName];
}
@end
