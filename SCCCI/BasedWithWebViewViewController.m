//
//  BasedWithWebViewViewController.m
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "BasedWithWebViewViewController.h"
#import "StringTable.h"

@interface BasedWithWebViewViewController ()

@end

@implementation BasedWithWebViewViewController
- (UIWebView *)webView{
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.topBannerSilder.frame.origin.y + self.topBannerSilder.frame.size.height, self.view.frame.size.width, self.view.frame.size.height -(self.topBannerSilder.frame.origin.y + self.topBannerSilder.frame.size.height) - STATUS_BAR_NAV_BAR_HEIGHT)];
    }
    return _webView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.webView];
}

- (void) hideTopBanner{
    [self.webView setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height - STATUS_BAR_NAV_BAR_HEIGHT)];
    self.topBannerSilder.hidden = YES;
    [self hideBasedTopBanner];
}


@end
