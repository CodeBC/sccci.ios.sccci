//
//  LuckyDrawWebViewController.m
//  SCCCI
//
//  Created by Zayar on 6/26/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "LuckyDrawWebViewController.h"
#import "StringTable.h"
@interface LuckyDrawWebViewController ()

@end

@implementation LuckyDrawWebViewController
@synthesize strTitle;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.webView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated{
    [self setNavBarTitle:strTitle];
    NSURLRequest * urlRequest;
    NSInteger lang = [StringTable getCurrentLangage];
    NSString *key = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    if (lang == LANGAUGE_EGN) {
        urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",APP_BASED_LINK,LUCKYDRAW_LINK, key]]];
    }
    else if(lang == LANGAUGE_CN)
        urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",APP_BASED_LINK,LUCKYDRAW_CN_LINK, key]]];
    NSLog(@"%@", [NSString stringWithFormat:@"%@%@%@",APP_BASED_LINK,LUCKYDRAW_LINK, key]);
    [self.webView loadRequest:urlRequest];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [SVProgressHUD showWithStatus:@"Trying"];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismiss];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
}

@end
