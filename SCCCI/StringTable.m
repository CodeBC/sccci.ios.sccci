//
//  StringTable.m
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import "StringTable.h"


@implementation StringTable
NSString * const APP_DB = @"sccci.sqlite";
NSString * const APP_TITLE = @"SCCCI";
NSString * const APP_ID = @"---";
//NSString * const APP_SHARE_LINK = @"http://sccci.herokuapp.com/";
//NSString * const APP_BASED_LINK = @"http://176.56.237.217/git/sccci/";
NSString * const APP_BASED_LINK = @"http://sccci.herokuapp.com/";
NSString * const SPEAKERS_LINK = @"api/speakers";
NSString * const SPONSORS_LINK = @"api/ios/sponsors";
NSString * const EXHIBITOR_LINK = @"api/exhibitors";
NSString * const PROGRAMME_LINK = @"api/ios/programs";
NSString * const LUCKYDRAW_LINK = @"draws/show?token=";
NSString * const POLL_LINK = @"polls";
NSString * const SURVEY_LINK = @"ict_surveys/en";
NSString * const INFO_LINK = @"/api/infos";
NSString * const ABOUT_LINK = @"/api/about";
NSString * const PROMOTION_LINK = @"/api/promotions";
NSString * const NEWS_LINK = @"/api/news";
NSString * const PARTNER_CLINIC_LINK = @"/api/partners";
NSString * const GOV_LINK = @"/api/gov";

NSString * const LUCKYDRAW_CN_LINK = @"draws/show?token=";
NSString * const POLL_CN_LINK = @"polls";
NSString * const SURVEY_CN_LINK = @"ict_surveys/cn";

NSString * const SHARE_LINK = @"http://english.sccci.org.sg/index.cfm?GPID=2182";
NSString * const SHARE_MSG = @"I have signed up for my FREE pass tp the 16th SMEs Annual Conference and the 17th Infocomm Commerce Conference to learn more from successful businesspeople and industry experts, find new ways to improve productivity in my business, and source for various business solutions. Join me!";

NSString * const ROBOTO_LIGHT= @"Roboto-Light";
NSString * const ROBOTO_REGUALAR= @"Roboto-Regular";
NSString * const ROBOTO_BOLD= @"Roboto-Bold";

NSString * const SPONSOR_TYPE_PLATINUM= @"platinum";
NSString * const SPONSOR_TYPE_GOLD= @"gold";

CGFloat const LEFT_MARGIN = 15.0f;
CGFloat const NORMAL_WIDTH = 290.0f;

CGFloat const TABLE_SECTION_HEIGHT = 35.0f;
CGFloat const STATUS_BAR_NAV_BAR_HEIGHT = 64.0f;

NSInteger const LANGAUGE_EGN=1;
NSInteger const LANGAUGE_CN=2;

+ (NSInteger) getCurrentLangage{
    NSUserDefaults * pref = [NSUserDefaults standardUserDefaults];
    NSInteger i = [pref integerForKey:@"language"];
    return i;
}

+ (void) setCurrentLanguage:(NSInteger)lang{
    NSUserDefaults * pref = [NSUserDefaults standardUserDefaults];
    [pref setInteger:lang forKey:@"language"];
    [pref synchronize];
}

@end
