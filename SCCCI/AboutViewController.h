//
//  AboutViewController.h
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : BasedWithTableViewController
@property (nonatomic,strong) NSString * strTitle;
@end
