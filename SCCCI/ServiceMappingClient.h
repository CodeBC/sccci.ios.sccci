//
//  ServiceClient.h
//  SCCCI
//
//  Created by Zayar on 6/27/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OVCClient.h"
#import "Programme.h"
#import "ProgrameDayIds.h"
#import "Speakers.h" 
#import "Partner.h"
#import "Exhibitor.h"
@interface ServiceMappingClient : OVCClient
- (void) syncDataWithCompletionBlock:(void (^)(BOOL status)) block;
- (NSArray *) getSpeakers;
- (NSArray *) getSponsors;
- (NSArray *) getSponsorTypes;
- (NSArray *) getSponsorsByTypeId:(NSNumber *)type_id;
- (NSArray *) getDayIds;
- (NSArray *) getProgrammeByDayId:(NSNumber *)day_id;
- (NSArray *) getSpeakersById:(NSNumber *)idx;
- (NSArray *) getProgrammeTime;
- (NSArray *) getProgrammeByTime:(NSString *)programme_time andDay_id:(NSNumber *)day_id;
- (NSArray *) getProgrammes;
- (Speakers *) getSpeakerById:(NSNumber *)idx;
- (NSArray *)getExhibitionTypes;
- (NSArray *)getExhibitorsByTypeName:(NSString *)strType;
- (Exhibitor *)getExhibitorsByID:(NSString *)ID;
- (void) saveProgrammeFav:(NSNumber *)programe_id andFav:(NSNumber *)isFav;
- (ProgrameDayIds *)getDayIdById:(NSNumber *)idx;
- (NSArray *) getProgrammeSpeakerIdsByDayId:(NSNumber *)programme_id;
- (NSArray *) getProgramsDetailBySpeakerId:(NSNumber *)speaker_id;
- (NSArray *) getAbouts;
- (NSArray *) getPromotions;
- (NSArray *) getNews;
- (NSNumber *) getProgramFavouriteByProgramId:(NSNumber *)program_id;
- (NSMutableArray *) getFavouriteProgrammeByTime:(NSString *)programme_time andDay_id:(NSNumber *)day_id;
- (Partner *)getPartnerClinic;
- (NSArray *) getProgrammeByDay_id:(NSNumber *)day_id andSpeakeId:(NSNumber *)speaker_id;
- (NSArray *)getExhibitors;
- (void) registerForPush:(NSString *) token andDeviceType:(NSString *) type;
@end
