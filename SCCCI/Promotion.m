//
//  Promotion.m
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Promotion.h"


@implementation Promotion

#pragma mark - JSON serialization
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"promotion_id" : @"promotion_id",
             @"promotion_description" : @"promotion_description",
             @"promotion_logo" : @"promotion_logo",
             @"promotion_image" : @"promotion_image",
             @"promotion_title" : @"promotion_title",
             @"promotion_map" : @"promotion_map",
             @"promotion_excerpt" : @"promotion_excerpt",
             @"promotion_excerpt_cn" : @"promotion_excerpt_cn",
             @"promotion_description_cn" : @"promotion_description_cn",
             @"promotion_title_cn" : @"promotion_title_cn",
             @"promotion_coordinates" : @"promotion_coordinates",
             @"promotion_exhibitor_id": @"promotion_exhibitor_id",
             @"promotion_exhibitor_name": @"promotion_exhibitor_name"
             };
}

#pragma mark - Managed object serialization
+ (NSString *)managedObjectEntityName {
    return @"Promotion";
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    return @{
             @"promotion_id" : @"promotion_id",
             @"promotion_description" : @"promotion_description",
             @"promotion_logo" : @"promotion_logo",
             @"promotion_image" : @"promotion_image",
             @"promotion_title" : @"promotion_title",
             @"promotion_map" : @"promotion_map",
             @"promotion_excerpt" : @"promotion_excerpt",
             @"promotion_excerpt_cn" : @"promotion_excerpt_cn",
             @"promotion_description_cn" : @"promotion_description_cn",
             @"promotion_title_cn" : @"promotion_title_cn",
             @"promotion_coordinates" : @"promotion_coordinates",
             @"promotion_exhibitor_id": @"promotion_exhibitor_id",
             @"promotion_exhibitor_name": @"promotion_exhibitor_name"
             };
}

+ (NSSet *)propertyKeysForManagedObjectUniquing {
    return [NSSet setWithObject:@"promotion_id"];
}

+ (void) deleteAllWithContext: (NSManagedObjectContext *) context {
    NSFetchRequest * fectchRequest = [[NSFetchRequest alloc] init];
    [fectchRequest setEntity:[NSEntityDescription entityForName:@"Promotion" inManagedObjectContext:context]];
    [fectchRequest setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * objArr = [context executeFetchRequest:fectchRequest error:&error];
    //error handling goes here
    for (NSManagedObject * managedObj in objArr) {
        [context deleteObject:managedObj];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //more error handling here
}

@end
