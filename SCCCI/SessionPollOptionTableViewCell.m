//
//  SessionPollOptionTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 6/26/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SessionPollOptionTableViewCell.h"
#import "StringTable.h"
@interface SessionPollOptionTableViewCell()
@property (nonatomic,strong) UIImageView * imgCheckIcon;
@property (nonatomic,strong) UILabel * lblName;
@end
@implementation SessionPollOptionTableViewCell
- (UIImageView *)imgCheckIcon{
    if (!_imgCheckIcon) {
        _imgCheckIcon = [[UIImageView alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 5, 22, 22)];
        [_imgCheckIcon setImage:[UIImage imageNamed:@"uncheck"]];
    }
    return _imgCheckIcon;
}

- (UILabel *) lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(self.imgCheckIcon.frame.origin.x + self.imgCheckIcon.frame.size.width + 5, 5, NORMAL_WIDTH -self.imgCheckIcon.frame.size.width, 22)];
        _lblName.font = [UIFont fontWithName:ROBOTO_LIGHT size:13];
        _lblName.textColor = [UIColor colorWithHexString:@"2b2b2b"];
    }
    return _lblName;
}

- (void) setUpViews{
    [self addSubview:self.imgCheckIcon];
    [self addSubview:self.lblName];
}

- (void) loadWithName:(NSString *)strName andCheck:(BOOL)check{
    self.lblName.text = strName;
    [self setImageCheck:check];
}

- (void) setImageCheck:(BOOL) isCheck{
    if (isCheck)
        [_imgCheckIcon  setImage:[UIImage imageNamed:@"uncheck"]];
    else [_imgCheckIcon  setImage:[UIImage imageNamed:@"checked"]];
    
}

@end
