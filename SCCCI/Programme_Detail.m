//
//  Programme_Detail.m
//  SCCCI
//
//  Created by Zayar on 7/13/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Programme_Detail.h"


@implementation Programme_Detail

#pragma mark - JSON serialization
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"programme_id" : @"programme_id",
             @"speaker_id" : @"speaker_id"
             };
}

#pragma mark - Managed object serialization
+ (NSString *)managedObjectEntityName {
    return @"Programme_Detail";
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    return @{
             @"programme_id" : @"programme_id",
             @"speaker_id" : @"speaker_id"
             };
}

+ (NSSet *)propertyKeysForManagedObjectUniquing {
    return [NSSet setWithObjects:@"programme_id",@"speaker_id",nil];
}

+ (void) deleteAllWithContext: (NSManagedObjectContext *) context {
    NSFetchRequest * fectchRequest = [[NSFetchRequest alloc] init];
    [fectchRequest setEntity:[NSEntityDescription entityForName:@"Programme_Detail" inManagedObjectContext:context]];
    [fectchRequest setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * objArr = [context executeFetchRequest:fectchRequest error:&error];
    //error handling goes here
    for (NSManagedObject * managedObj in objArr) {
        [context deleteObject:managedObj];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //more error handling here
}
@end
