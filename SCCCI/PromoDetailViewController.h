//
//  PromoDetailViewController.h
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasedImageTextDescriptionViewController.h"
#import "Promotion.h"
@interface PromoDetailViewController : BasedImageTextDescriptionViewController
@property (nonatomic,strong) Promotion * promotion;
@end
