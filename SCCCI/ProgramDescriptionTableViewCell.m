//
//  ProgramDescriptionTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ProgramDescriptionTableViewCell.h"
#import "StringTable.h"
#import "RTLabel.h"
#import "NSString+HTML.h"
@interface ProgramDescriptionTableViewCell()
@property (nonatomic,strong) RTLabel * lblDescription;
@end

@implementation ProgramDescriptionTableViewCell
- (RTLabel *) lblDescription{
    if (!_lblDescription) {
        _lblDescription = [[RTLabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 0, 290, 0)];
        _lblDescription.font = [UIFont fontWithName:@"Helvetica Neue" size:12];
        _lblDescription.textColor = [UIColor colorWithHexString:@"7a7a7a"];
    }
    return _lblDescription;
}

- (void) setUpView{
    [self addSubview:self.lblDescription];
}

- (void) viewLoadWithDescription:(NSString *)strDescription{
    
    
    CGRect newFrame = _lblDescription.frame;
    newFrame.size.height = [ProgramDescriptionTableViewCell heightForCellWithPost:strDescription];
    self.lblDescription.frame = newFrame;
    self.lblDescription.text = [strDescription stringByDecodingHTMLEntities];
    //self.lblDescription.attributedText
}

+ (CGFloat)heightForCellWithPost:(NSString *)strDescription {
    NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"Helvetica Neue" size:15.0f] forKey: NSFontAttributeName];
    
    CGSize sizeToFit  = [strDescription boundingRectWithSize:CGSizeMake(290.0f, CGFLOAT_MAX)
                                                     options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:stringAttributes context:nil].size;
    return fmaxf(50.0f, sizeToFit.height+ 5);
}

@end
