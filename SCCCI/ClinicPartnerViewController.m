//
//  ClinicPartnerViewController.m
//  SCCCI
//
//  Created by Zayar on 7/10/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ClinicPartnerViewController.h"
#import "Partner.h"
#import "StringTable.h"
#import "AppDelegate.h"
#import "ServiceMappingClient.h"
@interface ClinicPartnerViewController ()

@end

@implementation ClinicPartnerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"Partner\'s Clinic"];
    

}

- (void)viewWillAppear:(BOOL)animated{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    Partner * partner = [client getPartnerClinic];
    //pathForResource:@"AboutHTML/about_template" ofType:@"html"
    NSString* path = [[NSBundle mainBundle] pathForResource:@"HighlightsHTML/Clinic_template"
                                                     ofType:@"html"];
    NSString* content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    
    NSInteger lang = [StringTable getCurrentLangage];
    if (lang == LANGAUGE_EGN) {
        content = [content stringByReplacingOccurrencesOfString:@"body_content" withString:partner.partners_clinic];
        
    }
    else if (lang == LANGAUGE_CN){
        content = [content stringByReplacingOccurrencesOfString:@"body_content" withString:partner.partners_clinic_cn];
        
    }
    //NSLog(@"fname %@",content);
    [self.webView loadHTMLString:content baseURL:nil];
}

- (void)onRegister{
    
}

@end
