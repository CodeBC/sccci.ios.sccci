//
//  HomeViewController.m
//  SCCCI
//
//  Created by Zayar on 6/13/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeCollectionCell.h"
#import "ZYCustomScrollSliderView.h"
#import "AboutViewController.h"
#import "ProgrammeViewController.h"
#import "SpeakerViewController.h"
#import "MapsViewController.h"
#import "MyScheduleViewController.h"
#import "ExhibitorListViewController.h"
#import "OrganiserListViewController.h"
#import "HighlightViewController.h"
#import "LuckyDrawWebViewController.h"
#import "LivePollWebViewController.h"
#import "SurveyWebViewController.h"
#import "SettingViewController.h"
#import "StringTable.h"
#import "SponsersViewController.h"
#import "PromotionsViewController.h"
#import "NewsViewController.h"
#import "SIAlertView.h"
@interface HomeViewController ()
//@property (strong, nonatomic) IBOutlet ZYCustomScrollSliderView *topBannerSilder;
@property (strong, nonatomic) IBOutlet UICollectionView *homeCollectionView;
@property (strong, nonatomic) AboutViewController * aboutViewController;
@property (strong, nonatomic) ProgrammeViewController * programmeViewController;
@property (strong, nonatomic) SpeakerViewController * speakerViewController;
@property (strong, nonatomic) MapsViewController * mapsViewController;
@property (strong, nonatomic) MyScheduleViewController * myScheduleViewController;
@property (strong, nonatomic) ExhibitorListViewController * exhibitorListViewController;
@property (strong, nonatomic) OrganiserListViewController * organiserListViewController;
@property (strong, nonatomic) HighlightViewController * highlightViewController;
@property (strong, nonatomic) LuckyDrawWebViewController * luckyDrawWebViewController;
@property (strong, nonatomic) LivePollWebViewController  * livePollWebViewController;
@property (strong, nonatomic) SurveyWebViewController  * surveyWebViewController;
@property (strong, nonatomic) SettingViewController  * settingViewController;
@property (strong, nonatomic) SponsersViewController  * sponsersViewController;
@property (nonatomic,strong) NSArray * arrImages;// icon image
@property (nonatomic,strong) NSArray * arrTitles;// title of icon
@property (nonatomic,strong) NSArray * arrTitlesCn;// title of icon

@end

@implementation HomeViewController

- (SponsersViewController *) sponsersViewController{
    if (!_sponsersViewController) {
        _sponsersViewController = [[SponsersViewController alloc] init];
    }
    return _sponsersViewController;
}

- (SurveyWebViewController *) surveyWebViewController{
    if (!_surveyWebViewController) {
        _surveyWebViewController = [[SurveyWebViewController alloc] init];
    }
    return _surveyWebViewController;
}

- (SettingViewController *) settingViewController{
    if (!_settingViewController) {
        _settingViewController = [[SettingViewController alloc] init];
    }
    return _settingViewController;
}

- (LuckyDrawWebViewController *) luckyDrawWebViewController{
    if (!_luckyDrawWebViewController) {
        _luckyDrawWebViewController = [[LuckyDrawWebViewController alloc] init];
    }
    return _luckyDrawWebViewController;
}

- (LivePollWebViewController *) livePollWebViewController{
    if (!_livePollWebViewController) {
        _livePollWebViewController = [[LivePollWebViewController alloc] init];
    }
    return _livePollWebViewController;
}

- (HighlightViewController *) highlightViewController{
    if (!_highlightViewController) {
        _highlightViewController = [[HighlightViewController alloc] init];
    }
    return _highlightViewController;
}

- (OrganiserListViewController *) organiserListViewController{
    if (!_organiserListViewController) {
        _organiserListViewController = [[OrganiserListViewController alloc] init];
    }
    return _organiserListViewController;
}

- (AboutViewController *) aboutViewController{
    if (!_aboutViewController) {
        _aboutViewController = [[AboutViewController alloc] init];
    }
    return _aboutViewController;
}

- (ProgrammeViewController *) programmeViewController{
    if (!_programmeViewController) {
        _programmeViewController = [[UIStoryboard storyboardWithName:@"Programme" bundle:nil] instantiateViewControllerWithIdentifier:@"Programme"];
    }
    return _programmeViewController;
}

- (SpeakerViewController *) speakerViewController{
    if (!_speakerViewController) {
        _speakerViewController = [[SpeakerViewController alloc] init];
    }
    return _speakerViewController;
}

- (MapsViewController *) mapsViewController{
    if (!_mapsViewController) {
        _mapsViewController = [[UIStoryboard storyboardWithName:@"Maps" bundle:nil] instantiateViewControllerWithIdentifier:@"Maps"];
    }
    return _mapsViewController;
}

- (MyScheduleViewController *) myScheduleViewController{
    if (!_myScheduleViewController) {
        _myScheduleViewController = [[UIStoryboard storyboardWithName:@"MySchedule" bundle:nil] instantiateViewControllerWithIdentifier:@"MySchedule"];
    }
    return _myScheduleViewController;
}

- (ExhibitorListViewController *) exhibitorListViewController{
    if (!_exhibitorListViewController) {
        _exhibitorListViewController = [[ExhibitorListViewController alloc] init];
    }
    return _exhibitorListViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.homeCollectionView setBackgroundColor:[UIColor clearColor]];
    
    UIBarButtonItem * settingBarButton= [[UIBarButtonItem alloc] initWithTitle:@"Eng | 华文" style:UIBarButtonItemStylePlain target:self action:@selector(onSetting:)];
    [settingBarButton setTitleTextAttributes:@{
                                               NSFontAttributeName: [UIFont fontWithName:ROBOTO_LIGHT size:13.0],
                                               NSForegroundColorAttributeName: [UIColor whiteColor]
                                               } forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = settingBarButton;
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor whiteColor];
    
}

- (void) onSetting:(UIBarButtonItem *)sender{
    [self.navigationController pushViewController:self.settingViewController animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [self.topBannerSilder sliderPlay];
    [self.homeCollectionView reloadData];
    
    if ([self getCurrentLanguage] == LANGAUGE_EGN) {
        //首页
        [self setNavBarTitle:@"Home"];
    }
    else if([self getCurrentLanguage] == LANGAUGE_CN){
        [self setNavBarTitle:@"首页"];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getPushNoti:) name:@"pushNotiArrived" object:nil];

}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pushNotiArrived" object:nil];

}

- (NSArray *)arrImages{
    if (!_arrImages) {
        _arrImages = [NSArray arrayWithObjects:@"about_icon",@"programs_icon",@"speakers_icon",@"map_icon",@"my_schedule",@"exhibitor_icon",@"organizer_icon",@"highlight_icon",@"lucky_draw_icon",@"live_poll_icon",@"survey_icon",@"setting_icon", nil];
    }
    return _arrImages;
}

- (NSArray *)arrTitles{
    if (!_arrTitles) {
        _arrTitles = [NSArray arrayWithObjects:@"About",@"Programme",@"Speakers",@"Map",@"My Schedule",@"Exhibition",@"Sponsors",@"Highlights",@"Lucky Draw",@"Live Poll",@"Survey",@"Settings", nil];
//        _arrTitles = [NSArray arrayWithObjects:@"About",@"Programme",@"Speakers",@"Map",@"My Schedule",@"Exhibition",@"Sponsors",@"Highlights",@"Settings", nil];
    }
    return _arrTitles;
}

- (NSArray *)arrTitlesCn{
    if (!_arrTitlesCn) {
        _arrTitlesCn = [NSArray arrayWithObjects:@"大会介绍",@"会议议程",@"主讲者介绍",@"地点",@"我的日程",@"展览",@"赞助商介绍",@"活动亮点",@"幸运抽奖",@"现场调查",@"问卷调查",@"系统设置", nil];
//        _arrTitlesCn = [NSArray arrayWithObjects:@"大会介绍",@"会议议程",@"主讲者介绍",@"地点",@"我的日程",@"展览",@"赞助商介绍",@"活动亮点",@"系统设置", nil];
    }
    return _arrTitlesCn;
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.arrTitles count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HomeCollectionCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"homeCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 5;
    [cell setUpViews];
    NSInteger lang = [StringTable getCurrentLangage];
    if (lang == LANGAUGE_EGN)
        [cell loadTheViewWithTitle:[self.arrTitles objectAtIndex:indexPath.row] andIcon:[self.arrImages objectAtIndex:indexPath.row]];
    else if (lang == LANGAUGE_CN)
        [cell loadTheViewWithTitle:[self.arrTitlesCn objectAtIndex:indexPath.row] andIcon:[self.arrImages objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * str;
    if ([self getCurrentLanguage] == LANGAUGE_EGN)
            str = [self.arrTitles objectAtIndex:indexPath.row];
    else if ([self getCurrentLanguage] == LANGAUGE_CN)
        str = [self.arrTitlesCn objectAtIndex:indexPath.row];
    
    switch (indexPath.row) {
        case 0:
            self.aboutViewController.strTitle = str;
            [self.navigationController pushViewController:self.aboutViewController animated:YES];
            break;
        case 1:
            self.programmeViewController.strTitle = str;
            [self.navigationController pushViewController:self.programmeViewController animated:YES];
            break;
        case 2:
            self.speakerViewController.strTitle = str;
            [self.navigationController pushViewController:self.speakerViewController animated:YES];
            break;
        case 3:
            self.mapsViewController.strTitle = str;
            [self.navigationController pushViewController:self.mapsViewController animated:YES];
            break;
        case 4:
            self.myScheduleViewController.strTitle = str;
            [self.navigationController pushViewController:self.myScheduleViewController animated:YES];
            break;
        case 5:
            self.exhibitorListViewController.strTitle = str;
            [self.navigationController pushViewController:self.exhibitorListViewController animated:YES];
            break;
        case 6:
            self.sponsersViewController.strTitle = str;
            [self.navigationController pushViewController:self.sponsersViewController animated:YES];
            break;
        case 7:
            self.highlightViewController.strTitle = str;
            [self.navigationController pushViewController:self.highlightViewController animated:YES];
            break;
        case 8:
            self.luckyDrawWebViewController.strTitle = str;
            [self.navigationController pushViewController:self.luckyDrawWebViewController animated:YES];
            break;
        case 9:
            self.livePollWebViewController.strTitle = str;
            [self.navigationController pushViewController:self.livePollWebViewController animated:YES];
            break;
        case 10:
            self.surveyWebViewController.strTitle = str;
            [self.navigationController pushViewController:self.surveyWebViewController animated:YES];
            break;
        case 11:
            self.settingViewController.strTitle = str;
            [self.navigationController pushViewController:self.settingViewController animated:YES];
            break;
        default:
            break;
    }
}

#pragma mark – UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize retval = CGSizeMake(78, 90);
    return retval;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 20, 5, 20);
}

- (void) getPushNoti:(NSNotification *) notification{
    NSLog(@"THE NOTIFICATION IS %@",notification.userInfo);

    if ([[notification.userInfo objectForKey:@"alert_type"] isEqualToString:@"promo"])
    {
        PromotionsViewController *promotionsViewController = [[PromotionsViewController alloc] init];
        [self.navigationController pushViewController:promotionsViewController animated:YES];
    }
    else if ([[notification.userInfo objectForKey:@"alert_type"] isEqualToString:@"news"]){
        NewsViewController *newsViewController = [[NewsViewController alloc] init];
        [self.navigationController pushViewController:newsViewController animated:YES];
    }
    
}


@end
