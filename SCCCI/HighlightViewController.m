//
//  HighlightViewController.m
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "HighlightViewController.h"
#import "SimpleNameTableViewCell.h"
#import "Utility.h"
#import "PromotionsViewController.h"
#import "ClinicPartnerViewController.h"
#import "NewsViewController.h"
#import "StringTable.h"

@interface HighlightViewController ()
//@property (strong, nonatomic) IBOutlet UITableView *tbl;
@property (strong, nonatomic) NSArray * arrHighlightNames;
@property (strong, nonatomic) PromotionsViewController * promotionsViewController;
@property (strong, nonatomic) ClinicPartnerViewController * clinicPartnerViewController;
@property (strong, nonatomic) NewsViewController * newsViewController;
@end

@implementation HighlightViewController
@synthesize strTitle;
- (NewsViewController *) newsViewController{
    if (!_newsViewController) {
        _newsViewController = [[NewsViewController alloc] init];
    }
    return _newsViewController;
}

- (PromotionsViewController *)promotionsViewController{
    if (!_promotionsViewController) {
        _promotionsViewController = [[PromotionsViewController alloc] init];
    }
    return _promotionsViewController;
}

- (ClinicPartnerViewController *)clinicPartnerViewController{
    if (!_clinicPartnerViewController) {
        _clinicPartnerViewController = [[ClinicPartnerViewController alloc] init];
    }
    return _clinicPartnerViewController;
}


- (NSArray *) arrHighlightNames{
    //Scott Smith Ki-Ho Park Mimiwati
    if ([self getCurrentLanguage] == LANGAUGE_EGN) {
        _arrHighlightNames = [NSArray arrayWithObjects:@"Special Promotions",@"Partner's Clinic",@"Other News", nil];
    } else {
        _arrHighlightNames = [NSArray arrayWithObjects:@"特惠促销",@"专家会诊把脉",@"其他信息", nil];
    }
    return _arrHighlightNames;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavBarTitle:@"Highlights"];
    
    /*if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([self.tbl respondsToSelector:@selector(separatorInset)]) {
            [self.tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }*/
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
    [self.tbl reloadData];
}

- (void)viewWillAppear:(BOOL)animated{
    [self setNavBarTitle:strTitle];
    [self.tbl reloadData];
}

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SimpleNameTableViewCell";
	SimpleNameTableViewCell *cell = (SimpleNameTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[SimpleNameTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    
    [cell viewLoadWithString:[self.arrHighlightNames   objectAtIndex:indexPath.row]];
    UIImageView * indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
    indicatorView.image = [UIImage imageNamed:@"cell_indicator"];
    cell.accessoryView = indicatorView;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrHighlightNames count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            [self.navigationController pushViewController:self.promotionsViewController animated:YES];
            break;
        case 1:
            [self.navigationController pushViewController:self.clinicPartnerViewController animated:YES];
            break;
        case 2:
            [self.navigationController pushViewController:self.newsViewController animated:YES];
            break;
            
        default:
            break;
    }
}



@end
