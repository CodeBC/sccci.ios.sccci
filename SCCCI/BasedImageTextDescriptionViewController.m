//
//  BasedImageTextDescriptionViewController.m
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "BasedImageTextDescriptionViewController.h"
#import "StringTable.h"
#import "RTLabel.h"
@interface BasedImageTextDescriptionViewController ()

@end
#define TOP_IMAGE_HEIGHT 128.0f
@implementation BasedImageTextDescriptionViewController
- (UIImageView *) imgView{
    if (!_imgView) {
        _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,TOP_IMAGE_HEIGHT)];
    }
    return _imgView;
}

- (UILabel *)lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.imgView.frame.origin.y + self.imgView.frame.size.height + 5, 290, 20)];
        _lblName.font = [UIFont fontWithName:ROBOTO_BOLD size:15];
        _lblName.textColor = [UIColor colorWithHexString:@"01457b"];
    }
    return _lblName;
}

- (RTLabel *)lblDescription{
    if (!_lblDescription) {
        _lblDescription = [[RTLabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.lblName.frame.origin.y + self.lblName.frame.size.height + 5, 290, 400)];
        _lblDescription.font = [UIFont fontWithName:ROBOTO_REGUALAR size:12];
        _lblDescription.textColor = [UIColor colorWithHexString:@"333333"];
        
    }
    return _lblDescription;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self hideBasedTopBanner];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.imgView];
    [self.view addSubview:self.lblName];
    [self.view addSubview:self.lblDescription];
}

@end
