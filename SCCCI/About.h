//
//  About.h
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"
@interface About: MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (nonatomic, retain) NSString * about_intro_annual;
@property (nonatomic, retain) NSString * about_intro_info;
@property (nonatomic, retain) NSString * about_intro_sm;
@property (nonatomic, retain) NSString * about_sccci;
@property (nonatomic, retain) NSString * about_ida;
@property (nonatomic, retain) NSString * about_lianhe_zaobao;
@property (nonatomic, retain) NSString * about_intro_annual_cn;
@property (nonatomic, retain) NSString * about_intro_info_cn;
@property (nonatomic, retain) NSString * about_intro_sm_cn;
@property (nonatomic, retain) NSString * about_sccci_cn;
@property (nonatomic, retain) NSString * about_ida_cn;
@property (nonatomic, retain) NSString * about_lianhe_zaobao_cn;

@end
