//
//  AboutDetailViewController.m
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "AboutDetailViewController.h"
#import "StringTable.h"
@interface AboutDetailViewController ()
@property (nonatomic, strong) UIActivityViewController *activityViewController;
@end

@implementation AboutDetailViewController
@synthesize strTitle,strContent,info;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIBarButtonItem * btnShareFb = [[UIBarButtonItem alloc] initWithTitle:@"Share on FB" style:UIBarButtonItemStylePlain target:self action:@selector(onShare)];
    btnShareFb.tintColor = [UIColor colorWithHexString:@"333333"];
    UIBarButtonItem * btnShareICon = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_share"] style:UIBarButtonItemStylePlain target:self action:@selector(onShare)];
    btnShareICon.tintColor = [UIColor blueColor];
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //fixedSpace.width = 10;
    [self.footerToolBar setItems:@[btnShareFb,fixedSpace,btnShareICon]];
    strTitle = @"Detail";
    [self setNavBarTitle:strTitle];
}

- (void)viewWillAppear:(BOOL)animated{
    /*NSURL *htmlURL = [[NSBundle mainBundle] URLForResource:@"AboutHTML/about_template" withExtension:@"html"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:htmlURL];
    NSLog(@"request %@",request);
    [self.webView loadRequest:request];*/
    NSInteger lang = [StringTable getCurrentLangage];
    if (lang == LANGAUGE_EGN) {
         [self.webView loadHTMLString:info.descr baseURL:nil];
    }
    else if (lang == LANGAUGE_CN){
        [self.webView loadHTMLString:info.descr_cn baseURL:nil];
    }
}

- (void)onShare{
    self.activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[[NSString stringWithFormat:@"%@ \n via SMEICC[%@]",SHARE_MSG,SHARE_LINK]] applicationActivities:nil];
    [self presentViewController:self.activityViewController animated:YES completion:nil];
    //[NSString stringWithFormat:@"Original Link:[%@]",SHARE_LINK]
}

@end
