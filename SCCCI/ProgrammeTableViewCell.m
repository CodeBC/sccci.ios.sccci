//
//  ProgrammeTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ProgrammeTableViewCell.h"
#import "StringTable.h"
@interface ProgrammeTableViewCell()
{
    NSInteger selectedSection;
    BOOL isFav;
}
@property (nonatomic,strong) UIImageView * imgIconView;
@property (nonatomic,strong) UIButton * btnFavView;
@property (nonatomic,strong) UILabel * lblName;
@property (nonatomic,strong) UILabel * lblLocationTimeLanguage;

@end
@implementation ProgrammeTableViewCell
@synthesize owner;
- (UIButton *) btnFavView{
    if (!_btnFavView) {
        _btnFavView = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnFavView setFrame:CGRectMake(LEFT_MARGIN, 10, 37, 37)];
        [_btnFavView setImage:[UIImage imageNamed:@"programme_cell_icon_deactive"] forState:UIControlStateNormal];
        [_btnFavView addTarget:self action:@selector(onFav:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnFavView;
}

- (UIImageView *) imgIconView{
    if (!_imgIconView) {
        _imgIconView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 5, 37, 37)];
    }
    return _imgIconView;
}

- (UILabel *) lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(self.btnFavView.frame.origin.x + self.imgIconView.frame.size.width + 15, 5, 210, 23)];
        _lblName.font = [UIFont fontWithName:ROBOTO_BOLD size:14];
        _lblName.textColor = [UIColor colorWithHexString:@"01457b"];
    }
    return _lblName;
}

- (UILabel *) lblLocationTimeLanguage{
    if (!_lblLocationTimeLanguage) {
        _lblLocationTimeLanguage = [[UILabel alloc] initWithFrame:CGRectMake(self.imgIconView.frame.origin.x + self.imgIconView.frame.size.width + 15, self.lblName.frame.origin.y + self.lblName.frame.size.height+3, 210, 20)];
        _lblLocationTimeLanguage.font = [UIFont fontWithName:ROBOTO_LIGHT size:11];
        _lblLocationTimeLanguage.textColor = [UIColor colorWithHexString:@"454545"];
    }
    return _lblLocationTimeLanguage;
}

- (void) setUpTheViews{
    [self addSubview:self.btnFavView];
    [self addSubview:self.lblName];
    [self addSubview:self.lblLocationTimeLanguage];
}

- (void)onFav:(UIButton *)sender{
    [owner onProgrammeTableViewCellFav:self andSection:selectedSection andFav:isFav];
    [self setFavButton:isFav];
}

- (void)loadTheViewWithName:(NSString *)strName andIsActive:(BOOL) isActive andSection:(NSInteger)section andLocation:(NSString *)strLocation andTime:(NSString *)strTime andLanguage:(NSString *)strLanguage{
    selectedSection = section;
    self.lblName.text = strName;
    isFav = isActive;
    [self setFavButton:isFav];
    [self setTheLocationTimeLanguageValueWith:strLocation andTime:strTime andLanguage:strLanguage];
}

- (void)setTheLocationTimeLanguageValueWith:(NSString *)strLocation andTime:(NSString *)strTime andLanguage:(NSString *)strLangauge{
    self.lblLocationTimeLanguage.text = [NSString stringWithFormat:@"%@   %@   %@",strLocation,strTime,strLangauge];
}

- (void) setFavButton:(BOOL) is_Fav{
    if (is_Fav)
    {
        [_btnFavView setImage:[UIImage imageNamed:@"programme_cell_icon_active"] forState:UIControlStateNormal];
        isFav = false;
    }
    else
    {
        [_btnFavView setImage:[UIImage imageNamed:@"programme_cell_icon_deactive"] forState:UIControlStateNormal];
        isFav = true;
    }
}
@end
