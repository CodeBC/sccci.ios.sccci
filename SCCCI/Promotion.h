//
//  Promotion.h
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"
@interface Promotion: MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (nonatomic, retain) NSNumber * promotion_id;
@property (nonatomic, retain) NSString * promotion_description;
@property (nonatomic, retain) NSString * promotion_logo;
@property (nonatomic, retain) NSString * promotion_image;
@property (nonatomic, retain) NSString * promotion_title;
@property (nonatomic, retain) NSString * promotion_map;
@property (nonatomic, retain) NSString * promotion_excerpt;
@property (nonatomic, retain) NSString * promotion_excerpt_cn;
@property (nonatomic, retain) NSString * promotion_description_cn;
@property (nonatomic, retain) NSString * promotion_title_cn;
@property (nonatomic, retain) NSString * promotion_coordinates;
@property (nonatomic, retain) NSString * promotion_exhibitor_id;
@property (nonatomic, retain) NSString * promotion_exhibitor_name;

+ (void) deleteAllWithContext: (NSManagedObjectContext *) context;

@end
