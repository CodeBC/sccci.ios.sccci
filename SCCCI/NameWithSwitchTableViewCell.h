//
//  NameWithOptionTableViewCell.h
//  SCCCI
//
//  Created by Zayar on 7/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NameWithSwitchTableViewCell : UITableViewCell
- (void)setUpViews;
- (void)viewLoadWithString:(NSString *)strName andSwitch:(BOOL)isSwitch;
@end
