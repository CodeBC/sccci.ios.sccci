//
//  SpeakerAboutTableViewCell.h
//  SCCCI
//
//  Created by Zayar on 6/18/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpeakerAboutTableViewCell : UITableViewCell
+ (CGFloat)heightForCellWithPost:(NSString *)strAbout;
- (void) setUpViews;
- (void) viewLoadWithName:(NSString *)strName andJob:(NSString *)strJob andAbout:(NSString *)strAbout andImageURL:(NSString *)strImageUrl andCompany:(NSString *)strCompanyName;
+ (CGFloat)heightForCellAndProfileImageWithAbout:(NSString *)strAbout;
-(NSString *) stringByStrippingHTML: (NSString *) s;
-(void)updateSizeToFit;
@end
