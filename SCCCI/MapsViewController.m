//
//  MapsViewController.m
//  SCCCI
//
//  Created by Zayar on 6/18/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MapsViewController.h"
#import "StringTable.h"

@interface MapsViewController ()
{
    CLLocationDegrees lat;
    CLLocationDegrees lon;
    CGFloat currentLocationLat;
    CGFloat currentLocationLong;
}
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) UIImage * lvl3Image;
@property (weak, nonatomic) UIImage * lvl4Image;
@property (weak, nonatomic) IBOutlet UIImageView *lvlImageView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnDirection;
@property (weak, nonatomic) IBOutlet UIToolbar *footerToolBar;

@end

@implementation MapsViewController
@synthesize strTitle;
- (UIImage *) lvl3Image{
    if (!_lvl3Image) {
        _lvl3Image = [UIImage imageNamed:@"Level3_SCCCI_2014"];
    }
    return _lvl3Image;
}

- (UIImage *) lvl4Image{
    if (!_lvl4Image) {
        _lvl4Image = [UIImage imageNamed:@"Level4_SCCCI_2014.jpg"];
    }
    return _lvl4Image;
}

- (CLLocationManager *) locationManager{
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    }
    return _locationManager;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.locationManager startUpdatingLocation];
    [self setNavBarTitle:@"Maps"];
    self.scrollView.delegate = self;
    
    self.scrollView.minimumZoomScale = 1;
    self.scrollView.maximumZoomScale = 5.0f;
    self.scrollView.zoomScale = 1;
    
    lat = 1.293640;
    lon = 103.857326;
    
    CLLocationCoordinate2D crood= CLLocationCoordinate2DMake(lat, lon);
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(crood, 500, 500);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    [self.mapView setRegion:adjustedRegion animated:YES];
    self.mapView.showsUserLocation = YES;
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    /*UIAlertView *errorAlert = [[UIAlertView alloc]
     initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
     [errorAlert show];*/
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@ :lat: %.6f lon: %.6f", newLocation,newLocation.coordinate.latitude,newLocation.coordinate.longitude);
    
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation.coordinate.longitude != 0 && currentLocationLat == 0) {
        
        currentLocationLat = currentLocation.coordinate.latitude;
        currentLocationLong = currentLocation.coordinate.longitude;

    }
}

- (void)viewWillAppear:(BOOL)animated{
    [self setNavBarTitle:strTitle];
    self.segment.selectedSegmentIndex = 0;
    [self onSegment:self.segment];
    if ([self getCurrentLanguage] == LANGAUGE_EGN) {
        [self.segment setTitle:@"Venue Map" forSegmentAtIndex:0];
        [self.segment setTitle:@"Level 3 Map" forSegmentAtIndex:1];
        [self.segment setTitle:@"Level 4 Map" forSegmentAtIndex:2];
    } else {
        [self.segment setTitle:@"会场地图" forSegmentAtIndex:0];
        [self.segment setTitle:@"三楼地图" forSegmentAtIndex:1];
        [self.segment setTitle:@"四楼地图" forSegmentAtIndex:2];
    }

}

- (IBAction)onSegment:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case 0:
            [self showVenueMap:YES];
            break;
        case 1:
            //[self.imgLvlView setImage:self.lvl3Image];
            // Set up the image we want to scroll & zoom and add it to the scroll view
            [self.lvlImageView setImage:self.lvl3Image];
            self.scrollView.zoomScale = 1;
            [self showVenueMap:NO];
            
            break;
        case 2:
            [self.lvlImageView setImage:self.lvl4Image];
            self.scrollView.zoomScale = 1;
            [self showVenueMap:NO];
            break;
        default:
            break;
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.lvlImageView;
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view{
    NSLog(@"scroll begin scroll!!");
}

- (IBAction)onGetDirection:(id)sender {
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        
        NSString * strMapRoute = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f",currentLocationLat,currentLocationLong,lat,lon];
        NSLog(@"str map route %@",strMapRoute);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strMapRoute]];
    } else {
        NSLog(@"Can't use comgooglemaps://");
        
    }
    /*NSURL *testURL = [NSURL URLWithString:@"comgooglemaps-x-callback://"];
    if ([[UIApplication sharedApplication] canOpenURL:testURL]) {
        //NSString *directionsRequest = @"comgooglemaps-x-callback://?daddr=John+F.+Kennedy+International+Airport,+Van+Wyck+Expressway,+Jamaica,+New+York&x-success=sourceapp://?resume=true&x-source=AirApp";
        NSString * strMapRoute = [NSString stringWithFormat:@"comgooglemaps-x-callback://?saddr=%f,%f&daddr=%f,%f&x-success=sourceapp://?resume=true&x-source=AirApp",lat,lon,currentLocationLat,currentLocationLong];
        NSURL *directionsURL = [NSURL URLWithString:strMapRoute];
        [[UIApplication sharedApplication] openURL:directionsURL];
    } else {
        NSLog(@"Can't use comgooglemaps-x-callback:// on this device.");
    }*/
}

- (void) showVenueMap:(BOOL)show{
    if (show) {
        self.mapView.hidden = NO;
        self.footerToolBar.hidden = NO;
        self.scrollView.hidden = YES;
    }
    else{
        [self.view bringSubviewToFront:self.scrollView];
        self.mapView.hidden = YES;
        self.footerToolBar.hidden = YES;
        self.scrollView.hidden = NO;
    }
}

@end
