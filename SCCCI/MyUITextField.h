//
//  MyUITextField.h
//  wunzinn
//
//  Created by Zayar on 2/23/14.
//  Copyright (c) 2014 bit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyUITextField : UITextField
@property (nonatomic, assign) float verticalPadding;
@property (nonatomic, assign) float horizontalPadding;
@end
