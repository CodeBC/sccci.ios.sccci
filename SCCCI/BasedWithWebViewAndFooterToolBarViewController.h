//
//  BasedWithWebViewAndFooterToolBarViewController.h
//  SCCCI
//
//  Created by Zayar on 7/10/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "BasedViewController.h"

@interface BasedWithWebViewAndFooterToolBarViewController : BasedWithWebViewViewController
@property (nonatomic,strong) UIToolbar * footerToolBar;
@end
