//
//  ExhibitorsViewController.h
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "BasedWithTableViewController.h"
#import "ExhibitorTypes.h"
@interface ExhibitorsViewController : BasedWithTableViewController
@property (nonatomic,strong) ExhibitorTypes * exhibitorType;
@property (nonatomic,strong) NSString * typeName;
@end
