//
//  Sponsor.h
//  SCCCI
//
//  Created by Zayar on 7/11/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"


@interface Sponsor : MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (nonatomic, retain) NSNumber * sponsor_id;
@property (nonatomic, retain) NSString * sponsor_name;
@property (nonatomic, retain) NSString * sponsor_name_cn;
@property (nonatomic, retain) NSString * sponsor_about;
@property (nonatomic, retain) NSString * sponsor_about_cn;
@property (nonatomic, retain) NSString * sponsor_info;
@property (nonatomic, retain) NSString * sponsor_booth;
@property (nonatomic, retain) NSNumber * sponsor_type;
@property (nonatomic, retain) NSString * sponsor_logo;
@property (nonatomic, retain) NSString * sponsor_contact;
@property (nonatomic, retain) NSString * sponsor_contact_cn;
+ (void) deleteAllWithContext: (NSManagedObjectContext *) context;
@end
