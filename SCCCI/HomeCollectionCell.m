//
//  HomeCollectionCell.m
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "HomeCollectionCell.h"
#import "StringTable.h"
@interface HomeCollectionCell()
@property (nonatomic, strong) UIImageView * iconImgView;
@property (nonatomic, strong) UILabel * lblTitle;
@end
@implementation HomeCollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (UIImageView *) iconImgView{
    if (!_iconImgView) {
        _iconImgView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 0, 68, 68)];
    }
    return _iconImgView;
}

- (UILabel *) lblTitle{
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, self.iconImgView.frame.origin.y + self.iconImgView.frame.size.height, 78, 30)];
        _lblTitle.font = [UIFont fontWithName:ROBOTO_REGUALAR size:13];
        _lblTitle.textAlignment = NSTextAlignmentCenter;
        _lblTitle.textColor = [UIColor colorWithHexString:@"01457b"];
    }
    return _lblTitle;
}

- (void) setUpViews{
    [self addSubview:self.iconImgView];
    [self addSubview:self.lblTitle];
    [self setBackgroundColor:[UIColor clearColor]];
}

- (void) loadTheViewWithTitle:(NSString *)strTitle andIcon:(NSString *)strIcon{
    [self.iconImgView setImage:[UIImage imageNamed:strIcon]];
    [self.lblTitle setText:strTitle];
}

@end
