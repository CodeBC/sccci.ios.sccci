//
//  PromoMapViewController.h
//  SCCCI
//
//  Created by Ye Myat Min on 12/8/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "BasedViewController.h"
#import "Promotion.h"
@interface PromoMapViewController : BasedWithWebViewViewController

@property(nonatomic, strong) Promotion* promotion;

@end
