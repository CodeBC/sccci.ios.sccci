//
//  SponsorTypes.h
//  SCCCI
//
//  Created by Zayar on 7/12/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"


@interface SponsorTypes : MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (nonatomic, retain) NSNumber * type_id;
@property (nonatomic, retain) NSString * type_name;
+ (void) deleteAllWithContext: (NSManagedObjectContext *) context;
@end
