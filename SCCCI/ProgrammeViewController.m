//
//  ProgrammeViewController.m
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ProgrammeViewController.h"
#import "SVSegmentedControl.h"
#import "UIImage+Addition.h"
#import "Utility.h"
#import "StringTable.h"
#import "ProgrammeTableViewCell.h"
#import "ProgramDetailViewController.h"
#import "AppDelegate.h"
#import "ProgrameDayIds.h"
#import "ProgrammeTime.h"
#import "Programme.h"
@interface ProgrammeViewController ()
{
    NSInteger selectedDayIndex;
    NSString * strPrograms;
}
@property (strong, nonatomic) IBOutlet UITableView *tbl;
@property (strong, nonatomic) SVSegmentedControl *dateChooser;
@property (strong, nonatomic) IBOutlet UIView *dateChooserView;
@property (strong, nonatomic) NSArray * arrDayTitles;// of days
@property (strong, nonatomic) NSMutableArray * arrSectionTitles;
@property (strong, nonatomic) ProgramDetailViewController * programDetailViewController;

@end

@implementation ProgrammeViewController
@synthesize strTitle;
- (NSMutableArray *) arrSectionTitles{
    if (!_arrSectionTitles) {
        _arrSectionTitles = [[NSMutableArray alloc] init];
    }
    return _arrSectionTitles;
}

- (ProgramDetailViewController *) programDetailViewController{
    if (!_programDetailViewController) {
        _programDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProgramDetail"];
    }
    return _programDetailViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"Programme"];
    strPrograms = @"programmes";
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([self.tbl respondsToSelector:@selector(separatorInset)]) {
            [self.tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
    selectedDayIndex = 0;
    
}

- (void)viewWillAppear:(BOOL)animated{
    [self dataReload];
    [self setNavBarTitle:strTitle];
}

- (void) dataReload{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    self.arrDayTitles = [client getDayIds];
    if ([self.arrDayTitles count]) {
        [self loadDateChooserWithArr:self.arrDayTitles];
        ProgrameDayIds * objDayId = [self.arrDayTitles objectAtIndex:selectedDayIndex];
        NSArray * arr = [client getProgrammeTime];
        if ([arr count]) {
            [self.arrSectionTitles removeAllObjects];
        }
        for (ProgrammeTime * progTime in arr) {
            NSArray * programs = [client getProgrammeByTime:progTime.programme_time andDay_id:objDayId.day_id];
            NSMutableArray * arr = [NSMutableArray arrayWithArray:programs];
            NSDictionary * dic = @{ @"title":progTime.programme_time,strPrograms:arr};
            [self.arrSectionTitles addObject:dic];
        }
        [self.tbl reloadData];
    }
}

- (void)loadDateChooserWithArr:(NSArray *)arr{
    NSMutableArray * titles;
    if ([arr count]) {
        
        titles = [[NSMutableArray alloc] init];
    }
    for (ProgrameDayIds * day_id in arr) {
        [titles addObject:day_id.day_name];
    }
    if (!self.dateChooser) {
        self.dateChooser = [[SVSegmentedControl alloc] initWithSectionTitles:titles];
    }
    self.dateChooser.backgroundColor = [UIColor colorWithHexString:@"ebeaf0"];
    self.dateChooser.textColor = [UIColor colorWithHexString:@"01457b"];
    self.dateChooser.font = [UIFont fontWithName:ROBOTO_LIGHT size:14];
    self.dateChooser.thumb.textColor = [UIColor colorWithHexString:@"ffffff"];
    self.dateChooser.textShadowOffset = CGSizeMake(0, 0);
    
    UIImage *transparentBackground = [[UIImage imageWithColor:[UIColor colorWithWhite:1.0 alpha:0.0] size:CGSizeMake(1.0, 32)]
                                      resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    _dateChooser.backgroundImage = transparentBackground;
    
    //self.dateChooser.thumb.highlightedBackgroundImage = [UIImage imageNamed:@"DateThumbHilighted"];
    self.dateChooser.thumb.tintColor = [UIColor colorWithHexString:@"01457b"];
    
    [self.dateChooser setSelectedSegmentIndex:selectedDayIndex animated:NO];
    //self.dateChooser.center = CGPointMake(self.dateChooser.center.x, self.dateChooser.height / 2);
    //[Utility makeCornerRadius:self.dateChooser.thumb andRadius:40];
    
    [self.view addSubview:self.dateChooser];
    [self.dateChooser addTarget:self action:@selector(onDaySegment:) forControlEvents:UIControlEventTouchUpInside];
    
    //self.dateChooser.center = CGPointMake(self.view.frame.size.width/2, self.dateChooserView.frame.size.height/2);
    self.dateChooser.center = self.dateChooserView.center;
}

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ProgrammeTableViewCell";
	ProgrammeTableViewCell *cell = (ProgrammeTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[ProgrammeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpTheViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    cell.owner = self;
    NSDictionary * dic = [self.arrSectionTitles objectAtIndex:indexPath.section];
    NSArray * arr = [dic objectForKey:strPrograms];
    Programme * prog = [arr objectAtIndex:indexPath.row];
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    prog.is_fav = [client getProgramFavouriteByProgramId:prog.event_id];
    NSInteger lang = [StringTable getCurrentLangage];
    NSLog(@"prog start date %@",prog.start_time_stamp);
    if (lang == LANGAUGE_EGN) {
        [cell loadTheViewWithName:prog.event_name andIsActive:[prog.is_fav boolValue] andSection:indexPath.section andLocation:prog.event_location andTime:[NSString stringWithFormat:@"%@ to %@",prog.start_time,prog.end_time] andLanguage:prog.event_language];
    }
    else if (lang == LANGAUGE_CN){
        [cell loadTheViewWithName:prog.event_name_cn andIsActive:[prog.is_fav boolValue] andSection:indexPath.section andLocation:prog.event_location andTime:[NSString stringWithFormat:@"%@ to %@",prog.start_time,prog.end_time] andLanguage:prog.event_language];
    }
    
    UIImageView * indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
    indicatorView.image = [UIImage imageNamed:@"cell_indicator"];
    cell.accessoryView = indicatorView;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSDictionary * dic = [self.arrSectionTitles objectAtIndex:section];
    NSArray * arr = [dic objectForKey:strPrograms];
    return [arr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return [self.arrSectionTitles count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSLog(@"viewForHeaderInSection");
    NSString * strTitle;
    strTitle=@"";
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
    [v setBackgroundColor:[UIColor colorWithHexString:@"ebeaf0"]];
    UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-20, 35)];
    lblTitle.font = [UIFont fontWithName:ROBOTO_BOLD size:15];
    lblTitle.textColor = [UIColor colorWithHexString:@"01457b"];
    NSDictionary * dic = [self.arrSectionTitles objectAtIndex:section];
    lblTitle.text = [dic objectForKey:@"title"];
    [v addSubview:lblTitle];
    [Utility makeBorder:v andWidth:0.5 andColor:[UIColor colorWithHexString:@"bfbfbf"] andCornerRadius:0];

    return v;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 35;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary * dic = [self.arrSectionTitles objectAtIndex:indexPath.section];
    NSArray * arr = [dic objectForKey:strPrograms];
    
    self.programDetailViewController.programme = [arr objectAtIndex:indexPath.row];
    NSLog(@"programme description %@",self.programDetailViewController.programme.event_description);
    [self.navigationController pushViewController:self.programDetailViewController animated:YES];
}

- (void) onProgrammeTableViewCellFav:(ProgrammeTableViewCell *)cell andSection:(NSInteger)section andFav:(BOOL)fav{
    NSDictionary * dic = [self.arrSectionTitles objectAtIndex:section];
    NSArray * arr = [dic objectForKey:strPrograms];
    Programme * prog = [arr objectAtIndex:cell.tag];
    prog.is_fav = [NSNumber numberWithBool:fav];
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    [client saveProgrammeFav:prog.event_id andFav:prog.is_fav];
}

#pragma mark segment
- (void)onDaySegment:(SVSegmentedControl *)segment{
    selectedDayIndex = segment.selectedSegmentIndex;
    [self dataReload];
}



@end
