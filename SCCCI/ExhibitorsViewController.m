//
//  ExhibitorsViewController.m
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ExhibitorsViewController.h"
#import "Exhibitor.h"
#import "AppDelegate.h"
#import "ServiceMappingClient.h"
#import "ExhibitorListTableViewCell.h"
#import "ExhibitorDetailViewController.h"
#import "StringTable.h"

@interface ExhibitorsViewController ()
@property (nonatomic,strong) NSArray * arrList;
@property (strong, nonatomic) ExhibitorDetailViewController * exhibitorDetailViewController;
@end

@implementation ExhibitorsViewController
@synthesize exhibitorType,typeName;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
}

- (ExhibitorDetailViewController *) exhibitorDetailViewController{
    if (!_exhibitorDetailViewController) {
        _exhibitorDetailViewController = [[ExhibitorDetailViewController alloc] init];
    }
    return _exhibitorDetailViewController;
}

- (void)viewWillAppear:(BOOL)animated{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    [self setNavBarTitle:typeName];
    self.arrList = [client getExhibitorsByTypeName:typeName];
    [self.tbl reloadData];
}

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"aboutCell";
	ExhibitorListTableViewCell *cell = (ExhibitorListTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[ExhibitorListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    Exhibitor * obj = [self.arrList   objectAtIndex:indexPath.row];
    
    if ([self getCurrentLanguage] == LANGAUGE_EGN) {
        [cell viewLoadWithString:obj.exhibitor_name];
    } else {
        [cell viewLoadWithString:obj.exhibitor_name_cn];
    }
    
    
    UIImageView * indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
    indicatorView.image = [UIImage imageNamed:@"cell_indicator"];
    cell.accessoryView = indicatorView;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrList count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.exhibitorDetailViewController.exhibitor = [self.arrList objectAtIndex:indexPath.row];
    [self.navigationController  pushViewController:self.exhibitorDetailViewController animated:YES];
    
}

@end
