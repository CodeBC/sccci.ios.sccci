//
//  StringTable.h
//  zBox
//
//  Created by Zayar Cn on 6/3/12.
//  Copyright (c) 2012 MCC. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface StringTable : NSObject {
    
}
extern NSString * const APP_DB;
extern NSString * const APP_TITLE;
extern NSString * const APP_ID;
extern NSString * const APP_SHARE_LINK;
extern NSString * const APP_BASED_LINK;
extern NSString * const SPEAKERS_LINK;
extern NSString * const SPONSORS_LINK;
extern NSString * const EXHIBITOR_LINK;
extern NSString * const PROGRAMME_LINK;
extern NSString * const LUCKYDRAW_LINK ;
extern NSString * const POLL_LINK;
extern NSString * const SURVEY_LINK;
extern NSString * const INFO_LINK;
extern NSString * const ABOUT_LINK;
extern NSString * const PROMOTION_LINK;
extern NSString * const NEWS_LINK;
extern NSString * const PARTNER_CLINIC_LINK;
extern NSString * const GOV_LINK;

extern NSString * const LUCKYDRAW_CN_LINK;
extern NSString * const POLL_CN_LINK;
extern NSString * const SURVEY_CN_LINK;

extern NSString * const SHARE_MSG;
extern NSString * const SHARE_LINK;

extern NSString * const ROBOTO_LIGHT;
extern NSString * const ROBOTO_REGUALAR;
extern NSString * const ROBOTO_BOLD;

extern CGFloat const LEFT_MARGIN;
extern CGFloat const NORMAL_WIDTH;
extern CGFloat const TABLE_SECTION_HEIGHT;
extern CGFloat const STATUS_BAR_NAV_BAR_HEIGHT;

extern NSString * const SPONSOR_TYPE_PLATINUM;
extern NSString * const SPONSOR_TYPE_GOLD;

extern NSInteger const LANGAUGE_EGN;
extern NSInteger const LANGAUGE_CN;
+ (NSInteger) getCurrentLangage;
+ (void) setCurrentLanguage:(NSInteger)lang;
@end
