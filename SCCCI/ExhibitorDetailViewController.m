//
//  ExhibitorDetailViewController.m
//  SCCCI
//
//  Created by Zayar on 6/24/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ExhibitorDetailViewController.h"
#import "ExhibitorDetailProfileTableViewCell.h"
#import "ExhibitorAboutTableViewCell.h"
#import "StringTable.h"
#import "Utility.h"
@interface ExhibitorDetailViewController ()
{
    NSString * strAbout;
    NSMutableDictionary *cellHeights; // <-- add this
    NSMutableDictionary *didReloadRowsBools;// <-- and this
}
@end

@implementation ExhibitorDetailViewController
@synthesize exhibitor;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"View Exhibitor"];
    
    if (!cellHeights) {
        cellHeights = [[NSMutableDictionary alloc] init];
    }
    if (!didReloadRowsBools) {
        didReloadRowsBools = [[NSMutableDictionary alloc] init];
    }

    
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
    
    
    UIView * v = [[UIView alloc] init];
    self.tbl.tableFooterView = v;
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([self.tbl respondsToSelector:@selector(separatorInset)]) {
            [self.tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    strAbout = exhibitor.exhibitor_about;
    [self.tbl reloadData];
}

- (void) viewWillDisappear:(BOOL)animated {
    didReloadRowsBools = [[NSMutableDictionary alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"ExhibitorDetailProfileTableViewCell";
        ExhibitorDetailProfileTableViewCell *cell = (ExhibitorDetailProfileTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[ExhibitorDetailProfileTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setUpViews];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
        if ([self getCurrentLanguage] == LANGAUGE_EGN) {
            [cell viewLoadWithName:exhibitor.exhibitor_name andBoothNo:exhibitor.exhibitor_booth andWebsite:exhibitor.exhibitor_logo andImageURL:exhibitor.exhibitor_logo andContactNo:exhibitor.exhibitor_contact andContactNo2:exhibitor.exhibitor_contact];
        } else {
            [cell viewLoadWithName:exhibitor.exhibitor_name_cn andBoothNo:exhibitor.exhibitor_booth andWebsite:exhibitor.exhibitor_logo andImageURL:exhibitor.exhibitor_logo andContactNo:exhibitor.exhibitor_contact andContactNo2:exhibitor.exhibitor_contact];
        }
        return cell;
    }
    else if (indexPath.section == 1){
        static NSString *CellIdentifier = @"ExhibitorAboutTableViewCell";
        ExhibitorAboutTableViewCell *cell = (ExhibitorAboutTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[ExhibitorAboutTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setUpViews];
        } else {
        [cell forceMeasure];
        }
        cell.owner = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
        if ([self getCurrentLanguage] == LANGAUGE_EGN) {
            [cell viewLoadWithAbout:exhibitor.exhibitor_about];
        } else {
            [cell viewLoadWithAbout:exhibitor.exhibitor_about_cn];
        }
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 183;
    }
    else if(indexPath.section == 1){
        NSString *key = [NSString stringWithFormat:@"%d",indexPath.row];
        if([cellHeights objectForKey:key] == nil) {
            return 44; // change it to your default height
        } else {
            return [[cellHeights objectForKey:key] floatValue];
        }
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 2;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*self.speakerDetailViewController.strName = [self.arrSpeakerNames objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:self.speakerDetailViewController animated:YES];*/
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section != 0) {
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
        [v setBackgroundColor:[UIColor colorWithHexString:@"ebeaf0"]];
        UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-20, 35)];
        lblTitle.font = [UIFont fontWithName:ROBOTO_BOLD size:15];
        lblTitle.textColor = [UIColor colorWithHexString:@"01457b"];
        lblTitle.text = @"About";
        [v addSubview:lblTitle];
        [Utility makeBorder:v andWidth:0.5 andColor:[UIColor colorWithHexString:@"bfbfbf"] andCornerRadius:0];
        
        return v;
    }
    return nil;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section != 0) {
        return 35;
    }
    return 0;
}

- (void) webDescriptionCellDidFinish:(ExhibitorAboutTableViewCell *)cell shouldAssignHeight:(CGFloat)newHeight{
    
    NSIndexPath *indexPath = [self.tbl indexPathForCell:cell];
    NSNumber * cellDidLoad = [didReloadRowsBools objectForKey:[NSString stringWithFormat:@"%d",indexPath.row]];
    if (!cellDidLoad) {
        [cellHeights setObject:[NSString stringWithFormat:@"%.2f",newHeight ] forKey:[NSString stringWithFormat:@"%d",indexPath.row]];
        [didReloadRowsBools setObject:[NSNumber numberWithBool:YES] forKey:[NSString stringWithFormat:@"%d",indexPath.row]];
        [self.tbl beginUpdates];
        [self.tbl reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tbl endUpdates];
    }
}


@end
