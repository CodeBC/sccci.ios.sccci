//
//  BasedWithWebViewViewController.h
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasedWithWebViewViewController : BasedViewController
@property (nonatomic,strong) UIWebView * webView;
- (void) hideTopBanner;
@end
