//
//  SpeakerAboutTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 6/18/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SpeakerAboutTableViewCell.h"
#import "StringTable.h"
#import "UIImageView+WebCache.h"
#import "NSString+HTML.h"
@interface SpeakerAboutTableViewCell()
@property (nonatomic,strong) UIImageView * imgProfileView;
@property (nonatomic,strong) UILabel * lblName;
@property (nonatomic,strong) UILabel * lblCompany;
@property (nonatomic,strong) UILabel * lblJobDescription;
@property (nonatomic,strong) UILabel * lblAbout;
@end
@implementation SpeakerAboutTableViewCell
- (UIImageView *) imgProfileView{
    if (!_imgProfileView) {
        _imgProfileView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 10, 90, 90)];
        _imgProfileView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imgProfileView;
}

- (UILabel *)lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(self.imgProfileView.frame.origin.x + self.imgProfileView.frame.size.width + 10, 10, 189, 23)];
        _lblName.font = [UIFont fontWithName:ROBOTO_BOLD size:14];
        _lblName.textColor = [UIColor colorWithHexString:@"01457b"];
    }
    return _lblName;
}

- (UILabel *) lblCompany{
    if (!_lblCompany) {
        _lblCompany = [[UILabel alloc] init];
        _lblCompany.font = [UIFont fontWithName:ROBOTO_REGUALAR size:13];
        _lblCompany.textColor = [UIColor colorWithHexString:@"333333"];
        _lblCompany.numberOfLines = 0;
        _lblCompany.contentMode = UIViewContentModeTop;
        _lblCompany.lineBreakMode = UILineBreakModeWordWrap;
    }
    return _lblCompany;
}

- (UILabel *) lblJobDescription{
    if (!_lblJobDescription) {
        _lblJobDescription = [[UILabel alloc] initWithFrame:CGRectMake(self.imgProfileView.frame.origin.x + self.imgProfileView.frame.size.width + 10, self.lblCompany.frame.origin.y + self.lblCompany.frame.size.height + 5, 180, 200)];
        _lblJobDescription.font = [UIFont fontWithName:ROBOTO_REGUALAR size:12];
        _lblJobDescription.textColor = [UIColor colorWithHexString:@"666666"];
        _lblJobDescription.numberOfLines = 0;
        _lblJobDescription.contentMode = UIViewContentModeTop;
        _lblJobDescription.lineBreakMode = UILineBreakModeWordWrap;
    }
    return _lblJobDescription;
}

- (UILabel *) lblAbout{
    if (!_lblAbout) {
        _lblAbout = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.imgProfileView.frame.origin.y + self.imgProfileView.frame.size.height + 30, 290, 0)];
        _lblAbout.font = [UIFont fontWithName:ROBOTO_REGUALAR size:12];
        _lblAbout.textColor = [UIColor colorWithHexString:@"333333"];
        _lblAbout.numberOfLines = 0;
    }
    return _lblAbout;
}

- (void) setUpViews{
    [self addSubview:self.imgProfileView];
    [self addSubview:self.lblName];
    [self addSubview:self.lblCompany];
    [self addSubview:self.lblJobDescription];
    [self addSubview:self.lblAbout];
}

- (void) viewLoadWithName:(NSString *)strName andJob:(NSString *)strJob andAbout:(NSString *)strAbout andImageURL:(NSString *)strImageUrl andCompany:(NSString *)strCompanyName{
    [self.imgProfileView setImageWithURL:[NSURL URLWithString:strImageUrl] placeholderImage:nil];
    self.lblName.text = strName;
    self.lblCompany.text = [NSString stringWithFormat:@"%@\n\n%@", strCompanyName, strJob];
    [self.lblCompany sizeToFit];
    
    CGRect newFrame = self.lblAbout.frame;
    newFrame.size.height = [SpeakerAboutTableViewCell heightForCellWithPost:strAbout];
    self.lblAbout.frame = newFrame;
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[strAbout dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.lblAbout.text = [self stringByStrippingHTML:[strAbout stringByDecodingHTMLEntities]];
}

-(NSString *) stringByStrippingHTML: (NSString *) s {
    NSRange r;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    
    return [s stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    self.lblCompany.frame = CGRectMake(self.imgProfileView.frame.origin.x + self.imgProfileView.frame.size.width + 10, self.lblName.frame.origin.y + self.lblName.frame.size.height, 180, 130);
    [self.lblCompany sizeToFit];
}

+ (CGFloat)heightForCellWithPost:(NSString *)strAbout {
    NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:ROBOTO_REGUALAR size:12.0f] forKey: NSFontAttributeName];
    
    CGSize sizeToFit  = [strAbout boundingRectWithSize:CGSizeMake(290.0f, CGFLOAT_MAX)
                                                     options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:stringAttributes context:nil].size;
    return fmaxf(50.0f, sizeToFit.height+ 5);
}

+ (CGFloat)heightForCellAndProfileImageWithAbout:(NSString *)strAbout {
    NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:ROBOTO_REGUALAR size:12.0f] forKey: NSFontAttributeName];
    
    CGSize sizeToFit  = [strAbout boundingRectWithSize:CGSizeMake(290.0f, CGFLOAT_MAX)
                                               options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                            attributes:stringAttributes context:nil].size;
    
    return fmaxf(50.0f, sizeToFit.height+ 30 + 110);
}

@end
