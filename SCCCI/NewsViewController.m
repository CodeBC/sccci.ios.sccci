//
//  NewsViewController.m
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "NewsViewController.h"
#import "News.h"
#import "AppDelegate.h"
#import "ServiceMappingClient.h"
#import "StringTable.h"
#import "ExhibitorListTableViewCell.h"
#import "NewsDetailViewController.h"

@interface NewsViewController ()
@property (nonatomic,strong) NSArray * arrList;
@property (nonatomic,strong) NewsDetailViewController * newsDetailViewController;
@end

@implementation NewsViewController

- (NewsDetailViewController *) newsDetailViewController{
    if (!_newsDetailViewController) {
        _newsDetailViewController = [[NewsDetailViewController alloc] init];
    }
    return _newsDetailViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"News"];
    
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
}

- (void)viewWillAppear:(BOOL)animated{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    self.arrList = [client getNews];
    [self.tbl reloadData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getPushNoti:) name:@"pushNotiArrived" object:nil];
}


- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pushNotiArrived" object:nil];
}

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"aboutCell";
	ExhibitorListTableViewCell *cell = (ExhibitorListTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[ExhibitorListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    News * obj = [self.arrList   objectAtIndex:indexPath.row];
    NSInteger lang = [StringTable getCurrentLangage];
    if (lang == LANGAUGE_EGN) {
        [cell viewLoadWithString:obj.news_title];
    }
    else if (lang == LANGAUGE_CN){
        [cell viewLoadWithString:obj.news_title_cn];
    }
    
    UIImageView * indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
    indicatorView.image = [UIImage imageNamed:@"cell_indicator"];
    cell.accessoryView = indicatorView;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrList count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.newsDetailViewController.news = [self.arrList objectAtIndex:indexPath.row];
    [self.navigationController  pushViewController:self.newsDetailViewController animated:YES];
}

- (void) getPushNoti:(NSNotification *) notification{
    NSLog(@"THE NOTIFICATION IS %@",notification.userInfo);
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    self.arrList = [client getNews];
    [self.tbl reloadData];
    
    
}



@end
