//
//  SponsersViewController.m
//  SCCCI
//
//  Created by Zayar on 6/18/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SponsersViewController.h"
#import "SponsorListTableViewCell.h"
#import "StringTable.h"
#import "Utility.h"
#import "OrganiserDetailViewController.h"
#import "AppDelegate.h"
#import "Sponsor.h"
#import "SponsorTypes.h"
@interface SponsersViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tbl;
@property (strong, nonatomic) NSMutableArray * arrSectionTitles;
@property (strong, nonatomic) OrganiserDetailViewController * organiserDetailViewController;
@end

@implementation SponsersViewController
@synthesize strTitle;
- (OrganiserDetailViewController *) organiserDetailViewController{
    if (!_organiserDetailViewController) {
        _organiserDetailViewController = [[OrganiserDetailViewController alloc] init];
    }
    return _organiserDetailViewController;
}

- (NSMutableArray *) arrSectionTitles{
    if (!_arrSectionTitles) {
        _arrSectionTitles = [[NSMutableArray alloc] init];
    }
    return _arrSectionTitles;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
    [self.tbl reloadData];
    [self setNavBarTitle:@"Sponsors"];
}

- (void)viewWillAppear:(BOOL)animated{
    [self setNavBarTitle:strTitle];
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    NSArray * arrTemp = [client getSponsorTypes];
    if([arrTemp count]) [self.arrSectionTitles removeAllObjects];
    for (SponsorTypes * sponType in arrTemp) {
        
        NSDictionary * dicSponsors = @{@"title":sponType.type_name,@"sponsors":[client getSponsorsByTypeId:sponType.type_id]};
        NSLog(@"getSponsorsByTypeId count %d",[client getSponsorsByTypeId:sponType.type_id].count);
        [self.arrSectionTitles addObject:dicSponsors];
    }
    
    [self.tbl reloadData];
}

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SponsorListTableViewCell";
	SponsorListTableViewCell *cell = (SponsorListTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[SponsorListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    Sponsor * obj;
    NSDictionary * dic = [self.arrSectionTitles objectAtIndex:indexPath.section];
    NSArray * arr = [dic objectForKey:@"sponsors"];
    obj = [arr objectAtIndex:indexPath.row];
    NSInteger lang = [StringTable getCurrentLangage];
    if (lang == LANGAUGE_EGN) {
        [cell viewLoadWithName:obj.sponsor_name andDescription:obj.sponsor_booth andImageName:obj.sponsor_logo];
    }
    else if (lang == LANGAUGE_CN){
        [cell viewLoadWithName:obj.sponsor_name_cn andDescription:obj.sponsor_booth andImageName:obj.sponsor_logo];
    }
    
    UIImageView * indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
    indicatorView.image = [UIImage imageNamed:@"cell_indicator"];
    cell.accessoryView = indicatorView;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 57;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    #warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSDictionary * dic = [self.arrSectionTitles objectAtIndex:section];
    NSArray * arr = [dic objectForKey:@"sponsors"];
    return [arr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    #warning Potentially incomplete method implementation.
    // Return the number of sections.
    return [self.arrSectionTitles count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSLog(@"viewForHeaderInSection");
    NSString * strTitle;
    strTitle=@"";
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
    [v setBackgroundColor:[UIColor colorWithHexString:@"ebeaf0"]];
    UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-20, 35)];
    lblTitle.font = [UIFont fontWithName:ROBOTO_BOLD size:15];
    lblTitle.textColor = [UIColor colorWithHexString:@"01457b"];
    NSDictionary * dic = [self.arrSectionTitles objectAtIndex:section];
    lblTitle.text = [dic objectForKey:@"title"];
    [v addSubview:lblTitle];
    [Utility makeBorder:v andWidth:0.5 andColor:[UIColor colorWithHexString:@"bfbfbf"] andCornerRadius:0];
    
    return v;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 35;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Sponsor * obj;
    NSDictionary * dic = [self.arrSectionTitles objectAtIndex:indexPath.section];
    NSArray * arr = [dic objectForKey:@"sponsors"];
    obj = [arr objectAtIndex:indexPath.row];
    self.organiserDetailViewController.sponsor = obj;
    [self.navigationController pushViewController:self.organiserDetailViewController animated:YES];
}

@end
