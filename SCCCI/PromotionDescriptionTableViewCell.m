//
//  PromotionDescriptionTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "PromotionDescriptionTableViewCell.h"
#import "StringTable.h"
@interface PromotionDescriptionTableViewCell()
@property (nonatomic, strong) UILabel * capHowItWork;
@property (nonatomic, strong) UILabel * lblAbout;
@end
@implementation PromotionDescriptionTableViewCell
- (UILabel *) capHowItWork{
    if (!_capHowItWork) {
        _capHowItWork = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 3, 290, 30)];
        _capHowItWork.font = [UIFont fontWithName:ROBOTO_REGUALAR size:16];
        _capHowItWork.textColor = [UIColor colorWithHexString:@"333333"];
        _capHowItWork.text = @"How it works:";
    }
    return _capHowItWork;
}

- (UILabel *) lblAbout{
    if (!_lblAbout) {
        _lblAbout = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.capHowItWork.frame.origin.y + self.capHowItWork.frame.size.height , 290, 0)];
        _lblAbout.font = [UIFont fontWithName:ROBOTO_REGUALAR size:12];
        _lblAbout.textColor = [UIColor colorWithHexString:@"333333"];
        _lblAbout.numberOfLines = 0;
    }
    return _lblAbout;
}

- (void) setUpViews{
    [self addSubview:self.capHowItWork];
    [self addSubview:self.lblAbout];
}

- (void) viewLoadWithAbout:(NSString *)strAbout{
    CGRect newFrame = self.lblAbout.frame;
    newFrame.size.height = [PromotionDescriptionTableViewCell heightForCellWithPost:strAbout];
    NSLog(@"lblAbout height %f",newFrame.size.height);
    self.lblAbout.frame = newFrame;
    self.lblAbout.text = strAbout;
}

+ (CGFloat)heightForCellWithPost:(NSString *)strAbout {
    NSLog(@"str about %@",strAbout);
    NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:ROBOTO_REGUALAR size:12.0f] forKey: NSFontAttributeName];
    
    CGSize sizeToFit  = [strAbout boundingRectWithSize:CGSizeMake(290.0f, CGFLOAT_MAX)
                                               options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                            attributes:stringAttributes context:nil].size;
    return fmaxf(30.0f, sizeToFit.height+ 5);
}

+ (CGFloat)heightForCellAndCapWithAbout:(NSString *)strAbout {
    NSLog(@"str about CapWith%@",strAbout);
    NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:ROBOTO_REGUALAR size:12.0f] forKey: NSFontAttributeName];
    
    CGSize sizeToFit  = [strAbout boundingRectWithSize:CGSizeMake(290.0f, CGFLOAT_MAX)
                                               options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                            attributes:stringAttributes context:nil].size;
    return fmaxf(30.0f, sizeToFit.height+ 5 + 40);
}
@end
