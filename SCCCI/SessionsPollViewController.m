//
//  SessionsPollViewController.m
//  SCCCI
//
//  Created by Zayar on 6/26/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SessionsPollViewController.h"
#import "SimpleNameTableViewCell.h"
#import "SessionPollViewController.h"
@interface SessionsPollViewController ()
@property (strong, nonatomic) NSArray * arrSessionTitles;
@property (strong,nonatomic) SessionPollViewController * sessionPollViewController;
@end

@implementation SessionsPollViewController

- (SessionPollViewController *)sessionPollViewController{
    if (!_sessionPollViewController) {
        _sessionPollViewController = [[SessionPollViewController alloc] init];
    }
    return _sessionPollViewController;
}

- (NSArray *) arrSessionTitles{
    if (!_arrSessionTitles) {
        _arrSessionTitles = [NSArray arrayWithObjects: @"The Changing Face of Karat", @"Infections Around the World",nil];
    }
    return _arrSessionTitles;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
    [self.tbl reloadData];
    
    [self setNavBarTitle:@"Sessions Poll"];
}

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SimpleNameTableViewCell";
	SimpleNameTableViewCell *cell = (SimpleNameTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[SimpleNameTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    
    [cell viewLoadWithString:[self.arrSessionTitles   objectAtIndex:indexPath.row]];
    UIImageView * indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
    indicatorView.image = [UIImage imageNamed:@"cell_indicator"];
    cell.accessoryView = indicatorView;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrSessionTitles count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.sessionPollViewController.strSessionName = [self.arrSessionTitles objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:self.sessionPollViewController animated:YES];
}

@end
