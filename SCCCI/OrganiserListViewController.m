//
//  OrganiserListViewController.m
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "OrganiserListViewController.h"
#import "OrganiserLogoTableViewCell.h"
#import "OrganiserDetailViewController.h"
#import "StringTable.h"
#import "Utility.h"
@interface OrganiserListViewController ()
@property (strong, nonatomic) NSArray * arrSectionTitles;
@property (strong, nonatomic) NSArray * arrOrganiserName;
@property (strong, nonatomic) OrganiserDetailViewController * organiserDetailViewController;
@end

@implementation OrganiserListViewController
- (OrganiserDetailViewController *) organiserDetailViewController{
    if (!_organiserDetailViewController) {
        _organiserDetailViewController = [[OrganiserDetailViewController alloc] init];
    }
    return _organiserDetailViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
    [self.tbl reloadData];
    [self setNavBarTitle:@"Organisers/Partners"];
}

- (NSArray *) arrSectionTitles{
    if (!_arrSectionTitles) {
        _arrSectionTitles = [NSArray arrayWithObjects: @"Organisers", @"Partners",nil];
    }
    return _arrSectionTitles;
}

- (NSArray *) arrOrganiserName{
    if (!_arrOrganiserName) {
        _arrOrganiserName = [NSArray arrayWithObjects: @"Logo_Sample", @"Logo_Sample",@"Logo_Sample",nil];
    }
    return _arrOrganiserName;
}

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ProgrammeTableViewCell";
	OrganiserLogoTableViewCell *cell = (OrganiserLogoTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[OrganiserLogoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    
    [cell viewLoadWithString:[self.arrOrganiserName objectAtIndex:indexPath.row]];
    UIImageView * indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
    indicatorView.image = [UIImage imageNamed:@"cell_indicator"];
    cell.accessoryView = indicatorView;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrOrganiserName count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return [self.arrSectionTitles count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSLog(@"viewForHeaderInSection");
    NSString * strTitle;
    strTitle=@"";
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
    [v setBackgroundColor:[UIColor colorWithHexString:@"ebeaf0"]];
    UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-20, 35)];
    lblTitle.font = [UIFont fontWithName:ROBOTO_BOLD size:15];
    lblTitle.textColor = [UIColor colorWithHexString:@"01457b"];
    lblTitle.text = [self.arrSectionTitles objectAtIndex:section];
    [v addSubview:lblTitle];
    [Utility makeBorder:v andWidth:0.5 andColor:[UIColor colorWithHexString:@"bfbfbf"] andCornerRadius:0];
    
    return v;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 35;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.navigationController pushViewController:self.organiserDetailViewController animated:YES];
}

@end
