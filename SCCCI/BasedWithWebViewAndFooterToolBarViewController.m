//
//  BasedWithWebViewAndFooterToolBarViewController.m
//  SCCCI
//
//  Created by Zayar on 7/10/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "BasedWithWebViewAndFooterToolBarViewController.h"
#import "StringTable.h"
@interface BasedWithWebViewAndFooterToolBarViewController ()

@end
#define TOOLBAR_HEIGHT 44
@implementation BasedWithWebViewAndFooterToolBarViewController

- (UIToolbar *) footerToolBar{
    if (!_footerToolBar) {
        _footerToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.webView.frame.origin.y + self.webView.frame.size.height, self.view.frame.size.width, TOOLBAR_HEIGHT)];
    }
    return _footerToolBar;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.webView setFrame:CGRectMake(self.view.frame.origin.x, self.topBannerSilder.frame.origin.y + self.topBannerSilder.frame.size.height, self.view.frame.size.width, (self.view.frame.size.height -(self.topBannerSilder.frame.origin.y + self.topBannerSilder.frame.size.height) - STATUS_BAR_NAV_BAR_HEIGHT) - TOOLBAR_HEIGHT)];
    [self.view addSubview:self.footerToolBar];
}

- (void) hideTopBanner{
    [self.webView setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, (self.view.frame.size.height - STATUS_BAR_NAV_BAR_HEIGHT)-TOOLBAR_HEIGHT)];
    self.footerToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.webView.frame.origin.y + self.webView.frame.size.height, self.view.frame.size.width, TOOLBAR_HEIGHT)];
    self.topBannerSilder.hidden = YES;

    [self hideBasedTopBanner];
}


@end
