//
//  NameWithOptionTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 7/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "NameWithSwitchTableViewCell.h"
#import "StringTable.h"
@interface NameWithSwitchTableViewCell()
@property (nonatomic, strong) UILabel * lblName;
@property (nonatomic, strong) UISwitch * swtSwitch;
@end
@implementation NameWithSwitchTableViewCell
- (UILabel *)lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 5, 220, 30)];
        _lblName.numberOfLines = 2;
        _lblName.font = [UIFont fontWithName:ROBOTO_BOLD size:14];
        _lblName.textColor = [UIColor colorWithHexString:@"333333"];
    }
    return _lblName;
}

- (UISwitch *) swtSwitch{
    if (!_swtSwitch) {
        _swtSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(self.lblName.frame.origin.x + self.lblName.frame.size.width+5, 5, 40, 30)];
        
    }
    return _swtSwitch;
}

- (void)setUpViews{
    [self addSubview:self.lblName];
    [self addSubview:self.swtSwitch];
}

- (void)viewLoadWithString:(NSString *)strName andSwitch:(BOOL)isSwitch{
    [self.lblName setText:strName];
    [self.swtSwitch setOn:isSwitch];
    
}


@end
