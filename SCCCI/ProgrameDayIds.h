//
//  ProgrameDayIds.h
//  SCCCI
//
//  Created by Zayar on 7/13/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"
@interface ProgrameDayIds : MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (nonatomic, retain) NSNumber * day_id;
@property (nonatomic, retain) NSString * day_name;
+ (void) deleteAllWithContext: (NSManagedObjectContext *) context;
@end
