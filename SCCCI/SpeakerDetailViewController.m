//
//  SpeakerDetailViewController.m
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SpeakerDetailViewController.h"
#import "SpeakerAboutTableViewCell.h"
#import "StringTable.h"
#import "Utility.h"
#import "ProgrammeTableViewCell.h"
#import "StringTable.h"
#import "AppDelegate.h"
#import "ProgrameDayIds.h"
#import "ProgramDetailViewController.h"
@interface SpeakerDetailViewController ()
{
    NSString * strAbout;
    NSString * strPrograms;
}
@property (strong, nonatomic) IBOutlet UITableView *tbl;
@property (strong, nonatomic) NSArray * arrDayTitles;// of days
@property (strong, nonatomic) NSMutableArray * arrSessionTitles;
@property (strong, nonatomic) ProgramDetailViewController * programDetailViewController;
@property (nonatomic, strong) UIActivityViewController *activityViewController;
@end

@implementation SpeakerDetailViewController
@synthesize speaker;

/*- (NSArray *) arrDayTitles{
    if (!_arrDayTitles) {
        _arrDayTitles = [NSArray arrayWithObjects: @"Web 20 Aug", @"Thu 21 Aug",nil];
    }
    return _arrDayTitles;
}*/

- (ProgramDetailViewController *) programDetailViewController{
    if (!_programDetailViewController) {
        _programDetailViewController = [[UIStoryboard storyboardWithName:@"Programme" bundle:nil] instantiateViewControllerWithIdentifier:@"ProgramDetail"];
    }
    return _programDetailViewController;
}

- (NSMutableArray *) arrSessionTitles{
    if (!_arrSessionTitles) {
        _arrSessionTitles = [[NSMutableArray alloc] init];
    }
    return _arrSessionTitles;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"Speakers"];
    //strAbout = @"About text about text About text about text About text about text About text about text About text about text About text about text About text about text About text about text About text about text";
    
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
    
    strPrograms = @"programs";
    [self.tbl reloadData];
    
    UIBarButtonItem * btnShareFb = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStylePlain target:self action:@selector(onShare)];
    self.navigationItem.rightBarButtonItem = btnShareFb;
}

- (void)viewWillAppear:(BOOL)animated{
    //[self.tbl reloadData];
    [self viewLoadWithSpeaker:speaker];
}

- (void)viewLoadWithSpeaker:(Speakers *)obj{
    if (obj) {
        AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        ServiceMappingClient *client = [delegate client];
        self.arrDayTitles = [client getDayIds];
        NSLog(@"speaker id %@",obj.speaker_id);
        if ([self.arrDayTitles count]) {
            [self.arrSessionTitles removeAllObjects];
        }
        for (ProgrameDayIds * progDay in self.arrDayTitles) {
            NSArray * programs = [client getProgrammeByDay_id:progDay.day_id andSpeakeId:obj.speaker_id];
            NSMutableArray * arr = [NSMutableArray arrayWithArray:programs];
            NSDictionary * dic = @{ @"title":progDay.day_name,strPrograms:arr};
            [self.arrSessionTitles addObject:dic];
        }
        if ([self getCurrentLanguage] == LANGAUGE_EGN) {
            strAbout = obj.speaker_description;
        }
        else if ([self getCurrentLanguage] == LANGAUGE_CN){
            strAbout = obj.speaker_description_cn;
        }
        
        [self.tbl reloadData];
    }
}

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"aboutCell";
        SpeakerAboutTableViewCell *cell = (SpeakerAboutTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[SpeakerAboutTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setUpViews];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
    
        if ([self getCurrentLanguage] == LANGAUGE_EGN) {
            [cell viewLoadWithName:speaker.speaker_name andJob:speaker.speaker_designation andAbout:speaker.speaker_description andImageURL:speaker.speaker_avatar andCompany:speaker.speaker_company];
        }
        else if ([self getCurrentLanguage] == LANGAUGE_CN){
            [cell viewLoadWithName:speaker.speaker_name_cn andJob:speaker.speaker_designation_cn andAbout:speaker.speaker_description_cn andImageURL:speaker.speaker_avatar andCompany:speaker.speaker_company_cn];
        }
        
        
        return cell;
    }
    else {
        static NSString *CellIdentifier = @"ProgrammeTableViewCell";
        ProgrammeTableViewCell *cell = (ProgrammeTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[ProgrammeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setUpTheViews];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
        cell.owner = self;
        
        NSDictionary * dic = [self.arrSessionTitles objectAtIndex:indexPath.section-1];
        NSArray * arr = [dic objectForKey:strPrograms];
        Programme * prog = [arr objectAtIndex:indexPath.row];
        AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        ServiceMappingClient *client = [delegate client];
        prog.is_fav = [client getProgramFavouriteByProgramId:prog.event_id];
        
        if ([self getCurrentLanguage] == LANGAUGE_EGN) {
            [cell loadTheViewWithName:prog.event_name andIsActive:[prog.is_fav boolValue] andSection:indexPath.section andLocation:prog.event_location andTime:[NSString stringWithFormat:@"%@ to %@",prog.start_time,prog.end_time] andLanguage:prog.event_language];
        }
        else if ([self getCurrentLanguage] == LANGAUGE_CN){
            [cell loadTheViewWithName:prog.event_name_cn andIsActive:[prog.is_fav boolValue] andSection:indexPath.section andLocation:prog.event_location andTime:[NSString stringWithFormat:@"%@ to %@",prog.start_time,prog.end_time] andLanguage:prog.event_language];
        }
        
        //[cell loadTheViewWithName:[self.arrSessionTitles   objectAtIndex:indexPath.row] andIsActive:indexPath.row andSection:indexPath.section];
        UIImageView * indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
        indicatorView.image = [UIImage imageNamed:@"cell_indicator"];
        cell.accessoryView = indicatorView;
        return cell;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return [SpeakerAboutTableViewCell heightForCellAndProfileImageWithAbout:strAbout];
    }
    else{
        return 58;
    }
    return 0;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    if (section == 0) {
        return 1;
    }
    else{
        NSDictionary * dic = [self.arrSessionTitles objectAtIndex:section-1];
        NSArray * arr = [dic objectForKey:strPrograms];
        return [arr count];
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return self.arrDayTitles.count + 1;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section != 0) {
        NSDictionary * dic = [self.arrSessionTitles objectAtIndex:indexPath.section-1];
        NSMutableArray * arr = [dic objectForKey:strPrograms];
        self.programDetailViewController.programme = [arr objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:self.programDetailViewController animated:YES];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section != 0) {
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
        [v setBackgroundColor:[UIColor colorWithHexString:@"ebeaf0"]];
        UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width-20, 35)];
        lblTitle.font = [UIFont fontWithName:ROBOTO_BOLD size:15];
        lblTitle.textColor = [UIColor colorWithHexString:@"01457b"];
        ProgrameDayIds * progDay = [self.arrDayTitles objectAtIndex:section-1];
        lblTitle.text = progDay.day_name;
        [v addSubview:lblTitle];
        [Utility makeBorder:v andWidth:0.5 andColor:[UIColor colorWithHexString:@"bfbfbf"] andCornerRadius:0];
        
        return v;
    }
    return nil;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section != 0) {
        return 35;
    }
    return 0;
}

- (void) onProgrammeTableViewCellFav:(ProgrammeTableViewCell *)cell andSection:(NSInteger)section andFav:(BOOL)fav{
    NSDictionary * dic = [self.arrSessionTitles objectAtIndex:section-1];
    NSArray * arr = [dic objectForKey:strPrograms];
    Programme * prog = [arr objectAtIndex:cell.tag];
    prog.is_fav = [NSNumber numberWithBool:fav];
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    [client saveProgrammeFav:prog.event_id andFav:prog.is_fav];
}


- (void)onShare{
    self.activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[[NSString stringWithFormat:@"%@ \n %@",SHARE_MSG,SHARE_LINK]] applicationActivities:nil];
    [self presentViewController:self.activityViewController animated:YES completion:nil];
}

@end
