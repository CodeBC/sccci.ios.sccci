//
//  LuckyDrawResultViewController.m
//  SCCCI
//
//  Created by Zayar on 6/26/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "LuckyDrawResultViewController.h"
#import "StringTable.h"
#import "Utility.h"
@interface LuckyDrawResultViewController ()
@property (nonatomic, strong) UIView * successView;
@property (nonatomic, strong) UIView * failView;
@property (nonatomic, strong) UILabel * lblSuccessTitle;
@property (nonatomic, strong) UILabel * lblSuccessCode;
@property (nonatomic, strong) UILabel * lblFailTitle;
@property (nonatomic, strong) UILabel * lblFailText;
@end

@implementation LuckyDrawResultViewController
@synthesize strSuccessCode;
- (UIView *)successView{
    if (!_successView) {
        _successView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.topBannerSilder.frame.origin.x + self.topBannerSilder.frame.size.height, self.view.frame.size.width, self.view.frame.size.height -(self.topBannerSilder.frame.origin.y + self.topBannerSilder.frame.size.height) - STATUS_BAR_NAV_BAR_HEIGHT)];
        _successView.hidden = YES;
    }
    return _successView;
}

- (UIView *)failView{
    if (!_failView) {
        _failView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.topBannerSilder.frame.origin.x + self.topBannerSilder.frame.size.height, self.view.frame.size.width, self.view.frame.size.height -(self.topBannerSilder.frame.origin.y + self.topBannerSilder.frame.size.height) - STATUS_BAR_NAV_BAR_HEIGHT)];
        _failView.hidden = YES;
    }
    return _failView;
}

- (UILabel *)lblSuccessTitle{
    if (!_lblSuccessTitle) {
        _lblSuccessTitle = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 30, NORMAL_WIDTH, 40)];
        _lblSuccessTitle.font = [UIFont fontWithName:ROBOTO_LIGHT size:20];
        _lblSuccessTitle.textColor = [UIColor colorWithHexString:@"333333"];
        _lblSuccessTitle.text = @"Congratulation! You’ve won!";
        _lblSuccessTitle.textAlignment = NSTextAlignmentCenter;
    }
    return _lblSuccessTitle;
}

- (UILabel *)lblSuccessCode{
    if (!_lblSuccessCode) {
        _lblSuccessCode = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.lblSuccessTitle.frame.origin.y + self.lblSuccessTitle.frame.size.height + 5, NORMAL_WIDTH, 150)];
        _lblSuccessCode.font = [UIFont fontWithName:ROBOTO_LIGHT size:18];
        _lblSuccessCode.textColor = [UIColor colorWithHexString:@"333333"];
        _lblSuccessCode.numberOfLines = 0;
        _lblSuccessCode.textAlignment = NSTextAlignmentCenter;
        //_lblSuccessCode.backgroundColor = [UIColor redColor];
    }
    return _lblSuccessCode;
}

- (UILabel *)lblFailTitle{
    if (!_lblFailTitle) {
        _lblFailTitle = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 50, NORMAL_WIDTH, 40)];
        _lblFailTitle.font = [UIFont fontWithName:ROBOTO_LIGHT size:20];
        _lblFailTitle.textColor = [UIColor colorWithHexString:@"333333"];
        _lblFailTitle.text = @"You did not win.";
        _lblFailTitle.textAlignment = NSTextAlignmentCenter;
        _lblFailTitle.textColor = [UIColor redColor];
    }
    return _lblFailTitle;
}

- (UILabel *)lblFailText{
    if (!_lblFailText) {
        _lblFailText = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.lblFailTitle.frame.origin.y + self.lblFailTitle.frame.size.height + 5, NORMAL_WIDTH, 50)];
        _lblFailText.font = [UIFont fontWithName:ROBOTO_LIGHT size:20];
        _lblFailText.textColor = [UIColor colorWithHexString:@"333333"];
        _lblFailText.text = @"Better luck next time!";
        _lblFailText.textAlignment = NSTextAlignmentCenter;
    }
    return _lblFailText;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"Lucky Draw"];
    [self.view addSubview:self.successView];
    [self.view addSubview:self.failView];
    [self.successView addSubview:self.lblSuccessTitle];
    [self.successView addSubview:self.lblSuccessCode];
    [self.failView addSubview:self.lblFailTitle];
    [self.failView addSubview:self.lblFailText];
}

- (void) viewWillAppear:(BOOL)animated{
    if ([Utility stringIsEmpty:strSuccessCode shouldCleanWhiteSpace:YES]) {
        self.failView.hidden = NO;
        self.successView.hidden = YES;
    }
    else{
        self.failView.hidden = YES;
        self.successView.hidden = NO;
        [self setLabelSuccessCode:strSuccessCode];
    }
}

- (void) setLabelSuccessCode:(NSString *)str
{
    NSString * strValue = @"Winning ticket no.\n %@ \n \n Instructions to claim prize";
    self.lblSuccessCode.text = [NSString stringWithFormat:strValue,str];
}

@end
