//
//  PromotionDescriptionTableViewCell.h
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromotionDescriptionTableViewCell : UITableViewCell
+ (CGFloat)heightForCellAndCapWithAbout:(NSString *)strAbout;
+ (CGFloat)heightForCellWithPost:(NSString *)strAbout;
- (void) setUpViews;
- (void) viewLoadWithAbout:(NSString *)strAbout;
@end
