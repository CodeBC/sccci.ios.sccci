//
//  Programme_Detail.h
//  SCCCI
//
//  Created by Zayar on 7/13/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"
@interface Programme_Detail : MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (nonatomic, retain) NSNumber * programme_id;
@property (nonatomic, retain) NSNumber * speaker_id;

@end
