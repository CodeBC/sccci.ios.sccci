//
//  AboutDetailViewController.h
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasedWithWebViewAndFooterToolBarViewController.h"
#import "Info.h"
@interface AboutDetailViewController : BasedWithWebViewAndFooterToolBarViewController
@property (nonatomic,strong) NSString * strTitle;
@property (nonatomic,strong) NSString * strContent;
@property (nonatomic,strong) Info * info;
@end
