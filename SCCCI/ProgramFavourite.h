//
//  ProgramFavourite.h
//  SCCCI
//
//  Created by Zayar on 7/15/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"
@interface ProgramFavourite: MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (nonatomic, retain) NSNumber * programe_id;
@property (nonatomic, retain) NSNumber * is_fav;
+ (NSDictionary *) makeDictionaryWithProgramId:(NSNumber *)program_id andFav:(NSNumber *)isFav;
@end
