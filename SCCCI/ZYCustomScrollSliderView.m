//
//  CustomScrollSliderView.m
//  IACTAC
//
//  Created by Zayar on 10/31/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import "ZYCustomScrollSliderView.h"
#import "UIImageView+AFNetworking.h"
#import "Utility.h"
@implementation ZYCustomScrollSliderView
@synthesize owner;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        CGRect newScrollFrame = frame;
        newScrollFrame.origin.x = 0;
        newScrollFrame.origin.y = 0;
        scrollView = [[UIScrollView alloc] initWithFrame:newScrollFrame];
        [self addSubview:scrollView];
        
        /*CGRect newFrame = frame;
        newFrame.origin.x = 0;
        if ([Utility isGreaterOREqualOSVersion:@"7.0"] ) {
            newFrame.origin.y = frame.size.height-30;
        }
        else{
            newFrame.origin.y = frame.size.height-20;
        }
        newFrame.size.height = 30;
        pageControl = [[UIPageControl alloc] initWithFrame:newFrame];
        [self addSubview:pageControl];*/
        
        NSLog(@"custom scroll initial!!");
    }
    return self;
}

- (void) setUpViewWithFrame:(CGRect)frame{
    // Initialization code
    CGRect newScrollFrame = self.frame;
    newScrollFrame.origin.x = 0;
    newScrollFrame.origin.y = 0;
    scrollView = [[UIScrollView alloc] initWithFrame:newScrollFrame];
    [self addSubview:scrollView];
    
    /*CGRect newFrame = self.frame;
     newFrame.origin.x = 0;
     if ([Utility isGreaterOREqualOSVersion:@"7.0"] ) {
     newFrame.origin.y = frame.size.height-30;
     }
     else{
     newFrame.origin.y = frame.size.height-20;
     }
     newFrame.size.height = 30;
     pageControl = [[UIPageControl alloc] initWithFrame:newFrame];
     [self addSubview:pageControl];*/
    
    NSLog(@"custom scroll initial!!");
}

- (void) loadImageViewWith:(NSArray *)arr{
    
    if (arr != nil) {
        NSLog(@"slider count %lu",(unsigned long)[arr count]);
        NSInteger count=[arr count];
        arrContentView = arr;
        for (UIView * v in scrollView.subviews) {
            if ([v isKindOfClass:[UIImageView class]]) {
                [v removeFromSuperview];
                //[v release];
            }
        }
        scrollView.pagingEnabled=TRUE;
        
        pageControl.numberOfPages=[self calculatePageCountScreenParCount:2 andTotal:count];
        pageControl.currentPage=0;
        [pageControl setPageIndicatorTintColor:[UIColor grayColor]];
        [pageControl setCurrentPageIndicatorTintColor:[UIColor redColor]];
        /*if ([Utility isGreaterOREqualOSVersion:@"7.0"]) {
             [pageControl setCurrentPageIndicatorTintColor:[UIColor grayColor]];
        }*/
       
        currentPagingIndex=0;
        pageControlUsed=TRUE;
        scrollViewMode2=ScrollViewModePaging2;
        scrollView.delegate=self;
        scrollView.showsHorizontalScrollIndicator=FALSE;
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width * [self calculatePageCountScreenParCount:1 andTotal:count], scrollView.frame.size.height)];
        int i = 0;
        for (NSString * strURL in arr) {
            UIButton * btnMovieDetail=[[UIButton alloc]init];
            [btnMovieDetail setFrame:CGRectMake(i*scrollView.frame.size.width, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
            //[btnMovieDetail setTag:page];
            [btnMovieDetail setTag:i];
            [btnMovieDetail addTarget:self action:@selector(clickPromo:) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:btnMovieDetail];
            pageControl.currentPage=i;
            UIImageView * detailImage=[[UIImageView alloc]init];
            [detailImage setFrame:CGRectMake(i*scrollView.frame.size.width, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
            //[detailImage setImageWithURL:[NSURL URLWithString:strURL]];
            [detailImage setImage:[UIImage imageNamed:strURL]];
            [scrollView addSubview:detailImage];
            i ++;
        }
        
        if(myTimer ==nil){
            myTimer=[NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(slideTimely) userInfo:nil repeats:YES];
            slidingIndex=currentPagingIndex;
        }
    }
    
}

- (void) scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale {
	//NSLog(@"scrollViewDidEndZooming");
}

- (void) scrollViewWillBeginDragging:(UIScrollView *)scroll {
    
    if (myTimer != nil) {
        [myTimer invalidate];
        myTimer = nil;
    }
    
    //[myTimer invalidate];
    pageControlUsed = FALSE;
}

- (int) calculatePageCountScreenParCount:(int)itemOnScreen andTotal:(int)total{
    int onCount = total/itemOnScreen;
    int onCountRemind = total % itemOnScreen;
    return onCount + onCountRemind;
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scroll {
    NSLog(@"scrollViewDidEndDecelerating");
    
    CGFloat pageWidth = scroll.frame.size.width/2;
    int page = floor((scroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage=page;
    currentPagingIndex=page;
    
    if(myTimer==nil){
        myTimer=[NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(slideTimely) userInfo:nil repeats:YES];
        slidingIndex=currentPagingIndex;
    }
    
}

- (void) slideTimely{
    slidingIndex++;
    
    if(slidingIndex>[self calculatePageCountScreenParCount:1 andTotal:[arrContentView count]]-1)
        slidingIndex=0;
    [scrollView scrollRectToVisible:CGRectMake(slidingIndex*scrollView.frame.size.width, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height) animated:YES];
    pageControl.currentPage=slidingIndex;
}

- (void) clickPromo:(UIButton *)sender{
    
    NSLog(@" here is click!! ");
    /*ObjectSlide * objS = [delegate.db getPromoById:sender.tag];
    if (![self stringIsEmpty:objS.strPromoLink shouldCleanWhiteSpace:YES]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: objS.strPromoLink]];
    }*/
    [owner onSliderClick:self andTag:sender.tag];
}

- (void) sliderStop{
    if (myTimer != nil) {
        [myTimer invalidate];
        myTimer = nil;
    }
}

- (void) sliderPlay{
    if(myTimer ==nil){
        myTimer=[NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(slideTimely) userInfo:nil repeats:YES];
        slidingIndex=currentPagingIndex;
    }
}

- (void) cleanUpViews{
    if (myTimer != nil) {
        [myTimer invalidate];
        myTimer = nil;
    }
    
    for (UIView * v in arrContentView) {
        if ([v isKindOfClass:[UIImageView class]]) {
            UIImageView * imgView = (UIImageView *)v;
            [imgView removeFromSuperview];
            imgView = nil;
        }
    }
}



@end
