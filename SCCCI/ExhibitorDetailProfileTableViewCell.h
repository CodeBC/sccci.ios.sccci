//
//  ExhibitorDetailProfileTableViewCell.h
//  SCCCI
//
//  Created by Zayar on 6/24/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExhibitorDetailProfileTableViewCell : UITableViewCell
- (void) setUpViews;
- (void) viewLoadWithName:(NSString *)strName andBoothNo:(NSString *)strBoothNo andWebsite:(NSString *)strWebsite andImageURL:(NSString *)strImageUrl andContactNo:(NSString *)strContactNo andContactNo2:(NSString *)strContactNo2;
@end
