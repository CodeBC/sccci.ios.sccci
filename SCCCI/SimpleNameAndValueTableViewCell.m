//
//  SimpleNameAndValueTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 7/7/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SimpleNameAndValueTableViewCell.h"
#import "StringTable.h"
@interface SimpleNameAndValueTableViewCell()
@property (nonatomic, strong) UILabel * lblName;
@property (nonatomic, strong) UILabel * lblValue;
@end
@implementation SimpleNameAndValueTableViewCell
- (UILabel *)lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 5, 220, 30)];
        _lblName.numberOfLines = 2;
        _lblName.font = [UIFont fontWithName:ROBOTO_REGUALAR size:15];
        _lblName.textColor = [UIColor colorWithHexString:@"333333"];
    }
    return _lblName;
}

- (UILabel *)lblValue{
    if (!_lblValue) {
       _lblValue = [[UILabel alloc] initWithFrame:CGRectMake(self.lblName.frame.origin.x + self.lblName.frame.size.width+5, 5, 40, 30)];
        _lblValue.numberOfLines = 2;
        _lblValue.font = [UIFont fontWithName:ROBOTO_LIGHT size:14];
        _lblValue.textColor = [UIColor colorWithHexString:@"333333"];
    }
    return _lblValue;
}

- (void)setUpViews{
    [self addSubview:self.lblName];
    [self addSubview:self.lblValue];
}

- (void)viewLoadWithString:(NSString *)strName andValue:(NSString *)strValue{
    [self.lblName setText:strName];
    [self.lblValue setText:strValue];
}

@end
