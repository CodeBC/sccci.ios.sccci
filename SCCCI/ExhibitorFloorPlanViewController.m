//
//  ExhibitorFootPlanViewController.m
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ExhibitorFloorPlanViewController.h"
#import "AppDelegate.h"
#import "ServiceMappingClient.h"
#import "Exhibitor.h"
#import "ExhibitorDetailViewController.h"
@interface ExhibitorFloorPlanViewController ()
@property(nonatomic, strong) NSArray *arrList;

@end

@implementation ExhibitorFloorPlanViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self hideTopBanner];
    // Do any additional setup after loading the view.
    NSURL *htmlURL = [[NSBundle mainBundle] URLForResource:@"venue/venue" withExtension:@"html"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:htmlURL]];
    
    self.navigationItem.title = @"Exhibitor Floorplan";
    self.webView.delegate = self;

}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    self.arrList = [client getExhibitors];
    NSString *jsCommand = [NSString stringWithFormat:@"document.body.style.zoom = 1.5;"];
    [self.webView stringByEvaluatingJavaScriptFromString:jsCommand];
    self.webView.scalesPageToFit = YES;
    self.webView.contentMode = UIViewContentModeScaleAspectFit;
    NSLog(@"Exhibitors %@", self.arrList);

    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *jsonString = @"[";
    int i = 0;
    for (Exhibitor *e in self.arrList) {
        NSString* coorsTemp = [e.exhibitor_booth_location stringByReplacingOccurrencesOfString:@"[" withString:@""];
        NSString* coorsTemp2 = [coorsTemp stringByReplacingOccurrencesOfString:@"]" withString:@""];
        if (i == self.arrList.count - 1) {
            NSString *obj = [NSString stringWithFormat:@"{ \"x\": \"%@\", \"y\": \"%@\", \"uri\": \"sccci://exhibitors/%@\", \"text\": \"%@\"}", arr[0], arr[1], e.exhibitor_id, e.exhibitor_name];
            jsonString = [jsonString stringByAppendingString:obj];
            
        } else {
            NSString *obj = [NSString stringWithFormat:@"{ \"x\": \"%@\", \"y\": \"%@\", \"uri\": \"sccci://exhibitors/%@\", \"text\": \"%@\"},", arr[0], arr[1], e.exhibitor_id, e.exhibitor_name];
            jsonString = [jsonString stringByAppendingString:obj];
            
        }
        i++;
    }
    NSString *json = [jsonString stringByAppendingString:@"]"];
    NSLog(@"Here %@", json);
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.data=%@", json]];
    [self.webView stringByEvaluatingJavaScriptFromString:@"window.init()"];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"load request %@",[request URL]);
    if([[[request URL] absoluteString] rangeOfString:@"sccci://exhibitors/"].location != NSNotFound)
    {
        AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        ServiceMappingClient *client = [delegate client];
        
        NSArray *exhibitorIdArr = [[[request URL] absoluteString] componentsSeparatedByString:@"sccci://exhibitors/"];
        NSString *exhibitorId = [exhibitorIdArr objectAtIndex:1];
        ExhibitorDetailViewController * exhibitorDetailViewController = [[ExhibitorDetailViewController alloc] init];
        exhibitorDetailViewController.exhibitor = [client getExhibitorsByID:exhibitorId];
        [self.navigationController  pushViewController:exhibitorDetailViewController animated:YES];
        
        return false;
    }
    return true;
}

@end
