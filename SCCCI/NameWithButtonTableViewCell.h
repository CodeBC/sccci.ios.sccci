//
//  NameWithButtonTableViewCell.h
//  SCCCI
//
//  Created by Zayar on 7/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol NameWithButtonTableViewCellDelegate;
@interface NameWithButtonTableViewCell : UITableViewCell
@property id<NameWithButtonTableViewCellDelegate> owner;
- (void)viewLoadWithString:(NSString *)strName andSwitch:(BOOL)isSwitch;
- (void)setUpViews;
@end

@protocol NameWithButtonTableViewCellDelegate
- (void) onLanguageClick:(UIButton *) sender;
@end
