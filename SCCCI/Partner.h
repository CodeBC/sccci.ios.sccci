//
//  Partner.h
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"
@interface Partner: MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (nonatomic, retain) NSString * partners_clinic;
@property (nonatomic, retain) NSString * partners_clinic_cn;
+ (void) deleteAllWithContext: (NSManagedObjectContext *) context;
@end
