//
//  BasedImageTextDescriptionViewController.h
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTLabel.h"
@interface BasedImageTextDescriptionViewController : BasedViewController
@property (nonatomic,strong) UIImageView * imgView;
@property (nonatomic,strong) RTLabel * lblName;
@property (nonatomic,strong) RTLabel * lblDescription;
@end
