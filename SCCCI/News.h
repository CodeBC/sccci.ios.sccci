//
//  News.h
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"
@interface News: MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (nonatomic, retain) NSNumber * news_id;
@property (nonatomic, retain) NSString * news_title;
@property (nonatomic, retain) NSString * news_description;
@property (nonatomic, retain) NSString * news_title_cn;
@property (nonatomic, retain) NSString * news_description_cn;

+ (void) deleteAllWithContext: (NSManagedObjectContext *) context;

@end
