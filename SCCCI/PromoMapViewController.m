//
//  PromoMapViewController.m
//  SCCCI
//
//  Created by Ye Myat Min on 12/8/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "PromoMapViewController.h"
#import "Promotion.h"
#import "AppDelegate.h"
#import "ServiceMappingClient.h"
#import "ExhibitorDetailViewController.h"

@interface PromoMapViewController ()

@end

@implementation PromoMapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self hideTopBanner];
    // Do any additional setup after loading the view.
    NSURL *htmlURL = [[NSBundle mainBundle] URLForResource:@"venue/venue" withExtension:@"html"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:htmlURL]];
    
    self.navigationItem.title = @"Exhibitor Floorplan";
    self.webView.delegate = self;
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    NSString *jsCommand = [NSString stringWithFormat:@"document.body.style.zoom = 1.5;"];
    [self.webView stringByEvaluatingJavaScriptFromString:jsCommand];
    self.webView.scalesPageToFit = YES;
    self.webView.contentMode = UIViewContentModeScaleAspectFit;
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *jsonString = @"[";
    NSString* coorsTemp = [self.promotion.promotion_coordinates stringByReplacingOccurrencesOfString:@"[" withString:@""];
    NSString* coorsTemp2 = [coorsTemp stringByReplacingOccurrencesOfString:@"]" withString:@""];
    NSArray *arr = [coorsTemp2 componentsSeparatedByString:@","];

    NSString *obj = [NSString stringWithFormat:@"[{ \"x\": \"%@\", \"y\": \"%@\", \"uri\": \"sccci://exhibitors/%@\", \"text\": \"%@\"}]", arr[0], arr[1],self.promotion.promotion_exhibitor_id, self.promotion.promotion_exhibitor_name];

    NSString *json = obj;
    NSLog(@"Here %@", json);
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.data=%@", json]];
    [self.webView stringByEvaluatingJavaScriptFromString:@"window.  init()"];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"load request %@",[request URL]);
    if([[[request URL] absoluteString] rangeOfString:@"sccci://exhibitors/"].location != NSNotFound)
    {
        AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        ServiceMappingClient *client = [delegate client];
        
        NSArray *exhibitorIdArr = [[[request URL] absoluteString] componentsSeparatedByString:@"sccci://exhibitors/"];
        NSString *exhibitorId = [exhibitorIdArr objectAtIndex:1];
        ExhibitorDetailViewController * exhibitorDetailViewController = [[ExhibitorDetailViewController alloc] init];
        exhibitorDetailViewController.exhibitor = [client getExhibitorsByID:exhibitorId];
        [self.navigationController  pushViewController:exhibitorDetailViewController animated:YES];
        
        return false;
    }
    return true;
}


@end
