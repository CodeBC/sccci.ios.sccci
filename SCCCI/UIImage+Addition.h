//
//  UIImage+Addition.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Addition)

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

@end
