//
//  PromotionTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "PromotionTableViewCell.h"
#import "StringTable.h"
#import "UIImageView+WebCache.h"
#import "NSString+HTML.h"
@interface PromotionTableViewCell()
@property (nonatomic,strong) UIImageView * imgCircleView;
@property (nonatomic,strong) UILabel * lblName;
@property (nonatomic,strong) UILabel * lblDescription;
@end
@implementation PromotionTableViewCell
- (UIImageView *) imgCircleView{
    if (!_imgCircleView) {
        _imgCircleView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 5, 37, 37)];
    }
    return _imgCircleView;
}

- (UILabel *)lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(self.imgCircleView.frame.origin.x + self.imgCircleView.frame.size.width + 5, 5, 320 - self.imgCircleView.frame.origin.x + self.imgCircleView.frame.size.width - 120, 20)];
        _lblName.numberOfLines = 1;
        _lblName.font = [UIFont fontWithName:ROBOTO_BOLD size:15];
        _lblName.textColor = [UIColor colorWithHexString:@"01457b"];
        //_lblName.textInputMode = NSText
    }
    return _lblName;
}

- (UILabel *) lblDescription{
    if (!_lblDescription) {
        _lblDescription = [[UILabel alloc] initWithFrame:CGRectMake(self.imgCircleView.frame.origin.x + self.imgCircleView.frame.size.width + 5, self.lblName.frame.origin.y + self.lblName.frame.size.height, 320 -  self.imgCircleView.frame.origin.x + self.imgCircleView.frame.size.width - 120, 18)];
        _lblDescription.font = [UIFont fontWithName:ROBOTO_REGUALAR size:12];
        _lblDescription.textColor = [UIColor colorWithHexString:@"333333"];
        _lblDescription.numberOfLines = 1;
    }
    return _lblDescription;
}

-(NSString *) stringByStrippingHTML: (NSString *) s {
    NSRange r;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return [s stringByDecodingHTMLEntities];
}

- (void) setUpViews{
    [self addSubview:self.imgCircleView];
    [self addSubview:self.lblName];
    [self addSubview:self.lblDescription];
}

- (void) viewLoadWithName:(NSString *)strName andDescription:(NSString *)strDescription andImageName:(NSString *)strImage{
    [self.imgCircleView setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"promotion_circle"]];
    self.lblName.text = strName;
    self.lblDescription.text = strDescription;
}

@end
