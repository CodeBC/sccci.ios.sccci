//
//  BasedViewController.m
//  SCCCI
//
//  Created by Zayar on 6/13/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "BasedViewController.h"
#import "StringTable.h"
#import "ZYCustomScrollSliderView.h"
@interface BasedViewController ()


@end
#define TOP_BANNER_HEIGHT 98.0f
@implementation BasedViewController

- (NSArray *)arrBannerSilder{
    if (!_arrBannerSilder) {
        _arrBannerSilder = [NSArray arrayWithObjects:@"Banner_0", @"Banner_1",@"Banner_2",@"Banner_3",@"Banner_4",@"Banner_5", @"Banner_6", @"Banner_7", @"Banner_19", @"Banner_8", @"Banner_9", @"Banner_10",
                            @"Banner_11", @"Banner_12", @"Banner_13", @"Banner_14", @"Banner_15", @"Banner_16", @"Banner_18", nil];
    }
    return _arrBannerSilder;
}

- (NSArray *)arrBannerUrl{
    if (!_arrBannerUrl) {
        _arrBannerUrl = [NSArray arrayWithObjects: @"http://www.sccci.org.sg/smeicc", @"http://www.anz.com/singapore/en/commercial/products-services/", @"http://www.balancedconsultancy.com", @"http://www.bankofchina.com/sg", @"https://www.dbs.com.sg/sme/dbs-forms/accounts/account-opening.page", @"http://www.deskera.sg", @"http://www.hlf.com.sg", @"http://www.hp.com.sg/server", @"http://www.ibm.com/events/sccciconference", @"http://shopap.lenovo.com/sg/en/tablets/thinkpad/thinkpad-8/", @"http://www.m1.com.sg/business", @"http://www.ocbc.com.sg/business-banking/", @"http://www.rhbbank.com.sg/bizpower/quad.html", @"http://www.sap.com/sea/solution/sme.html", @"https://www.sc.com/sg/sme/", @"http://singpost.com/sme-expo", @"http://info.singtel.com/business", @"http://www.starhub.com/business/promo", @"http://www.uob.com.sg/", nil];
    }
    return _arrBannerUrl;
}

- (ZYCustomScrollSliderView *)topBannerSilder{
    if (!_topBannerSilder) {
        _topBannerSilder = [[ZYCustomScrollSliderView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, TOP_BANNER_HEIGHT)];
    }
    return _topBannerSilder;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"ffffff"]];
    [self navBarStyleSetUp];
    self.topBannerSilder.owner = self;
    
    [self.view addSubview:self.topBannerSilder];
    
    [self.topBannerSilder setUpViewWithFrame:CGRectMake(0, 0, self.view.frame.size.width, TOP_BANNER_HEIGHT)];
    [self.topBannerSilder loadImageViewWith:self.arrBannerSilder];
}

- (void) navBarStyleSetUp{
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.view.backgroundColor = [UIColor colorWithHexString:@"02457c"];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"02457c"];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:ROBOTO_LIGHT size:18]}];
    
    self.navigationItem.backBarButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)setNavBarTitle:(NSString *)str{
    self.navigationItem.title = str;
}

- (void) viewDidDisappear:(BOOL)animated{
    if (self.topBannerSilder) {
        [self.topBannerSilder sliderStop];
    }
}

- (void) hideBasedTopBanner{
    self.topBannerSilder.hidden = YES;
    [self.topBannerSilder cleanUpViews];
    NSLog(@"Based hide!!");
}

- (NSInteger) getCurrentLanguage{
    NSInteger lang = [StringTable getCurrentLangage];
    return lang;
}

- (void) onSliderClick:(ZYCustomScrollSliderView *)sliderView andTag:(NSInteger)tag{
    NSString * str = [self.arrBannerUrl objectAtIndex:tag];
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:str]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    } else {
        NSLog(@"Can't open this url:// %@",str);
    }
}

@end
