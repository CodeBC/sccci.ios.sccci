//
//  NewsDetailViewController.h
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "BasedWithWebViewViewController.h"
#import "News.h"
@interface NewsDetailViewController : BasedWithWebViewViewController
@property (nonatomic,strong) News * news;
@end
