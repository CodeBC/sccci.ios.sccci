//
//  ProgramDetailViewController.h
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Programme.h"
@interface ProgramDetailViewController : BasedViewController
@property (nonatomic,strong) Programme * programme;
@end
