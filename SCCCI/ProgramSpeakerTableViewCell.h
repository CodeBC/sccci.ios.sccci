//
//  ProgramSpeakerTableViewCell.h
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgramSpeakerTableViewCell : UITableViewCell
- (void) setUpView;
- (void) viewLoadWithDescription:(NSString *)strName andJob:(NSString *)strJob;
@end
