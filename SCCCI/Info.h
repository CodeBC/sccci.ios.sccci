//
//  Info.h
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"
@interface Info : MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * title_cn;
@property (nonatomic, retain) NSString * descr;
@property (nonatomic, retain) NSString * descr_cn;
+ (void) deleteAllWithContext: (NSManagedObjectContext *) context;
@end
