//
//  ServiceClient.m
//  SCCCI
//
//  Created by Zayar on 6/27/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ServiceMappingClient.h"
#import "StringTable.h"
#import "Speakers.h"
#import "Sponsor.h"
#import "Exhibitor.h"
#import "SponsorTypes.h"
#import "Programme.h"
#import "Programme_Detail.h"
#import "ProgrameDayIds.h"
#import "ProgrammeTime.h"
#import "ExhibitorCates.h"
#import "ExhibitorTypes.h"
#import "InfoCates.h"
#import "Info.h"
#import "About.h"
#import "Promotion.h"
#import "News.h"
#import "ProgramFavourite.h"
#import "Partner.h"
@interface ServiceMappingClient()
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@end
@implementation ServiceMappingClient
{
    NSManagedObjectContext *_managedObjectContext;
}

- (NSManagedObjectContext *)managedObjectContext {
    if (!_managedObjectContext) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:[self persistentStoreCoordinator]];
    }
    
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (!_managedObjectModel) {
        _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    }
    
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (!_persistentStoreCoordinator) {
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
        
        NSDictionary *options = @{
                                  NSMigratePersistentStoresAutomaticallyOption : @YES,
                                  NSInferMappingModelAutomaticallyOption : @YES
                                  };
        
        NSError *error = nil;
        NSString *filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        filePath = [filePath stringByAppendingPathComponent:@"SCCCI"];
        [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil
                                                            URL:[NSURL fileURLWithPath:filePath]
                                                        options:options
                                                          error:&error];
        NSLog(@"%@", filePath);
        NSAssert(error == nil, @"Error: %@", [error localizedDescription]);
    }
    
    return _persistentStoreCoordinator;
}

- (id)init {
    
    return [super initWithBaseURL:[NSURL URLWithString:APP_BASED_LINK]];
}

- (void) syncDataWithCompletionBlock:(void (^)(BOOL status)) block {
    dispatch_group_t group = dispatch_group_create();
    
    // Request for speakers
    dispatch_group_enter(group);
    [self syncSpeakerWithCompletionBlock:^(NSArray *results) {
        NSLog(@"Done synchronising Speakers");
        dispatch_group_leave(group);
    } andFailureBlock:^(NSError *error) {
        dispatch_group_leave(group);
    }];
    
    // Request for sponsors
    dispatch_group_enter(group);
    [self syncSponsorWithCompletionBlock:^(NSArray *results) {
        NSLog(@"Done synchronising Sponsors");
        dispatch_group_leave(group);
    } andFailureBlock:^(NSError *error) {
        dispatch_group_leave(group);
    }];
    
    // Request for exhibitor
    dispatch_group_enter(group);
    [self syncExhibitorWithCompletionBlock:^(NSArray *results) {
        NSLog(@"Done synchronising exhibitor");
        dispatch_group_leave(group);
    }andFailureBlock:^(NSError *error){
        dispatch_group_leave(group);
    }];
    
    // Request for programme
    dispatch_group_enter(group);
    [self syncProgrammeWithCompletionBlock:^(NSArray *results) {
        NSLog(@"Done synchronising programme");
        dispatch_group_leave(group);
    }andFailureBlock:^(NSError *error){
        dispatch_group_leave(group);
    }];
    
    /*// Request for about
        dispatch_group_enter(group);
    [self syncAboutWithCompletionBlock:^(NSArray *results) {
        NSLog(@"Done synchronising about");
        dispatch_group_leave(group);
    }andFailureBlock:^(NSError *error){
        dispatch_group_leave(group);
    }];*/
    // Request for info
    dispatch_group_enter(group);
    [self syncInfoWithCompletionBlock:^(NSArray *results) {
        NSLog(@"Done synchronising info");
        dispatch_group_leave(group);
    }andFailureBlock:^(NSError *error){
        dispatch_group_leave(group);
    }];
    
    // Request for promotion
    dispatch_group_enter(group);
    [self syncPromotionWithCompletionBlock:^(NSArray *results) {
        NSLog(@"Done synchronising promotion");
        dispatch_group_leave(group);
    }andFailureBlock:^(NSError *error){
        dispatch_group_leave(group);
    }];
    
    // Request for news
    dispatch_group_enter(group);
    [self syncNewsWithCompletionBlock:^(NSArray *results) {
        NSLog(@"Done synchronising news");
        dispatch_group_leave(group);
    }andFailureBlock:^(NSError *error){
        dispatch_group_leave(group);
    }];
    
    // Request for news
    dispatch_group_enter(group);
    [self syncPartnerWithCompletionBlock:^(NSArray *results) {
        NSLog(@"Done synchronising Clinic");
        
        dispatch_group_leave(group);
    }andFailureBlock:^(NSError *error){
        dispatch_group_leave(group);
        
    }];
    
    // Request for news
    dispatch_group_enter(group);
    [self syncGovExhibitorWithCompletionBlock:^(NSArray *results) {
        NSLog(@"Done synchronising GovExhibitor");
        block(true);
        dispatch_group_leave(group);
    }andFailureBlock:^(NSError *error){
        block(true);
        dispatch_group_leave(group);
        
    }];
}

- (void) syncSpeakerWithCompletionBlock:(void (^)(NSArray *results))block andFailureBlock:(void (^)(NSError *error))errorBlock {
    NSParameterAssert(block);
    NSParameterAssert(errorBlock);
    NSLog(@"Syncing Speaker");
    [self GET:SPEAKERS_LINK parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"Speaker return json : %@",responseObject);
        NSArray *speakers = (NSArray *)responseObject;
        [speakers enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            Speakers *speaker = [MTLJSONAdapter modelOfClass:Speakers.class
                                           fromJSONDictionary:obj
                                                        error:NULL];
           [MTLManagedObjectAdapter managedObjectFromModel:speaker
                                                                        insertingIntoContext:[self managedObjectContext]
                                                                                       error:NULL];
        }];
        NSError *modelError;
        [[self managedObjectContext] save: &modelError];
        block(speakers);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) syncSponsorWithCompletionBlock:(void (^)(NSArray *results))block andFailureBlock:(void (^)(NSError *error))errorBlock {
    NSParameterAssert(block);
    NSParameterAssert(errorBlock);
    NSLog(@"Syncing Sponsor");
    [self GET:SPONSORS_LINK parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"Sponsor return json : %@",responseObject);
        NSDictionary * dic = (NSDictionary *)responseObject;
        NSArray *arr = [dic objectForKey:@"sponsors"];
        NSArray *arrST = [dic objectForKey:@"sponsor_types"];
        if ([arr count]) {
            [Sponsor deleteAllWithContext:[self managedObjectContext]];
        }
        
        if ([arrST count]) {
            [SponsorTypes deleteAllWithContext:[self managedObjectContext]];
        }
        
        [arrST enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            
            SponsorTypes *mtlObj = [MTLJSONAdapter modelOfClass:SponsorTypes.class
                                         fromJSONDictionary:obj
                                                      error:NULL];
            [MTLManagedObjectAdapter managedObjectFromModel:mtlObj
                                       insertingIntoContext:[self managedObjectContext]
                                                      error:NULL];
        }];
        
        [arr enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            NSLog(@"sponsor return json %@",obj);
            Sponsor *sponsor = [MTLJSONAdapter modelOfClass:Sponsor.class
                                          fromJSONDictionary:obj
                                                       error:NULL];
            //NSLog(@"sponsor type %@",sponsor.sponsor_type);
            [MTLManagedObjectAdapter managedObjectFromModel:sponsor
                                       insertingIntoContext:[self managedObjectContext]
                                                      error:NULL];
        }];
        
        NSError *modelError;
        [[self managedObjectContext] save: &modelError];
        block(arr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) syncExhibitorWithCompletionBlock:(void (^)(NSArray *results))block andFailureBlock:(void (^)(NSError *error))errorBlock {
    NSParameterAssert(block);
    NSParameterAssert(errorBlock);
    NSLog(@"Syncing Exhibitor");
    [self GET:EXHIBITOR_LINK parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"Exhibitor return json : %@",responseObject);
        NSArray *arr = (NSArray *)responseObject;
        [arr enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            Exhibitor *mtlObj = [MTLJSONAdapter modelOfClass:Exhibitor.class
                                         fromJSONDictionary:obj
                                                      error:NULL];
            [MTLManagedObjectAdapter managedObjectFromModel:mtlObj
                                       insertingIntoContext:[self managedObjectContext]
                                                      error:NULL];
            
            if ([obj objectForKey:@"exhibitor_type"] != [NSNull null]) {
                NSDictionary * dicType = @{@"exhibitor_type":[obj objectForKey:@"exhibitor_type"]};
                ExhibitorTypes * typeObj = [MTLJSONAdapter modelOfClass:ExhibitorTypes.class
                                                  fromJSONDictionary:dicType
                                                               error:NULL];
                [MTLManagedObjectAdapter managedObjectFromModel:typeObj
                                           insertingIntoContext:[self managedObjectContext]
                                                          error:NULL];
            }
            
            if ([obj objectForKey:@"exhibitor_category"] != [NSNull null]) {
                NSDictionary * dicType = @{@"exhibitor_category":[obj objectForKey:@"exhibitor_category"]};
                ExhibitorCates * typeObj = [MTLJSONAdapter modelOfClass:ExhibitorCates.class
                                                     fromJSONDictionary:dicType
                                                                  error:NULL];
                [MTLManagedObjectAdapter managedObjectFromModel:typeObj
                                           insertingIntoContext:[self managedObjectContext]
                                                          error:NULL];
            }
            NSLog(@"Exhibitor json %@",obj);
            
        }];
        NSError *modelError;
        [[self managedObjectContext] save: &modelError];
        block(arr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) syncProgrammeWithCompletionBlock:(void (^)(NSArray *results))block andFailureBlock:(void (^)(NSError *error))errorBlock {
    NSParameterAssert(block);
    NSParameterAssert(errorBlock);
    NSLog(@"Syncing Programme");
    [self GET:PROGRAMME_LINK parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"Programme return json : %@",responseObject);
        NSDictionary * dic = (NSDictionary *)responseObject;
        NSArray *arr = [dic objectForKey:@"programs"];
        NSArray *arrDays = [dic objectForKey:@"program_day"];
        if ([arrDays count]) {
            [ProgrameDayIds deleteAllWithContext:[self managedObjectContext]];
        }
        if ([arr count]) {
            [Programme deleteAllWithContext:[self managedObjectContext]];
            [ProgrammeTime deleteAllWithContext:[self managedObjectContext]];
        }
        
        [arrDays enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            ProgrameDayIds *mtlObj = [MTLJSONAdapter modelOfClass:ProgrameDayIds.class
                                          fromJSONDictionary:obj
                                                       error:NULL];
            [MTLManagedObjectAdapter managedObjectFromModel:mtlObj
                                       insertingIntoContext:[self managedObjectContext]
                                                      error:NULL];
        }];
        
        [arr enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            
            
            if ([obj objectForKey:@"time_name"] != [NSNull null]) {
                NSDictionary * dicPT = @{@"programme_time":[obj objectForKey:@"time_name"]};
                ProgrammeTime * ptObj = [MTLJSONAdapter modelOfClass:ProgrammeTime.class
                                                  fromJSONDictionary:dicPT
                                                               error:NULL];
                [MTLManagedObjectAdapter managedObjectFromModel:ptObj
                                           insertingIntoContext:[self managedObjectContext]
                                                          error:NULL];
            }
            NSArray * arrSpeaker = [obj objectForKey:@"speakers"];
            NSInteger programme_id = [[obj objectForKey:@"event_id"] integerValue];
            for (int i = 0; i<[arrSpeaker count]; i++) {
                
                NSString * str = [arrSpeaker objectAtIndex:i];
                NSInteger speakerId = [str integerValue];
                //NSLog(@"speaker id %d & program id %d",speakerId,programme_id);
                NSDictionary * dicSpeakerDetail = @{@"programme_id":[NSNumber numberWithInteger:programme_id],@"speaker_id":[NSNumber numberWithInteger:speakerId]};
                Programme_Detail *mtlObj2 = [MTLJSONAdapter modelOfClass:Programme_Detail.class
                                                      fromJSONDictionary:dicSpeakerDetail
                                                                   error:NULL];
                [MTLManagedObjectAdapter managedObjectFromModel:mtlObj2
                                           insertingIntoContext:[self managedObjectContext]
                                                          error:NULL];
            }
            
            Programme *mtlObj = [MTLJSONAdapter modelOfClass:Programme.class
                                          fromJSONDictionary:obj
                                                       error:NULL];
            [MTLManagedObjectAdapter managedObjectFromModel:mtlObj
                                       insertingIntoContext:[self managedObjectContext]
                                                      error:NULL];
            
        }];
        NSError *modelError;
        [[self managedObjectContext] save: &modelError];
        block(arr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) syncInfoWithCompletionBlock:(void (^)(NSArray *results))block andFailureBlock:(void (^)(NSError *error))errorBlock {
    NSParameterAssert(block);
    NSParameterAssert(errorBlock);
    
    [self GET:INFO_LINK parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"Programme return json : %@",responseObject);
        NSLog(@"Syncing INFO and return json %@",(NSDictionary *) responseObject);
        NSArray *arr = (NSArray *)responseObject;
        if ([arr count]) {
            [Info deleteAllWithContext:[self managedObjectContext]];
        }
        [arr enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            
            Info *mtlObj = [MTLJSONAdapter modelOfClass:Info.class
                                          fromJSONDictionary:obj
                                                       error:NULL];
            [MTLManagedObjectAdapter managedObjectFromModel:mtlObj
                                       insertingIntoContext:[self managedObjectContext]
                                                      error:NULL];
            
        }];
        NSError *modelError;
        [[self managedObjectContext] save: &modelError];
        block(arr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) syncAboutWithCompletionBlock:(void (^)(NSArray *results))block andFailureBlock:(void (^)(NSError *error))errorBlock {
    NSParameterAssert(block);
    NSParameterAssert(errorBlock);
    NSLog(@"Syncing About");
    [self GET:ABOUT_LINK parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"Programme return json : %@",responseObject);
        
        NSArray *arr = (NSArray *)responseObject;
        
        [arr enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            
            About *mtlObj = [MTLJSONAdapter modelOfClass:About.class
                                     fromJSONDictionary:obj
                                                  error:NULL];
            [MTLManagedObjectAdapter managedObjectFromModel:mtlObj
                                       insertingIntoContext:[self managedObjectContext]
                                                      error:NULL];
            
        }];
        NSError *modelError;
        [[self managedObjectContext] save: &modelError];
        block(arr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) syncPromotionWithCompletionBlock:(void (^)(NSArray *results))block andFailureBlock:(void (^)(NSError *error))errorBlock {
    NSParameterAssert(block);
    NSParameterAssert(errorBlock);
    NSLog(@"Syncing Promotion");
    [self GET:PROMOTION_LINK parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"Programme return json : %@",responseObject);
        [Promotion deleteAllWithContext:[self managedObjectContext]];
        NSArray *arr = (NSArray *)responseObject;
        
        [arr enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            
            Promotion *mtlObj = [MTLJSONAdapter modelOfClass:Promotion.class
                                      fromJSONDictionary:obj
                                                   error:NULL];
            [MTLManagedObjectAdapter managedObjectFromModel:mtlObj
                                       insertingIntoContext:[self managedObjectContext]
                                                      error:NULL];
        }];
        NSError *modelError;
        [[self managedObjectContext] save: &modelError];
        block(arr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) syncNewsWithCompletionBlock:(void (^)(NSArray *results))block andFailureBlock:(void (^)(NSError *error))errorBlock {
    NSParameterAssert(block);
    NSParameterAssert(errorBlock);
    NSLog(@"Syncing News");

    [self GET:NEWS_LINK parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"Programme return json : %@",responseObject);
        [News deleteAllWithContext:[self managedObjectContext]];
        NSArray *arr = (NSArray *)responseObject;
        
        [arr enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            
            News *mtlObj = [MTLJSONAdapter modelOfClass:News.class
                                          fromJSONDictionary:obj
                                                       error:NULL];
            [MTLManagedObjectAdapter managedObjectFromModel:mtlObj
                                       insertingIntoContext:[self managedObjectContext]
                                                      error:NULL];
        }];
        NSError *modelError;
        [[self managedObjectContext] save: &modelError];
        block(arr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) syncPartnerWithCompletionBlock:(void (^)(NSArray *results))block andFailureBlock:(void (^)(NSError *error))errorBlock {
    NSParameterAssert(block);
    NSParameterAssert(errorBlock);
    NSLog(@"Syncing Partner Clinic");
    [self GET:PARTNER_CLINIC_LINK parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"Programme return json : %@",responseObject);
        
        NSArray *arr = (NSArray *)responseObject;
        if ([arr count]) {
            [Partner deleteAllWithContext:[self managedObjectContext]];
        }
        
        [arr enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            
            Partner *mtlObj = [MTLJSONAdapter modelOfClass:Partner.class
                                     fromJSONDictionary:obj
                                                  error:NULL];
            [MTLManagedObjectAdapter managedObjectFromModel:mtlObj
                                       insertingIntoContext:[self managedObjectContext]
                                                      error:NULL];
        }];
        NSError *modelError;
        [[self managedObjectContext] save: &modelError];
        block(arr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

- (void) syncGovExhibitorWithCompletionBlock:(void (^)(NSArray *results))block andFailureBlock:(void (^)(NSError *error))errorBlock {
    NSParameterAssert(block);
    NSParameterAssert(errorBlock);
    NSLog(@"Syncing GovExhibitor");
    [self GET:GOV_LINK parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"Exhibitor return json : %@",responseObject);
        NSArray *arr = (NSArray *)responseObject;
        [arr enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            Exhibitor *mtlObj = [MTLJSONAdapter modelOfClass:Exhibitor.class
                                          fromJSONDictionary:obj
                                                       error:NULL];
            [MTLManagedObjectAdapter managedObjectFromModel:mtlObj
                                       insertingIntoContext:[self managedObjectContext]
                                                      error:NULL];
            
        
            //NSLog(@"Exhibitor json %@",obj);
            
        }];
        NSError *modelError;
        [[self managedObjectContext] save: &modelError];
        block(arr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock(error);
    }];
}

#pragma mark speakers
- (NSArray *) getSpeakers {
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Speakers managedObjectEntityName]];
    
    NSSortDescriptor *sortDescriptor;
    
    if ([StringTable getCurrentLangage] == LANGAUGE_EGN) {
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"speaker_name" ascending:YES];
    }
    else if ([StringTable getCurrentLangage] == LANGAUGE_CN){
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"speaker_name_cn" ascending:YES];
    }
    
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return arr;
}

- (Speakers *) getSpeakerById:(NSNumber *)idx{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Speakers managedObjectEntityName]];
    NSLog(@"programme id %d",[idx integerValue]);
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"speaker_id == %d",[idx integerValue]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"speaker_id" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return (Speakers *)[arr objectAtIndex:0];
}

- (NSArray *) getSpeakersById:(NSNumber *)idx{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Speakers managedObjectEntityName]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"speaker_id == %d",[idx integerValue]];
    NSSortDescriptor *sortDescriptor;
   
    if ([StringTable getCurrentLangage] == LANGAUGE_EGN) {
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"speaker_name" ascending:YES];
    }
    else if ([StringTable getCurrentLangage] == LANGAUGE_CN){
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"speaker_name_cn" ascending:YES];
    }
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return arr;
}

#pragma mark Sponsors
- (NSArray *) getSponsors {
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Sponsor managedObjectEntityName]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sponsor_name" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return arr;
}

- (NSArray *) getSponsorsByTypeId:(NSNumber *)type_id{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Sponsor managedObjectEntityName]];
    //NSLog(@"type id %d",[type_id integerValue]);
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"sponsor_type == %d",[type_id integerValue]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sponsor_name" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return arr;
}

- (NSArray *)getSponsorTypes{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[SponsorTypes managedObjectEntityName]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"type_id" ascending:NO];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return arr;
}

#pragma mark Programme
- (NSArray *)getDayIds{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[ProgrameDayIds managedObjectEntityName]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"day_id" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    NSLog(@"day count %d",arr.count);
    return arr;
}

- (ProgrameDayIds *)getDayIdById:(NSNumber *)idx{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[ProgrameDayIds managedObjectEntityName]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"day_id == %d",[idx integerValue]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"day_id" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    NSLog(@"day count %d",arr.count);
    return (ProgrameDayIds *)[arr objectAtIndex:0];
}

- (NSArray *)getProgrammeTime{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[ProgrammeTime managedObjectEntityName]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"programme_time" ascending:NO];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    
    return arr;
}

- (NSArray *)getProgrammes{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Programme managedObjectEntityName]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"event_id" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return arr;
}

- (NSArray *) getProgrammeByDayId:(NSNumber *)day_id{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Programme managedObjectEntityName]];
    //NSLog(@"day id %d",[day_id integerValue]);
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"day_id == %d",[day_id integerValue]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"event_name" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return arr;
}

- (NSArray *) getProgrammeSpeakerIdsByDayId:(NSNumber *)programme_id{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Programme_Detail managedObjectEntityName]];
    NSLog(@"day id %d",[programme_id integerValue]);
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"programme_id == %d",[programme_id integerValue]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"speaker_id" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    NSLog(@"%@", arr);
    return arr;
}

- (NSArray *) getProgrammeByTime:(NSString *)programme_time andDay_id:(NSNumber *)day_id{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Programme managedObjectEntityName]];
    //NSLog(@"time id %@ and Day id %d",programme_time,[day_id integerValue]);
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"time_name == %@ && day_id == %d",programme_time,[day_id integerValue]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"start_time_stamp" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return arr;
}

- (NSArray *) getProgrammeByDay_id:(NSNumber *)day_id andSpeakeId:(NSNumber *)speaker_id{
    NSError *modelError;
    NSArray * arrProgramDetails = [self getProgramsDetailBySpeakerId:speaker_id];
    //NSLog(@"arr programe count %d",[arrProgramDetails count]);
    NSMutableArray * arrPrograms = nil;
    if ([arrProgramDetails count]) {
        arrPrograms = [[NSMutableArray alloc] init];
    }
    for (Programme_Detail * programDetail in arrProgramDetails) {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Programme managedObjectEntityName]];
        //NSLog(@"time id %@ and Day id %d",programme_time,[day_id integerValue]);
        NSLog(@"program id %d",[programDetail.programme_id integerValue]);
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"day_id == %d && event_id == %d",[day_id integerValue],[programDetail.programme_id integerValue]];
        //fetchRequest.predicate = [NSPredicate predicateWithFormat:@"event_id == %d",[programDetail.programme_id integerValue]];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"start_time_stamp" ascending:YES];
        [fetchRequest setSortDescriptors:@[sortDescriptor]];
        NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
        if ([arr count]) {
            [arrPrograms addObject:(Programme *)[arr objectAtIndex:0]];
        }
    }
    return (NSArray *)arrPrograms;
}

- (NSArray *) getProgramsDetailBySpeakerId:(NSNumber *)speaker_id{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Programme_Detail managedObjectEntityName]];
    //NSLog(@"time id %@ and Day id %d",programme_time,[day_id integerValue]);
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"speaker_id == %d",[speaker_id integerValue]];
    //NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"programme_id" ascending:YES];
    //[fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return arr;
}

- (NSMutableArray *) getFavouriteProgrammeByTime:(NSString *)programme_time andDay_id:(NSNumber *)day_id{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Programme managedObjectEntityName]];
    NSFetchRequest *fetchRequestProgramFavourite = [NSFetchRequest fetchRequestWithEntityName:[ProgramFavourite managedObjectEntityName]];
    fetchRequestProgramFavourite.predicate = [NSPredicate predicateWithFormat:@"is_fav == 1"];
    NSArray * favProgramsArr = [[self managedObjectContext] executeFetchRequest:fetchRequestProgramFavourite error:&modelError];
    NSMutableArray * progamArr = nil;
    if ([favProgramsArr count]) {
        progamArr = [[NSMutableArray alloc] init];
    }
    for (ProgramFavourite * programFav in favProgramsArr) {
       // NSLog(@"time id %@ and Day id %d event_id %d",programme_time,[day_id integerValue],[programFav.programe_id integerValue]);
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"event_id == %d && time_name == %@ && day_id == %d",[programFav.programe_id integerValue],programme_time,[day_id integerValue]];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"start_time_stamp" ascending:YES];
        
        [fetchRequest setSortDescriptors:@[sortDescriptor]];
       NSArray * arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
        if ([arr count]) {
            [progamArr addObject:(Programme *)[arr objectAtIndex:0]];
        }
    }
    
    
    return progamArr;
}

- (void) saveProgrammeFav:(NSNumber *)programe_id andFav:(NSNumber *)isFav{
    NSDictionary * dic = [ProgramFavourite makeDictionaryWithProgramId:programe_id andFav:isFav];
    ProgramFavourite *mtlObj = [MTLJSONAdapter modelOfClass:ProgramFavourite.class
                                         fromJSONDictionary:dic
                                                      error:NULL];
    [MTLManagedObjectAdapter managedObjectFromModel:mtlObj
                               insertingIntoContext:[self managedObjectContext]
                                              error:NULL];
    NSError *modelError;
    [[self managedObjectContext] save: &modelError];
}

- (NSNumber *) getProgramFavouriteByProgramId:(NSNumber *)program_id{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[ProgramFavourite managedObjectEntityName]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"programe_id == %d",[program_id integerValue]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"programe_id" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    NSNumber * isFav = [NSNumber numberWithBool:FALSE];
    if ([arr count]) {
        ProgramFavourite * progFav = (ProgramFavourite *)[arr objectAtIndex:0];
        isFav = progFav.is_fav;
    }
    return isFav;
}

#pragma mark Exhibition
- (NSArray *)getExhibitionTypes{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[ExhibitorTypes managedObjectEntityName]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"exhibitor_type" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return arr;
}

- (NSArray *)getExhibitorsByTypeName:(NSString *)strType{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Exhibitor managedObjectEntityName]];
    if (![strType isEqualToString:@"All"]) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"exhibitor_type == %@",strType];
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"exhibitor_name" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    NSLog(@"exhibitor count %d",arr.count);
    return arr;
}

- (Exhibitor *)getExhibitorsByID:(NSString *)ID{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Exhibitor managedObjectEntityName]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"exhibitor_id == %@",ID];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"exhibitor_name" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return (Exhibitor *)[arr objectAtIndex:0];;
}

- (NSArray *)getExhibitors{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Exhibitor managedObjectEntityName]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"exhibitor_name" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    NSLog(@"exhibitor count %d",arr.count);
    return arr;
}

#pragma mark Info
- (NSArray *)getAbouts{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Info managedObjectEntityName]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return arr;
}

#pragma mark Promotion
- (NSArray *)getPromotions{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Promotion managedObjectEntityName]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"promotion_id" ascending:NO];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return arr;
}

#pragma mark News
- (NSArray *)getNews{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[News managedObjectEntityName]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"news_id" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    return arr;
}

#pragma mark Partner Clinic
- (Partner *)getPartnerClinic{
    NSError *modelError;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[Partner managedObjectEntityName]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"partners_clinic" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    NSArray *arr = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&modelError];
    if ([arr count]) {
        return (Partner *)[arr objectAtIndex:0];
    }
    return nil;
}

- (void) registerForPush:(NSString *) token andDeviceType:(NSString *) type {
    NSDictionary *params = @{@"token": token, @"device_type": type};
    [self POST:@"api/devices" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"successful");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"THE ERROS is %@",error);
    }];
}

@end
