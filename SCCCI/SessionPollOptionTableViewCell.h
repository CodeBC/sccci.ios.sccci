//
//  SessionPollOptionTableViewCell.h
//  SCCCI
//
//  Created by Zayar on 6/26/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SessionPollOptionTableViewCell : UITableViewCell
- (void) setUpViews;
- (void) loadWithName:(NSString *)strName andCheck:(BOOL)check;
- (void) setImageCheck:(BOOL)isCheck;
@end
