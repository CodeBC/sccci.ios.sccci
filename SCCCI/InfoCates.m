//
//  InfoCates.m
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "InfoCates.h"


@implementation InfoCates

#pragma mark - JSON serialization
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"type" : @"type"
             };
}

#pragma mark - Managed object serialization
+ (NSString *)managedObjectEntityName {
    return @"InfoCates";
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    return @{
             @"type" : @"type"
             };
}

+ (NSSet *)propertyKeysForManagedObjectUniquing {
    return [NSSet setWithObject:@"type"];
}

+ (void) deleteAllWithContext: (NSManagedObjectContext *) context {
    NSFetchRequest * fectchRequest = [[NSFetchRequest alloc] init];
    [fectchRequest setEntity:[NSEntityDescription entityForName:@"InfoCates" inManagedObjectContext:context]];
    [fectchRequest setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * objArr = [context executeFetchRequest:fectchRequest error:&error];
    //error handling goes here
    for (NSManagedObject * managedObj in objArr) {
        [context deleteObject:managedObj];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //more error handling here
}
@end
