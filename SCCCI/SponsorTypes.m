//
//  SponsorTypes.m
//  SCCCI
//
//  Created by Zayar on 7/12/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SponsorTypes.h"


@implementation SponsorTypes

#pragma mark - JSON serialization
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"type_id" : @"type_id",
             @"type_name" : @"type_name"
             };
}

#pragma mark - Managed object serialization
+ (NSString *)managedObjectEntityName {
    return @"SponsorTypes";
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    return @{
             @"type_id" : @"type_id",
             @"type_name" : @"type_name"
             };
}

+ (NSSet *)propertyKeysForManagedObjectUniquing {
    return [NSSet setWithObject:@"type_id"];
}

+ (void) deleteAllWithContext: (NSManagedObjectContext *) context {
    NSFetchRequest * fectchRequest = [[NSFetchRequest alloc] init];
    [fectchRequest setEntity:[NSEntityDescription entityForName:@"SponsorTypes" inManagedObjectContext:context]];
    [fectchRequest setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * objArr = [context executeFetchRequest:fectchRequest error:&error];
    //error handling goes here
    for (NSManagedObject * managedObj in objArr) {
        [context deleteObject:managedObj];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //more error handling here
}

@end
