//
//  ProgramDescriptionTableViewCell.h
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgramDescriptionTableViewCell : UITableViewCell
- (void) setUpView;
- (void) viewLoadWithDescription:(NSString *)strDescription;
+ (CGFloat)heightForCellWithPost:(NSString *)strDescription;
@end
