//
//  ExhibitorListViewController.m
//  SCCCI
//
//  Created by Zayar on 6/18/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ExhibitorListViewController.h"
#import "ExhibitorListTableViewCell.h"
#import "SponsersViewController.h"
#import "ExhibitorDetailViewController.h"
#import "ExhibitorFloorPlanViewController.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "ServiceMappingClient.h"
#import "ExhibitorTypes.h"
#import "ExhibitorsViewController.h"
#import "NewsViewController.h"
#import "StringTable.h"

@interface ExhibitorListViewController ()
@property (strong, nonatomic) NSMutableArray * arrExhibitorsList;
@property (strong, nonatomic) SponsersViewController * sponsersViewController;
@property (strong, nonatomic) ExhibitorDetailViewController * exhibitorDetailViewController;
@property (strong, nonatomic) ExhibitorFloorPlanViewController * exhibitorFloorPlanViewController;
@property (strong, nonatomic) ExhibitorsViewController * exhibitorsViewController;

@end

@implementation ExhibitorListViewController
@synthesize strTitle;
- (SponsersViewController *) sponsersViewController{
    if (!_sponsersViewController) {
        _sponsersViewController = [[SponsersViewController alloc] init];
    }
    return _sponsersViewController;
}

- (ExhibitorDetailViewController *) exhibitorDetailViewController{
    if (!_exhibitorDetailViewController) {
        _exhibitorDetailViewController = [[ExhibitorDetailViewController alloc] init];
    }
    return _exhibitorDetailViewController;
}

- (ExhibitorFloorPlanViewController *) exhibitorFloorPlanViewController{
    if (!_exhibitorFloorPlanViewController) {
        _exhibitorFloorPlanViewController = [[ExhibitorFloorPlanViewController alloc] init];
    }
    return _exhibitorFloorPlanViewController;
}

- (ExhibitorsViewController *) exhibitorsViewController{
    if (!_exhibitorsViewController) {
        _exhibitorsViewController = [[ExhibitorsViewController alloc] init];
    }
    return _exhibitorsViewController;
}



- (NSMutableArray *) arrExhibitorsList{
    //Scott Smith Ki-Ho Park Mimiwati

    if ([self getCurrentLanguage] == LANGAUGE_CN){
        NSArray * arr = [NSArray arrayWithObjects:@"参展商名单",@"参展商",@"一站式政府服务平台",@"参展商位置图", nil];
        _arrExhibitorsList = [[NSMutableArray alloc] initWithArray:arr];

    } else {
        NSArray * arr = [NSArray arrayWithObjects:@"List of all Exhibitors",@"Exhibitors",@"Goverment Pavilion Exhibition",@"Exhibitor Floorplan", nil];
        _arrExhibitorsList = [[NSMutableArray alloc] initWithArray:arr];
    }
    
    
    return _arrExhibitorsList;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"Exhibition"];
    
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
    [self.tbl reloadData];
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([self.tbl respondsToSelector:@selector(separatorInset)]) {
            [self.tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated{
    /*AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    NSArray * arrExhibitorType = [client getExhibitionTypes];
    if ([arrExhibitorType count]) {
        [self.arrExhibitorsList removeAllObjects];
    }
    
    for (ExhibitorTypes * type in arrExhibitorType) {
        [self.arrExhibitorsList addObject:type];
    }*/
    [self setNavBarTitle:strTitle];
    self.arrExhibitorsList = [self arrExhibitorsList];
    NSLog(@"%ld", (long)[self getCurrentLanguage]);
    [self.tbl reloadData];
    
    
}


#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"aboutCell";
	ExhibitorListTableViewCell *cell = (ExhibitorListTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (cell == nil) {
        cell = [[ExhibitorListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setUpViews];
	}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    //ExhibitorTypes * type = [self.arrExhibitorsList   objectAtIndex:indexPath.row];
    [cell viewLoadWithString:[self.arrExhibitorsList   objectAtIndex:indexPath.row]];
    UIImageView * indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
    indicatorView.image = [UIImage imageNamed:@"cell_indicator"];
    cell.accessoryView = indicatorView;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrExhibitorsList count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*switch (indexPath.row) {
        case 0:
            [self.navigationController pushViewController:self.sponsersViewController animated:YES];
            break;
        case 1:
            [self.navigationController pushViewController:self.exhibitorDetailViewController animated:YES];
            break;
        case 4:
            [self.navigationController pushViewController:self.exhibitorFloorPlanViewController animated:YES];
            break;
        default:
            break;
    }*/
    /*ExhibitorTypes * type = [self.arrExhibitorsList  objectAtIndex:indexPath.row];
    if ([type.exhibitor_type isEqualToString:@"Sponsor"]) {
        [self.navigationController pushViewController:self.sponsersViewController animated:YES];
    }
    else{
        self.exhibitorsViewController.exhibitorType = [self.arrExhibitorsList objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:self.exhibitorsViewController animated:YES];
    }*/
    NSString * str = [self.arrExhibitorsList objectAtIndex:indexPath.row];
    if ([str isEqualToString:@"Exhibitors"] || [str isEqualToString:@"参展商"]) {
        self.exhibitorsViewController.typeName = @"Exhibitors";
        [self.navigationController pushViewController:self.exhibitorsViewController animated:YES];
    }
    else if([str isEqualToString:@"Goverment Pavilion Exhibition"] || [str isEqualToString:@"一站式政府服务平台"]){
        self.exhibitorsViewController.typeName = @"Government Pavilion Exhibition";
        [self.navigationController pushViewController:self.exhibitorsViewController animated:YES];
    }
    else if([str isEqualToString:@"List of all Exhibitors"] || [str isEqualToString:@"参展商名单"]){
        self.exhibitorsViewController.typeName = @"All";
        [self.navigationController pushViewController:self.exhibitorsViewController animated:YES];
    }
    else if([str isEqualToString:@"Exhibitor Floorplan"] || [str isEqualToString:@"参展商位置图"]){
        [self.navigationController pushViewController:self.exhibitorFloorPlanViewController animated:YES];
    }
}

@end
