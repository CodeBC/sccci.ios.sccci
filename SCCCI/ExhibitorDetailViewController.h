//
//  ExhibitorDetailViewController.h
//  SCCCI
//
//  Created by Zayar on 6/24/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Exhibitor.h"
@interface ExhibitorDetailViewController : BasedWithTableViewController
@property (nonatomic,strong) Exhibitor * exhibitor;
@end
