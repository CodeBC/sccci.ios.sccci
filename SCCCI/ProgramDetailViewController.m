//
//  ProgramDetailViewController.m
//  SCCCI
//
//  Created by Zayar on 6/17/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ProgramDetailViewController.h"
#import "ProgramDescriptionTableViewCell.h"
#import "ProgramSpeakerTableViewCell.h"
#import "StringTable.h"
#import "Utility.h"
#import "ProgrameDayIds.h"
#import "AppDelegate.h"
#import "ServiceMappingClient.h"
#import "Programme_Detail.h"
#import "Speakers.h"
@interface ProgramDetailViewController ()
{
    NSString * strDecription;
}
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UITableView *tbl;
@property (strong, nonatomic) NSArray * arrSectionTitles;
@property (strong, nonatomic) NSMutableArray * arrSpeakerNames;
@property (nonatomic, strong) UIActivityViewController *activityViewController;
@end

@implementation ProgramDetailViewController
@synthesize programme;
- (NSArray *) arrSectionTitles{
    if (!_arrSectionTitles) {
        _arrSectionTitles = [NSArray arrayWithObjects:@"Wed 20 Aug   Hall 1   08:00 to 09:30   Mandarin",@"Speakers", nil];
    }
    return _arrSectionTitles;
}

- (NSString *) getHeaderFormat:(Programme *)prog{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    ProgrameDayIds * programeDay = [client getDayIdById:prog.day_id];
    return [NSString stringWithFormat:@"%@  %@  %@ to %@  %@",programeDay.day_name, prog.event_location,prog.start_time,prog.end_time,prog.event_language];
}

- (NSMutableArray *) arrSpeakerNames{
    //Scott Smith Ki-Ho Park Mimiwati
    if (!_arrSpeakerNames) {
        _arrSpeakerNames = [[NSMutableArray alloc] init];
    }
    return _arrSpeakerNames;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"Session Details"];
    
    
    self.lblTitle.font = [UIFont fontWithName:ROBOTO_BOLD size:15];
    self.lblTitle.textColor = [UIColor colorWithHexString:@"01457b"];
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([self.tbl respondsToSelector:@selector(separatorInset)]) {
            [self.tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
    self.tbl.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)viewWillAppear:(BOOL)animated{
    NSInteger lang = [StringTable getCurrentLangage];
    if (lang == LANGAUGE_EGN) {
        self.lblTitle.text = programme.event_name;
        strDecription = programme.event_description;
    }
    else if (lang == LANGAUGE_CN){
        self.lblTitle.text = programme.event_name_cn;
        strDecription = programme.event_description_cn;
    }
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    NSArray * speaker_ids = [client getProgrammeSpeakerIdsByDayId:programme.event_id];
    if ([speaker_ids count]) {
        [self.arrSpeakerNames removeAllObjects];
    }
    self.arrSpeakerNames = [[NSMutableArray alloc] init];
    for (Programme_Detail * programeDetail in speaker_ids) {
        Speakers * speaker = [client getSpeakerById:programeDetail.speaker_id];
        [self.arrSpeakerNames addObject:speaker];
    }
    [self.tbl reloadData];
}

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"ProgramDescriptionTableViewCell";
        ProgramDescriptionTableViewCell *cell = (ProgramDescriptionTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[ProgramDescriptionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setUpView];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
        
        [cell viewLoadWithDescription:strDecription];
        //UIImageView * indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
        //indicatorView.image = [UIImage imageNamed:@"cell_indicator"];
        //cell.accessoryView = indicatorView;
        return cell;
    }
    else if(indexPath.section == 1){
        static NSString *CellIdentifier = @"ProgramSpeakerTableViewCell";
        ProgramSpeakerTableViewCell *cell = (ProgramSpeakerTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[ProgramSpeakerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setUpView];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
         Speakers * speaker = [self.arrSpeakerNames objectAtIndex:indexPath.row];
        if ([self getCurrentLanguage] == LANGAUGE_EGN) {
            [cell viewLoadWithDescription:speaker.speaker_name andJob: [NSString stringWithFormat:@"%@, %@", speaker.speaker_designation, speaker.speaker_company]];
        }
        else if ([self getCurrentLanguage] == LANGAUGE_CN){
            [cell viewLoadWithDescription:speaker.speaker_name_cn andJob:[NSString stringWithFormat:@"%@, %@", speaker.speaker_designation_cn, speaker.speaker_company_cn]];
        }
        
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return [ProgramDescriptionTableViewCell heightForCellWithPost:strDecription];
    }
    else if(indexPath.section == 1){
        return 60;
    }
    return 0;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    if (section == 0) {
        return 1;
    }
    else if(section == 1){
        return [self.arrSpeakerNames count];
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return [self.arrSectionTitles count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 35)];
    [v setBackgroundColor:[UIColor colorWithHexString:@"ebeaf0"]];
    UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.frame.size.width-50, 35)];
    lblTitle.font = [UIFont fontWithName:ROBOTO_BOLD size:12];
    lblTitle.textColor = [UIColor blackColor];
    if (section == 0) {
        lblTitle.text = [self getHeaderFormat:programme];
    }
    else lblTitle.text = [self.arrSectionTitles objectAtIndex:section];
    [v addSubview:lblTitle];
    [Utility makeBorder:v andWidth:0.5 andColor:[UIColor colorWithHexString:@"bfbfbf"] andCornerRadius:0];
    if (section == 0) {
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(lblTitle.frame.origin.x + lblTitle.frame.size.width + 10, 7, 15, 20);
        [btn setImage:[UIImage imageNamed:@"btn_share"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(onShare:) forControlEvents:UIControlEventTouchUpInside];
        [v addSubview:btn];
    }
    
    return v;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 35;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*self.detailViewController.strTitle = [self.arrTitles objectAtIndex:indexPath.row];
     [self.navigationController pushViewController:self.detailViewController animated:YES];*/
}

#pragma mark actions
- (void)onShare:(UIButton *)sender{
    self.activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[[NSString stringWithFormat:@"%@ \n %@",SHARE_MSG,SHARE_LINK]] applicationActivities:nil];
    [self presentViewController:self.activityViewController animated:YES completion:nil];
    //[NSString stringWithFormat:@"Original Link:[%@]",SHARE_LINK]
}



@end
