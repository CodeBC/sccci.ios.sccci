//
//  HighlightViewController.h
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HighlightViewController : BasedWithTableViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) NSString * strTitle;
@end
