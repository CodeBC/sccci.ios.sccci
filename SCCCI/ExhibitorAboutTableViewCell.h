//
//  ExhibitorAboutTableViewCell.h
//  SCCCI
//
//  Created by Zayar on 6/24/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ExhibitorAboutTableViewCellDelegate;
@interface ExhibitorAboutTableViewCell : UITableViewCell
@property id<ExhibitorAboutTableViewCellDelegate> owner;
- (void) setUpViews;
- (void) viewLoadWithAbout:(NSString *)strAbout;
+ (CGFloat)heightForCellWithPost:(NSString *)strAbout;
- (void) forceMeasure;
@end

@protocol ExhibitorAboutTableViewCellDelegate
- (void) webDescriptionCellDidFinish:(ExhibitorAboutTableViewCell *)cell shouldAssignHeight:(CGFloat)newHeight;

@end
