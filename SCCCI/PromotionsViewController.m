//
//  PromotionsViewController.m
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "PromotionsViewController.h"
#import "PromotionDescriptionTableViewCell.h"
#import "PromotionTableViewCell.h"
#import "PromoDetailViewController.h"
#import "AppDelegate.h"
#import "ServiceMappingClient.h"
#import "Promotion.h"
#import "StringTable.h"
@interface PromotionsViewController ()
{
    NSString * strDescription;
}
@property (strong, nonatomic) NSArray * arrPromos;
@property (strong, nonatomic) PromoDetailViewController * promoDetailViewController;
@end

@implementation PromotionsViewController
- (PromoDetailViewController *)promoDetailViewController{
    if (!_promoDetailViewController) {
        _promoDetailViewController = [[PromoDetailViewController alloc] init];
    }
    return _promoDetailViewController;
}

/*- (NSArray *) arrPromos{
    if (!_arrPromos) {
        _arrPromos = [NSArray arrayWithObjects: @"Promo", @"Promo",@"Promo",@"Promo",@"Promo",@"Promo",@"Promo",@"Promo",nil];
    }
    return _arrPromos;
}*/

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    strDescription = @"Details coming soon!";
    
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    self.arrPromos = [client getPromotions];
    [self.tbl reloadData];
    
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
    [self setNavBarTitle:@"Promotion"];
}

- (void)viewWillAppear:(BOOL)animated{

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getPushNoti:) name:@"pushNotiArrived" object:nil];
}


- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pushNotiArrived" object:nil];
    
}

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        static NSString *CellIdentifier = @"PromotionTableViewCell";
        PromotionTableViewCell *cell = (PromotionTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[PromotionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setUpViews];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

    Promotion * promo = [self.arrPromos objectAtIndex:indexPath.row];
        NSInteger lang = [StringTable getCurrentLangage];
        if (lang == LANGAUGE_EGN) {
            [cell viewLoadWithName:promo.promotion_title andDescription:promo.promotion_description andImageName:promo.promotion_logo];
        }
        else if (lang == LANGAUGE_CN){
            [cell viewLoadWithName:promo.promotion_title_cn andDescription:promo.promotion_description_cn andImageName:promo.promotion_logo];
        }
        
        
        UIImageView * indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
        indicatorView.image = [UIImage imageNamed:@"cell_indicator"];
        cell.accessoryView = indicatorView;
        return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return [PromotionDescriptionTableViewCell heightForCellAndCapWithAbout:strDescription];
    }
    else if(indexPath.section == 1){
        return 47;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.

        return [self.arrPromos count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.promoDetailViewController.promotion = [self.arrPromos objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:self.promoDetailViewController animated:YES];
}

- (void) showPromoAlert{
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Promo" message:@"Promo Text" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alertView show];
}

- (void) alertView: (UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex{
    
    if (buttonIndex == 1) {
        NSLog(@"Promo Ok!");
    }
}

- (void) getPushNoti:(NSNotification *) notification{
    NSLog(@"THE NOTIFICATION IS %@",notification.userInfo);
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ServiceMappingClient *client = [delegate client];
    self.arrPromos = [client getPromotions];
    [self.tbl reloadData];
    
    
}


@end
