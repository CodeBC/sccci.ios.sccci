//
//  LuckyDrawViewController.m
//  SCCCI
//
//  Created by Zayar on 6/26/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "LuckyDrawViewController.h"
#import "StringTable.h"
#import "Utility.h"
#import "LuckyDrawResultViewController.h"
@interface LuckyDrawViewController ()
@property (nonatomic,strong) UILabel * lblDescription;
@property (nonatomic,strong) UIButton * btnLuckyDraw;
@property (nonatomic,strong) LuckyDrawResultViewController * luckyDrawResultViewController;
@end

@implementation LuckyDrawViewController

- (LuckyDrawResultViewController *) luckyDrawResultViewController{
    if (!_luckyDrawResultViewController) {
        _luckyDrawResultViewController = [[LuckyDrawResultViewController alloc] init];
    }
    return _luckyDrawResultViewController;
}

- (UILabel *) lblDescription{
    if (!_lblDescription) {
        _lblDescription = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.topBannerSilder.frame.origin.y + self.topBannerSilder.frame.size.height + STATUS_BAR_NAV_BAR_HEIGHT + 20, NORMAL_WIDTH, 50)];
        _lblDescription.font = [UIFont fontWithName:ROBOTO_LIGHT size:18];
        _lblDescription.textColor = [UIColor colorWithHexString:@"333333"];
        _lblDescription.numberOfLines = 2;
        _lblDescription.text = @"Click the button below to win \n lucky draw prizes:";
        _lblDescription.textAlignment = NSTextAlignmentCenter;
    }
    return _lblDescription;
}

- (UIButton *) btnLuckyDraw{
    if (!_btnLuckyDraw) {
        _btnLuckyDraw = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnLuckyDraw.frame = CGRectMake(42, self.lblDescription.frame.origin.y + self.lblDescription.frame.size.height + 20, 233, 35);
        _btnLuckyDraw.backgroundColor = [UIColor colorWithHexString:@"01457b"];
        [_btnLuckyDraw setTitle:@"Take part in lucky draw" forState:UIControlStateNormal];
        [Utility makeCornerRadius:_btnLuckyDraw andRadius:5];
        [_btnLuckyDraw addTarget:self action:@selector(onDraw:) forControlEvents:UIControlEventTouchUpInside];
        _btnLuckyDraw.titleLabel.font = [UIFont fontWithName:ROBOTO_LIGHT size:15];
    }
    return _btnLuckyDraw;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"Lucky Draw"];
    [self.view addSubview:self.lblDescription];
    [self.view addSubview:self.btnLuckyDraw];
}

- (void) onDraw:(UIButton *)sender{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    [self performSelector:@selector(startDraw) withObject:nil afterDelay:2];
}

- (void)startDraw{
    [SVProgressHUD dismiss];
    NSInteger randomInteger = arc4random() % (int)2;
    if (randomInteger == 1)
        self.luckyDrawResultViewController.strSuccessCode = @"123454556";
    else self.luckyDrawResultViewController.strSuccessCode = @"";
    
    [self.navigationController pushViewController:self.luckyDrawResultViewController animated:YES];
}

@end
