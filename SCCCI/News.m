//
//  News.m
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "News.h"


@implementation News

#pragma mark - JSON serialization
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"news_id" : @"news_id",
             @"news_title" : @"news_title",
             @"news_description" : @"news_description",
             @"news_title_cn" : @"news_title_cn",
             @"news_description_cn" : @"news_description_cn"
             };
}

#pragma mark - Managed object serialization
+ (NSString *)managedObjectEntityName {
    return @"News";
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    return @{
             @"news_id" : @"news_id",
             @"news_title" : @"news_title",
             @"news_description" : @"news_description",
             @"news_title_cn" : @"news_title_cn",
             @"news_description_cn" : @"news_description_cn"
             };
}

+ (NSSet *)propertyKeysForManagedObjectUniquing {
    return [NSSet setWithObject:@"news_id"];
}

+ (void) deleteAllWithContext: (NSManagedObjectContext *) context {
    NSFetchRequest * fectchRequest = [[NSFetchRequest alloc] init];
    [fectchRequest setEntity:[NSEntityDescription entityForName:@"News" inManagedObjectContext:context]];
    [fectchRequest setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * objArr = [context executeFetchRequest:fectchRequest error:&error];
    //error handling goes here
    for (NSManagedObject * managedObj in objArr) {
        [context deleteObject:managedObj];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //more error handling here
}

@end
