//
//  ProgramFavourite.m
//  SCCCI
//
//  Created by Zayar on 7/15/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ProgramFavourite.h"


@implementation ProgramFavourite

#pragma mark - JSON serialization
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"programe_id" : @"programe_id",
             @"is_fav" : @"is_fav"
             };
}

#pragma mark - Managed object serialization
+ (NSString *)managedObjectEntityName {
    return @"ProgramFavourite";
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    return @{
             @"programe_id" : @"programe_id",
             @"is_fav" : @"is_fav"
             };
}

+ (NSSet *)propertyKeysForManagedObjectUniquing {
    return [NSSet setWithObject:@"programe_id"];
}

+ (void) deleteAllWithContext: (NSManagedObjectContext *) context {
    NSFetchRequest * fectchRequest = [[NSFetchRequest alloc] init];
    [fectchRequest setEntity:[NSEntityDescription entityForName:@"ProgramFavourite" inManagedObjectContext:context]];
    [fectchRequest setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * objArr = [context executeFetchRequest:fectchRequest error:&error];
    //error handling goes here
    for (NSManagedObject * managedObj in objArr) {
        [context deleteObject:managedObj];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //more error handling here
}

+ (NSDictionary *) makeDictionaryWithProgramId:(NSNumber *)program_id andFav:(NSNumber *)isFav{
    NSDictionary * dic = @{@"programe_id":program_id,@"is_fav":isFav};
    return dic;
}

@end
