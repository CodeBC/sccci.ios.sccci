//
//  LivePollViewController.m
//  SCCCI
//
//  Created by Zayar on 6/26/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "LivePollViewController.h"
#import "StringTable.h"
#import "Utility.h"
#import "MyUITextField.h"
#import "SessionsPollViewController.h"
@interface LivePollViewController ()
@property (nonatomic,strong) UILabel * lblTitle;
@property (nonatomic,strong) UIView * textAndButtonView;
@property (nonatomic,strong) MyUITextField * txtKeyCode;
@property (nonatomic,strong) UIButton * btnJoin;
@property (nonatomic,strong) UIScrollView * scrollView;
@property (nonatomic,strong) SessionsPollViewController * sessionsPollViewController;
@end

@implementation LivePollViewController
//---size of keyboard---
CGRect keyboardBounds;
//---size of application screen---
CGRect applicationFrame;
//---original size of ScrollVie
CGSize scrollViewOriginalSize;

- (SessionsPollViewController *) sessionsPollViewController{
    if (!_sessionsPollViewController) {
        _sessionsPollViewController = [[SessionsPollViewController alloc] init];
    }
    return _sessionsPollViewController;
}

- (UIScrollView *) scrollView{
    if (!_scrollView) {
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.topBannerSilder.frame.origin.x + self.topBannerSilder.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - (self.topBannerSilder.frame.origin.x+self.topBannerSilder.frame.size.height)-STATUS_BAR_NAV_BAR_HEIGHT)];
        //_scrollView.backgroundColor = [UIColor redColor];
    }
    return _scrollView;
}

-(UILabel *) lblTitle{
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN,20, NORMAL_WIDTH,70)];
        _lblTitle.font = [UIFont fontWithName:ROBOTO_LIGHT size:20];
        _lblTitle.textColor = [UIColor colorWithHexString:@"333333"];
        _lblTitle.text = @"To join poll,\n please key in code:";
        _lblTitle.textAlignment = NSTextAlignmentCenter;
        _lblTitle.numberOfLines = 2;
    }
    return _lblTitle;
}

- (UIView *) textAndButtonView{
    if (!_textAndButtonView) {
        NSLog(@"height %f",self.lblTitle.frame.origin.x + self.lblTitle.frame.size.height+50);
        _textAndButtonView = [[UIView alloc] initWithFrame:CGRectMake(40,self.lblTitle.frame.origin.x + self.lblTitle.frame.size.height+50 , 233, 43)];
        [Utility makeCornerRadius:_textAndButtonView andRadius:5];
        //[Utility makeBorder:_textAndButtonView andWidth:1 andColor:[UIColor blackColor] andCornerRadius:5];
    }
    return _textAndButtonView;
}

- (MyUITextField *)txtKeyCode{
    if (!_txtKeyCode) {
        _txtKeyCode = [[MyUITextField alloc] initWithFrame:CGRectMake(0, 0, self.textAndButtonView.frame.size.width - 66, self.textAndButtonView.frame.size.height)];
        
        _txtKeyCode.keyboardType = UIKeyboardTypeAlphabet;
        _txtKeyCode.returnKeyType  = UIReturnKeyDone;
        _txtKeyCode.borderStyle = UITextBorderStyleNone;
        //_txtKeyCode.placeholder = @"Take part in lucky draw";
        _txtKeyCode.delegate = self;
        _txtKeyCode.font = [UIFont fontWithName:ROBOTO_LIGHT size:14];
        _txtKeyCode.backgroundColor = [UIColor grayColor];
        _txtKeyCode.verticalPadding = 10;
        _txtKeyCode.horizontalPadding = 10;
        if ([_txtKeyCode respondsToSelector:@selector(setAttributedPlaceholder:)]) {
            UIColor *color = [UIColor colorWithHexString:@"333333"];
            _txtKeyCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString: @"Take part in lucky draw" attributes:@{NSForegroundColorAttributeName: color}];
        }
    }
    return _txtKeyCode;
}

- (UIButton *) btnJoin{
    if (!_btnJoin) {
        _btnJoin = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnJoin.frame = CGRectMake(self.txtKeyCode.frame.origin.x + self.txtKeyCode.frame.size.width, 0, self.textAndButtonView.frame.size.width - self.txtKeyCode.frame.size.width, self.textAndButtonView.frame.size.height);
        [_btnJoin setTitle:@"Join" forState:UIControlStateNormal];
        _btnJoin.titleLabel.font = [UIFont fontWithName:ROBOTO_LIGHT size:18];
        _btnJoin.backgroundColor = [UIColor colorWithHexString:@"01457b"];
        [_btnJoin addTarget:self action:@selector(onJoin:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnJoin;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNavBarTitle:@"Live Poll"];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.lblTitle];
    [self.scrollView addSubview:self.textAndButtonView];
    [self.textAndButtonView addSubview:self.txtKeyCode];
    [self.textAndButtonView addSubview:self.btnJoin];
    
    scrollViewOriginalSize = self.scrollView.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    //[self.txtKeyCode becomeFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated{
    [self registerForKeyboardNotifications];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void) moveScrollView:(CGFloat) theViewCenterY {
    //---get the y-coordinate of the view---
    NSLog(@"the view Center y %f",theViewCenterY);
    CGFloat viewCenterY = theViewCenterY + 20;
    
    //---calculate how much visible space is left---
    CGFloat freeSpaceHeight = applicationFrame.size.height - keyboardBounds.size.height;
    
    //---calculate how much the scrollview must scroll---
    CGFloat scrollAmount = viewCenterY - freeSpaceHeight / 2.0;
    if (scrollAmount < 0) scrollAmount = 0;
    
    //---set the new scrollView contentSize---
    self.scrollView.contentSize = CGSizeMake(applicationFrame.size.width, applicationFrame.size.height +keyboardBounds.size.height);
    
    //---scroll the ScrollView---
    [self.scrollView setContentOffset:CGPointMake(0, scrollAmount) animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([Utility isTall]) [self moveScrollView:self.textAndButtonView.center.y + self.scrollView.frame.origin.y];
    else [self moveScrollView:self.textAndButtonView.center.y + self.scrollView.frame.origin.y+STATUS_BAR_NAV_BAR_HEIGHT-20];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSLog(@"Keyboard shown");
    
   /* NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect bkgndRect = activeField.superview.frame;
    bkgndRect.size.height += kbSize.height;*/
    //[activeField.superview setFrame:bkgndRect];
    //[self.scrollView setContentOffset:CGPointMake(0.0, activeField.frame.origin.y-kbSize.height) animated:YES];
    
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    //    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    //    self.scrollView.contentInset = contentInsets;
    //    self.scrollView.scrollIndicatorInsets = contentInsets;
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

- (void) onJoin:(UIButton *)sender{
    [self.navigationController pushViewController:self.sessionsPollViewController animated:YES];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

@end
