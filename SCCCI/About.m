//
//  About.m
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "About.h"


@implementation About

#pragma mark - JSON serialization
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"about_intro_annual" : @"about_intro_annual",
             @"about_intro_info" : @"about_intro_info",
             @"about_intro_sm" : @"about_intro_sm",
             @"about_sccci" : @"about_sccci",
             @"about_ida" : @"about_ida",
             @"about_lianhe_zaobao" : @"about_lianhe_zaobao",
             @"about_intro_annual_cn" : @"about_intro_annual_cn",
             @"about_intro_info_cn" : @"about_intro_info_cn",
             @"about_intro_sm_cn" : @"about_intro_sm_cn",
             @"about_sccci_cn" : @"about_sccci_cn",
             @"about_ida_cn" : @"about_ida_cn",
             @"about_lianhe_zaobao_cn" : @"about_lianhe_zaobao_cn"
             };
}

#pragma mark - Managed object serialization
+ (NSString *)managedObjectEntityName {
    return @"About";
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    return @{
             @"about_intro_annual" : @"about_intro_annual",
             @"about_intro_info" : @"about_intro_info",
             @"about_intro_sm" : @"about_intro_sm",
             @"about_sccci" : @"about_sccci",
             @"about_ida" : @"about_ida",
             @"about_lianhe_zaobao" : @"about_lianhe_zaobao",
             @"about_intro_annual_cn" : @"about_intro_annual_cn",
             @"about_intro_info_cn" : @"about_intro_info_cn",
             @"about_intro_sm_cn" : @"about_intro_sm_cn",
             @"about_sccci_cn" : @"about_sccci_cn",
             @"about_ida_cn" : @"about_ida_cn",
             @"about_lianhe_zaobao_cn" : @"about_lianhe_zaobao_cn"
             };
}

+ (NSSet *)propertyKeysForManagedObjectUniquing {
    return [NSSet setWithObject:@"about_sccci"];
}

+ (void) deleteAllWithContext: (NSManagedObjectContext *) context {
    NSFetchRequest * fectchRequest = [[NSFetchRequest alloc] init];
    [fectchRequest setEntity:[NSEntityDescription entityForName:@"About" inManagedObjectContext:context]];
    [fectchRequest setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * objArr = [context executeFetchRequest:fectchRequest error:&error];
    //error handling goes here
    for (NSManagedObject * managedObj in objArr) {
        [context deleteObject:managedObj];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //more error handling here
}

@end
