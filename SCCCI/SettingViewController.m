//
//  SettingViewController.m
//  SCCCI
//
//  Created by Zayar on 6/27/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "SettingViewController.h"
#import "NameWithButtonTableViewCell.h"
#import "NameWithSwitchTableViewCell.h"
#import "SimpleNameAndValueTableViewCell.h"
#import "ButtonTableViewCell.h"
#import "StringTable.h"
#import "AppDelegate.h"
#import "SIAlertView.h"
#import "Utility.h"
#import "SANetworkTester.h"
@interface SettingViewController ()
@property (strong, nonatomic) NSArray * arrList;
@property (strong, nonatomic) NSArray * arrNames;
@property (strong, nonatomic) NSArray * arrNames_cn;
@end

@implementation SettingViewController
@synthesize strTitle;
- (NSArray *) arrNames{
    if (!_arrNames) {
        //_arrNames = [NSArray arrayWithObjects: @"Push Notification(通知)", @"Language(语言)",@"Sync",@"App version information",@"About app infromation",nil];
        _arrNames = [NSArray arrayWithObjects: @"Push Notification(通知)", @"Language(语言)",nil];
    }
    return _arrNames;
}

- (NSArray *) arrNames_cn{
    if (!_arrNames_cn) {
        _arrNames_cn = [NSArray arrayWithObjects: @"通知", @"语言",@"App version information",@"About app infromation",nil];
    }
    return _arrNames_cn;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
    [self.tbl reloadData];
    
    self.tbl.separatorColor = [UIColor clearColor];
    [self setNavBarTitle:@"Settings"];
}

- (void)viewWillAppear:(BOOL)animated{
    [self setNavBarTitle:strTitle];
    //[self switchLanguage];
    self.arrList = self.arrNames;
    [self.tbl reloadData];
    
}

- (void) switchLanguage{
    if ([self getCurrentLanguage] == LANGAUGE_EGN) {
        self.arrList = self.arrNames;
        [self.tbl reloadData];
    }
    else if([self getCurrentLanguage] == LANGAUGE_CN){
        self.arrList = self.arrNames_cn;
        [self.tbl reloadData];
    }
}

#pragma mark tableview datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        static NSString *CellIdentifier = @"NameWithSwitchTableViewCell";
        NameWithSwitchTableViewCell *cell = (NameWithSwitchTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[NameWithSwitchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setUpViews];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
    
        [cell viewLoadWithString:[self.arrList objectAtIndex:indexPath.row] andSwitch:YES];
        
        return cell;
    }
    else if(indexPath.row == 1){
        static NSString *CellIdentifier = @"NameWithButtonTableViewCell";
        NameWithButtonTableViewCell *cell = (NameWithButtonTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[NameWithButtonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setUpViews];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
        cell.owner = self;
        [cell viewLoadWithString:[self.arrList objectAtIndex:indexPath.row] andSwitch:YES];
        
        return cell;
    }
    else if(indexPath.row == 2){
        static NSString *CellIdentifier = @"ButtonTableViewCell";
        ButtonTableViewCell *cell = (ButtonTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[ButtonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell viewSetup];
        }
        [cell viewSetup];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
        cell.owner = self;
        
        return cell;
    }
    /*else if(indexPath.row == 3) {
        static NSString *CellIdentifier = @"SimpleNameAndValueTableViewCell";
        SimpleNameAndValueTableViewCell *cell = (SimpleNameAndValueTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[SimpleNameAndValueTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setUpViews];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
        [cell viewLoadWithString:[self.arrList objectAtIndex:indexPath.row] andValue:[Utility build]];
        
        return cell;
    }
    else if(indexPath.row == 4) {
        static NSString *CellIdentifier = @"SimpleNameAndValueTableViewCell";
        SimpleNameAndValueTableViewCell *cell = (SimpleNameAndValueTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[SimpleNameAndValueTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setUpViews];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
        [cell viewLoadWithString:[self.arrList objectAtIndex:indexPath.row] andValue:[Utility appVersion]];
        
        return cell;
    }*/
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [self.arrList count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

#pragma mark tableview delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //self.speakerDetailViewController.speaker = [self.arrSpeakers objectAtIndex:indexPath.row];
    //[self.navigationController pushViewController:self.speakerDetailViewController animated:YES];
}

- (void) onForceSyncClick:(UIButton *) sender{
    NSLog(@"onForceSyncClick");
    
    if ([SANetworkTester networkStatus] == SANotReachable) {
        NSLog(@"connection error!!");
        [Utility showAlert:APP_TITLE message:@"No internet connectivity!"];
    }
    else if([SANetworkTester networkStatus] == SAReachableViaWiFi || [SANetworkTester networkStatus] == SAReachableViaWWAN){
        AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        ServiceMappingClient *client = [delegate client];
        NSLog(@"connection avialable!!");
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Please wait" andMessage:@"Synchronising data set"];
        [alertView show];
        [client syncDataWithCompletionBlock:^(BOOL status) {
            
            [alertView dismissAnimated:YES];
        }];
    }
}


- (void) onLanguageClick:(UIButton *) sender{
    //[self switchLanguage];
}

@end
