//
//  ExhibitorDetailProfileTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 6/24/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ExhibitorDetailProfileTableViewCell.h"
#import "StringTable.h"
#import "UIImageView+WebCache.h"
#import "TTTAttributedLabel.h"
@interface ExhibitorDetailProfileTableViewCell()
@property (nonatomic,strong) UILabel * lblName;
@property (nonatomic,strong) UILabel * lblBoothNo;
@property (nonatomic,strong) TTTAttributedLabel * lblWebsite;
@property (nonatomic,strong) UILabel * lblContactNo;
@property (nonatomic,strong) UILabel * lblContactNo2;
@property (nonatomic,strong) UIImageView * imgProfileView;
@property (nonatomic,strong) UIWebView * webContact;
@property (nonatomic,strong) UITextView * textView;

@end
@implementation ExhibitorDetailProfileTableViewCell
- (UIWebView *) webContact{
    if (!_webContact) {
        _webContact = [[UIWebView alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.imgProfileView.frame.origin.y + self.imgProfileView.frame.size.height + 5, 290, 22*4)];
    }
    return _webContact;
}
- (UITextView *) textView{
    if (!_textView) {
        _textView = [[UITextView alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.imgProfileView.frame.origin.y + self.imgProfileView.frame.size.height + 5, 290, 20*4)];
        _textView.editable = NO;
        _textView.selectable = YES;
        //_textView.dataDetectorTypes = UIDataDetectorTypeLink;
        _textView.dataDetectorTypes = UIDataDetectorTypeAll;
        
    }
    return _textView;
}
- (UIImageView *) imgProfileView{
    if (!_imgProfileView) {
        _imgProfileView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 10, 90, 90)];
        _imgProfileView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imgProfileView;
}

- (UILabel *)lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(self.imgProfileView.frame.origin.x + self.imgProfileView.frame.size.width + 10, 10, 189, 23)];
        _lblName.font = [UIFont fontWithName:ROBOTO_BOLD size:16];
        _lblName.textColor = [UIColor colorWithHexString:@"01457b"];
    }
    return _lblName;
}

- (UILabel *) lblBoothNo{
    if (!_lblBoothNo) {
        _lblBoothNo = [[UILabel alloc] initWithFrame:CGRectMake(self.imgProfileView.frame.origin.x + self.imgProfileView.frame.size.width + 10, self.lblName.frame.origin.y + self.lblName.frame.size.height + 5, 189, 23)];
        _lblBoothNo.font = [UIFont fontWithName:ROBOTO_REGUALAR size:13];
        _lblBoothNo.textColor = [UIColor colorWithHexString:@"333333"];
    }
    return _lblBoothNo;
}

- (UILabel *)lblWebsite{
    if (!_lblWebsite) {
        _lblWebsite = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.imgProfileView.frame.origin.y + self.imgProfileView.frame.size.height + 5, 290, 25*4)];
        _lblWebsite.font = [UIFont fontWithName:ROBOTO_REGUALAR size:13];
        _lblWebsite.textColor = [UIColor colorWithHexString:@"333333"];
        
        _lblWebsite.numberOfLines = 0;
        _lblWebsite.delegate = self;
    }
    return _lblWebsite;
}

- (UILabel *)lblContactNo{
    if (!_lblContactNo) {
        _lblContactNo = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.lblWebsite.frame.origin.y + self.lblWebsite.frame.size.height, 290, 23)];
        _lblContactNo.font = [UIFont fontWithName:ROBOTO_REGUALAR size:13];
        _lblContactNo.textColor = [UIColor colorWithHexString:@"333333"];
    }
    return _lblContactNo;
}

- (UILabel *)lblContactNo2{
    if (!_lblContactNo2) {
        _lblContactNo2 = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, self.lblContactNo.frame.origin.y + self.lblContactNo.frame.size.height, 290, 23)];
        _lblContactNo2.font = [UIFont fontWithName:ROBOTO_REGUALAR size:13];
        _lblContactNo2.textColor = [UIColor colorWithHexString:@"333333"];
    }
    return _lblContactNo2;
}

- (void) setUpViews{
    [self addSubview:self.imgProfileView];
    [self addSubview:self.lblName];
    [self addSubview:self.lblBoothNo];
    //[self addSubview:self.lblWebsite];
    //[self addSubview:self.lblContactNo];
    //[self addSubview:self.lblContactNo2];
    //[self addSubview:self.webContact];
    [self addSubview:self.textView];
}

- (void) viewLoadWithName:(NSString *)strName andBoothNo:(NSString *)strBoothNo andWebsite:(NSString *)strWebsite andImageURL:(NSString *)strImageUrl andContactNo:(NSString *)strContactNo andContactNo2:(NSString *)strContactNo2{
    [self.imgProfileView setImageWithURL:[NSURL URLWithString:strImageUrl] placeholderImage:nil];
    self.lblName.text = strName;
    self.lblBoothNo.text = strBoothNo;
    NSLog(@"height %f",self.lblContactNo2.frame.origin.y + self.lblContactNo2.frame.size.height + 5);
    
    NSString* content = strContactNo ;
    [content UTF8String];
   content = [content stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
    content =[content stringByReplacingOccurrencesOfString:@"</p>" withString:@"\n"];
    content =[content stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    content =[content stringByReplacingOccurrencesOfString:@"<strong>" withString:@""];
    content =[content stringByReplacingOccurrencesOfString:@"</strong>" withString:@""];
    NSLog(@"str content %@",content);
    self.textView.text = content;
}

- (NSString *)setPhoneContent:(NSString *)str{
    return [NSString stringWithFormat:@"Phone :%@",str];
}

- (NSString *)setEmailContent:(NSString *)str{
    return [NSString stringWithFormat:@"Email :%@",str];
}

- (NSString *)setAddressContent:(NSString *)str{
    return [NSString stringWithFormat:@"Address :%@",str];
}

#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url
{
    [[[UIActionSheet alloc] initWithTitle:[url absoluteString] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Open link in browser", nil), nil] showInView:self];
}

- (void)actionSheet:(UIActionSheet *)actionSheet
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    NSURL *url = [NSURL URLWithString:actionSheet.title];
    [[UIApplication sharedApplication] openURL:url];
}


@end

