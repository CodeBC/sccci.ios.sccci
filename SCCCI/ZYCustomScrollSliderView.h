//
//  CustomScrollSliderView.h
//  IACTAC
//
//  Created by Zayar on 10/31/13.
//  Copyright (c) 2013 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ZYCustomScrollSliderViewDelegate;
typedef enum {
	ScrollViewModeNotInitialized2,           // view has just been loaded
	ScrollViewModePaging2,                   // fully zoomed out, swiping enabled
	ScrollViewModeZooming2,                  // zoomed in, panning enabled
} ScrollViewMode2;
@interface ZYCustomScrollSliderView : UIView
{
    UIScrollView * scrollView;
    UIPageControl * pageControl;
    ScrollViewMode2 scrollViewMode2;
    BOOL pageControlUsed;
    int currentPagingIndex;
    NSArray * arrContentView;
    NSTimer * myTimer;
    int slidingIndex;
}
@property id<ZYCustomScrollSliderViewDelegate> owner;
- (void) setUpViewWithFrame:(CGRect)frame;
- (void) loadImageViewWith:(NSArray *)arr;

- (void) sliderStop;
- (void) sliderPlay;
- (void) cleanUpViews;
@end

@protocol ZYCustomScrollSliderViewDelegate
- (void) onSliderClick:(ZYCustomScrollSliderView *)sliderView andTag:(NSInteger) tag;
@end
