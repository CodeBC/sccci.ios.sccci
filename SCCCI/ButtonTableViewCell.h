//
//  ButtonTableViewCell.h
//  SCCCI
//
//  Created by Zayar on 7/21/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ButtonTableViewCellDelegate;
@interface ButtonTableViewCell : UITableViewCell
@property id <ButtonTableViewCellDelegate> owner;
- (void) viewSetup;
@end
@protocol ButtonTableViewCellDelegate
- (void) onForceSyncClick:(UIButton *) sender;
@end