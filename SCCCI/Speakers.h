//
//  Speakers.h
//  SCCCI
//
//  Created by Zayar on 6/27/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"


@interface Speakers : MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (copy, nonatomic, readonly) NSString * speaker_avatar;
@property (copy, nonatomic, readonly) NSString * speaker_company;
@property (copy, nonatomic, readonly) NSString * speaker_description;
@property (copy, nonatomic, readonly) NSString * speaker_designation;
@property (copy, nonatomic, readonly) NSString * speaker_company_cn;
@property (copy, nonatomic, readonly) NSString * speaker_designation_cn;
@property (nonatomic, readonly) NSNumber * speaker_id;
@property (copy, nonatomic, readonly) NSString * speaker_name;
@property (copy, nonatomic, readonly) NSString * speaker_name_cn;
@property (copy, nonatomic, readonly) NSString * speaker_description_cn;
+ (void) deleteAllWithContext: (NSManagedObjectContext *) context;
@end
