//
//  AppDelegate.h
//  SCCCI
//
//  Created by Zayar on 6/12/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceMappingClient.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) ServiceMappingClient *client;
@property (strong, nonatomic) UIWindow *window;

@end
