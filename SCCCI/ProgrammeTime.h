//
//  ProgrammeTime.h
//  SCCCI
//
//  Created by Zayar on 7/13/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"
@interface ProgrammeTime : MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (nonatomic, retain) NSString * programme_time;
+ (void) deleteAllWithContext: (NSManagedObjectContext *) context;
@end
