//
//  AppDelegate.m
//  SCCCI
//
//  Created by Zayar on 6/12/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "AppDelegate.h"
#import "SANetworkTester.h"
#import "StringTable.h"
#import "SIAlertView.h"
#import "PromotionsViewController.h"
#import "NewsViewController.h"
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    
    self.client = [[ServiceMappingClient alloc] init];
    [self setDefaultEngLanguage];
    
    
    if ([SANetworkTester networkStatus] == SANotReachable) {
        NSLog(@"connection error!!");
    }
    else if([SANetworkTester networkStatus] == SAReachableViaWiFi || [SANetworkTester networkStatus] == SAReachableViaWWAN){
        NSLog(@"connection avialable!!");
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Please wait" andMessage:@"Synchronising data set"];
        [alertView show];
        [self.client syncDataWithCompletionBlock:^(BOOL status) {
            
            [alertView dismissAnimated:YES];
            if (launchOptions != nil)
            {
                NSDictionary *dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
                NSLog(@"Notification Dicionary %@", dictionary);
                if (dictionary != nil)
                {
                    if([dictionary[@"alert_type"] isEqualToString:@"promo"]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotiArrived" object:self userInfo: dictionary];
                    } else {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotiArrived" object:self userInfo: dictionary];
                    }
                }
            }

        }];
    }
    
    
    NSLog(@"My token is: %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]);
    
    return YES;
}

- (void)setDefaultEngLanguage{
    NSInteger lang = [StringTable getCurrentLangage];
    if (lang == 0) {
        [StringTable setCurrentLanguage:LANGAUGE_EGN];
    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
	NSLog(@"My token is: %@", deviceToken);
    
    NSString* newToken = [deviceToken description];
    
    newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"The description of device token is %@", newToken);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:@"deviceToken"] == nil) {
        [[NSUserDefaults standardUserDefaults] setObject:newToken forKey:@"deviceToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [[self client] registerForPush:newToken andDeviceType:@"I"];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)dictionary {
    NSString *title = @"New News!";
    if([dictionary[@"alert_type"] isEqualToString:@"promo"]) {
        title = @"New Promotion!";
    }
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Notification" andMessage:title];
    
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                              NSLog(@"Button1 Clicked");
                          }];
    [alertView addButtonWithTitle:@"View"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              SIAlertView *alertView2 = [[SIAlertView alloc] initWithTitle:@"Please wait" andMessage:@"Synchronising data set"];
                              [alertView2 show];
                              [self.client syncDataWithCompletionBlock:^(BOOL status) {
                                  
                                  [alertView2 dismissAnimated:YES];
                                  if (dictionary != nil)
                                  {
                                      if([dictionary[@"alert_type"] isEqualToString:@"promo"]) {
                                          [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotiArrived" object:self userInfo: dictionary];
                                      } else {
                                          [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotiArrived" object:self userInfo: dictionary];
                                      }
                                  }
                              }];
                          }];
    [alertView show];

}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) sync {
    if ([SANetworkTester networkStatus] == SANotReachable) {
        NSLog(@"connection error!!");
    }
    else if([SANetworkTester networkStatus] == SAReachableViaWiFi || [SANetworkTester networkStatus] == SAReachableViaWWAN){
        NSLog(@"connection avialable!!");
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Please wait" andMessage:@"Synchronising data set"];
        [alertView show];
        [self.client syncDataWithCompletionBlock:^(BOOL status) {
            
            [alertView dismissAnimated:YES];
        }];
    }
    
    
}

@end
