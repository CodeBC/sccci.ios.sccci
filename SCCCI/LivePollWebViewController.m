//
//  LibePollWebViewController.m
//  SCCCI
//
//  Created by Zayar on 6/26/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "LivePollWebViewController.h"
#import "StringTable.h"
@interface LivePollWebViewController ()

@end

@implementation LivePollWebViewController
@synthesize strTitle;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"Live Poll"];
    self.webView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated{
    [self setNavBarTitle:strTitle];
    NSURLRequest * urlRequest;
    NSInteger lang = [StringTable getCurrentLangage];
    if (lang == LANGAUGE_EGN) {
        urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",APP_BASED_LINK,POLL_LINK]]];
    }
    else if(lang == LANGAUGE_CN)
        urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",APP_BASED_LINK,POLL_CN_LINK]]];
    [self.webView loadRequest:urlRequest];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [SVProgressHUD show];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismiss];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@",error]];
}

@end
