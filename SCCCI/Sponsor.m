//
//  Sponsor.m
//  SCCCI
//
//  Created by Zayar on 7/11/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Sponsor.h"


@implementation Sponsor
#pragma mark - JSON serialization
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"sponsor_id" : @"sponsor_id",
             @"sponsor_name" : @"sponsor_name",
             @"sponsor_name_cn" : @"sponsor_name_cn",
             @"sponsor_about" : @"sponsor_about",
             @"sponsor_about_cn" : @"sponsor_about_cn",
             @"sponsor_info" : @"sponsor_info",
             @"sponsor_booth" : @"sponsor_booth",
             @"sponsor_type" : @"sponsor_type",
             @"sponsor_logo" : @"sponsor_logo",
            @"sponsor_contact" : @"sponsor_contact",
             @"sponsor_contact_cn" : @"sponsor_contact_cn"
             };
}

#pragma mark - Managed object serialization
+ (NSString *)managedObjectEntityName {
    return @"Sponsor";
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    return @{
             @"sponsor_id" : @"sponsor_id",
             @"sponsor_name" : @"sponsor_name",
             @"sponsor_name_cn" : @"sponsor_name_cn",
             @"sponsor_about" : @"sponsor_about",
             @"sponsor_about_cn" : @"sponsor_about_cn",
             @"sponsor_info" : @"sponsor_info",
             @"sponsor_booth" : @"sponsor_booth",
             @"sponsor_type" : @"sponsor_type",
             @"sponsor_logo" : @"sponsor_logo",
             @"sponsor_contact" : @"sponsor_contact",
              @"sponsor_contact_cn" : @"sponsor_contact_cn"
             };
}

+ (NSSet *)propertyKeysForManagedObjectUniquing {
    return [NSSet setWithObject:@"sponsor_id"];
}

+ (void) deleteAllWithContext: (NSManagedObjectContext *) context {
    NSFetchRequest * fectchRequest = [[NSFetchRequest alloc] init];
    [fectchRequest setEntity:[NSEntityDescription entityForName:@"Sponsor" inManagedObjectContext:context]];
    [fectchRequest setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * objArr = [context executeFetchRequest:fectchRequest error:&error];
    //error handling goes here
    for (NSManagedObject * managedObj in objArr) {
        [context deleteObject:managedObj];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //more error handling here
}

@end
