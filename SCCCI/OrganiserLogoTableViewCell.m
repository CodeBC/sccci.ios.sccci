//
//  OrganiserLogoTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "OrganiserLogoTableViewCell.h"
#import "StringTable.h"
@interface OrganiserLogoTableViewCell()
@property (nonatomic, strong) UIImageView * imgLogoView;
@end
@implementation OrganiserLogoTableViewCell
- (UIImageView *) imgLogoView{
    if (!_imgLogoView) {
        _imgLogoView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 2, 180, 64)];
        _imgLogoView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imgLogoView;
}

- (void)setUpViews{
    [self addSubview:self.imgLogoView];
}

- (void)viewLoadWithString:(NSString *)strName{
    [self.imgLogoView setImage:[UIImage imageNamed:strName]];
}

@end
