//
//  PromoDetailViewController.m
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "PromoDetailViewController.h"
#import "StringTable.h"
#import "UIImageView+WebCache.h"
#import "NSString+HTML.h"
#import "PromoMapViewController.h"
@interface PromoDetailViewController ()
@property (nonatomic, strong) UIToolbar * bottonToolBar;
@property (nonatomic, strong) UIActivityViewController *activityViewController;
@end

@implementation PromoDetailViewController
@synthesize promotion;
- (UIToolbar *) bottonToolBar{
    if (!_bottonToolBar) {
        _bottonToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, (self.view.frame.size.height - 44) - STATUS_BAR_NAV_BAR_HEIGHT, self.view.frame.size.width, 44)];
        NSLog(@"tool bar y: %f",self.view.frame.size.height - 44);
        _bottonToolBar.tintColor = [UIColor blackColor];
    }
    return _bottonToolBar;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavBarTitle:@"View Promo"];
    
    [self.view addSubview:self.bottonToolBar];
    
    UIBarButtonItem * btnLocateExhibitor = [[UIBarButtonItem alloc] initWithTitle:@"Locate Exhibitor" style:UIBarButtonItemStylePlain target:self action:@selector(onLocateExhibitor)];
    UIBarButtonItem * btnShareFb = [[UIBarButtonItem alloc] initWithTitle:@"Share on FB" style:UIBarButtonItemStylePlain target:self action:@selector(onShare)];
    UIBarButtonItem * btnShareICon = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_share"] style:UIBarButtonItemStylePlain target:self action:@selector(onShare)];
    btnShareICon.tintColor = [UIColor blueColor];
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //fixedSpace.width = 10;
    [self.bottonToolBar setItems:@[btnLocateExhibitor, fixedSpace, fixedSpace,btnShareFb,btnShareICon]];
}

- (void)viewWillAppear:(BOOL)animated{
    [self viewLoadWithPromoition:promotion];
}

- (void)viewLoadWithPromoition:(Promotion *)obj{
    [self.imgView setImageWithURL:[NSURL URLWithString:obj.promotion_image] placeholderImage:[UIImage imageNamed:@"promo_image_sample"]];
    
    NSInteger lang = [StringTable getCurrentLangage];
    if (lang == LANGAUGE_EGN) {
        self.lblName.text = obj.promotion_title;
        self.lblDescription.text = [obj.promotion_description stringByDecodingHTMLEntities];
    }
    else if (lang == LANGAUGE_CN){
        self.lblName.text = obj.promotion_title_cn;
        self.lblDescription.text = [obj.promotion_description_cn stringByDecodingHTMLEntities];
    }
    
}

- (void)onLocateExhibitor{
    PromoMapViewController *controller = [[PromoMapViewController alloc] init];
    controller.promotion = self.promotion;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)onShare{
    self.activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[[NSString stringWithFormat:@"%@ \n %@",SHARE_MSG,SHARE_LINK]] applicationActivities:nil];
    [self presentViewController:self.activityViewController animated:YES completion:nil];
}

@end
