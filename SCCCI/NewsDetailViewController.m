//
//  NewsDetailViewController.m
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "NewsDetailViewController.h"
#import "StringTable.h"
@interface NewsDetailViewController ()

@end

@implementation NewsDetailViewController
@synthesize news;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"News Detail"];
}

- (void)viewWillAppear:(BOOL)animated{
    //pathForResource:@"AboutHTML/about_template" ofType:@"html"
    NSString* path = [[NSBundle mainBundle] pathForResource:@"AboutHTML/about_template"
                                                    ofType:@"html"];
    NSString* content = [NSString stringWithContentsOfFile:path
                                                 encoding:NSUTF8StringEncoding
                                                    error:NULL];
    
    NSInteger lang = [StringTable getCurrentLangage];
    if (lang == LANGAUGE_EGN) {
        content = [content stringByReplacingOccurrencesOfString:@"Text Title" withString:news.news_title];
        content = [content stringByReplacingOccurrencesOfString:@"content_detail" withString:news.news_description];
    }
    else if (lang == LANGAUGE_CN){
        content = [content stringByReplacingOccurrencesOfString:@"Text Title" withString:news.news_title_cn];
        content = [content stringByReplacingOccurrencesOfString:@"content_detail" withString:news.news_description_cn];
    }
    //NSLog(@"fname %@",content);
    [self.webView loadHTMLString:content baseURL:nil];
}

@end
