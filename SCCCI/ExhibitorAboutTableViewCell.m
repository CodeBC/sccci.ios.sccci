//
//  ExhibitorAboutTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 6/24/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "ExhibitorAboutTableViewCell.h"
#import "StringTable.h"
#import "RTLabel.h"
@interface ExhibitorAboutTableViewCell()
@property (nonatomic,strong) RTLabel * lblAbout;
@property (nonatomic,strong) UIWebView * webAbout;
@end
@implementation ExhibitorAboutTableViewCell
@synthesize owner;
- (RTLabel *) lblAbout{
    if (!_lblAbout) {
        _lblAbout = [[RTLabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 10, 290, 0)];
        _lblAbout.font = [UIFont fontWithName:ROBOTO_REGUALAR size:12];
        _lblAbout.textColor = [UIColor colorWithHexString:@"333333"];
//        _lblAbout.numberOfLines = 0;
    }
    return _lblAbout;
}

- (UIWebView *) webAbout{
    if (!_webAbout) {
        _webAbout = [[UIWebView alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 10, 290, 0)];
        _webAbout.delegate = self;
        _webAbout.userInteractionEnabled = NO;
        
    }
    return _webAbout;
}

- (void) setUpViews{
    //[self addSubview:self.lblAbout];
    [self addSubview:self.webAbout];
}

- (void) viewLoadWithAbout:(NSString *)strAbout{
    /*CGRect newFrame = self.lblAbout.frame;
    newFrame.size.height = [ExhibitorAboutTableViewCell heightForCellWithPost:strAbout];
    self.lblAbout.frame = newFrame;
    self.lblAbout.text = strAbout;*/
    [self.webAbout loadHTMLString:strAbout baseURL:Nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
    CGRect frame = aWebView.frame;
    frame.size.height = 1;
    aWebView.frame = frame;
    CGSize fittingSize = [aWebView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    aWebView.frame = frame;
    
    NSLog(@"size: %f, %f", fittingSize.width, self.webAbout.scrollView.contentSize.height);
    NSLog(@"webView height: %f", [[self.webAbout stringByEvaluatingJavaScriptFromString: @"document.body.offsetHeight"] floatValue]);

    NSLog(@"Finished loading");
    [owner webDescriptionCellDidFinish:self shouldAssignHeight:[[self.webAbout stringByEvaluatingJavaScriptFromString: @"document.body.offsetHeight"] floatValue] + 80.0f];
}

- (void) forceMeasure {
//    NSLog(@"Forcing to measure");
//    [owner webDescriptionCellDidFinish:self shouldAssignHeight:self.webAbout.scrollView.contentSize.height];
}

+ (CGFloat)heightForCellWithPost:(NSString *)strAbout {
    NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:ROBOTO_REGUALAR size:12.0f] forKey: NSFontAttributeName];
    
    CGSize sizeToFit  = [strAbout boundingRectWithSize:CGSizeMake(290.0f, CGFLOAT_MAX)
                                               options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin
                                            attributes:stringAttributes context:nil].size;
    return fmaxf(50.0f, sizeToFit.height+ 30 + 50);
}
@end
