//
//  ExhibitorCates.h
//  SCCCI
//
//  Created by Zayar on 7/14/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"
@interface ExhibitorCates : MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (nonatomic, retain) NSString * exhibitor_category;

@end
