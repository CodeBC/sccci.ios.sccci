//
//  NameWithButtonTableViewCell.m
//  SCCCI
//
//  Created by Zayar on 7/4/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "NameWithButtonTableViewCell.h"
#import "StringTable.h"
@interface NameWithButtonTableViewCell()
{
    BOOL langangeEng;
}
@property (nonatomic, strong) UILabel * lblName;
@property (nonatomic, strong) UIButton * btnOption;
@end
@implementation NameWithButtonTableViewCell
@synthesize owner;
- (UILabel *)lblName{
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, 5, 220, 30)];
        _lblName.numberOfLines = 2;
        _lblName.font = [UIFont fontWithName:ROBOTO_BOLD size:14];
        _lblName.textColor = [UIColor colorWithHexString:@"333333"];
    }
    return _lblName;
}

- (UIButton *) btnOption{
    if (!_btnOption) {
        _btnOption = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnOption.frame = CGRectMake(self.lblName.frame.origin.x + self.lblName.frame.size.width+5, 5, 50, 30);
        _btnOption.titleLabel.font = [UIFont fontWithName:ROBOTO_BOLD size:11];
        [_btnOption setTitle:@"Eng | 华文" forState:UIControlStateNormal];
        [_btnOption addTarget:self action:@selector(onOption:) forControlEvents:UIControlEventTouchUpInside];
        [_btnOption addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
        [self showEnglishTitleInOptionButton:langangeEng andButton:_btnOption];
        langangeEng = FALSE;
    }
    return _btnOption;
}

- (void) showEnglishTitleInOptionButton:(BOOL)show andButton:(UIButton *)btn{
     NSMutableAttributedString * title = [[NSMutableAttributedString alloc] initWithString:btn.currentTitle];
    if (show) {
        [title addAttributes:@{ NSStrokeWidthAttributeName:@3,NSStrokeColorAttributeName:btn.tintColor } range:NSMakeRange(0, 3)];
        [btn setAttributedTitle:title forState:UIControlStateNormal];
    }
    else{
        [title addAttributes:@{ NSStrokeWidthAttributeName:@3,NSStrokeColorAttributeName:btn.tintColor } range:NSMakeRange(5, 3)];
        [btn setAttributedTitle:title forState:UIControlStateNormal];
    }
}

- (void)setUpViews{
    langangeEng = YES;
    [self addSubview:self.lblName];
    [self addSubview:self.btnOption];
}

- (void) onOption:(UIButton *)sender{
    if (langangeEng) {
        [self showEnglishTitleInOptionButton:langangeEng andButton:_btnOption];
        langangeEng = FALSE;
        [StringTable setCurrentLanguage:LANGAUGE_EGN];
    }
    else{
        [self showEnglishTitleInOptionButton:langangeEng andButton:_btnOption];
        langangeEng = TRUE;
        [StringTable setCurrentLanguage:LANGAUGE_CN];
    }
    
}

- (void) onClick:(UIButton *)sender{
    [owner onLanguageClick:sender];
}

- (void)viewLoadWithString:(NSString *)strName andSwitch:(BOOL)isSwitch{
    [self.lblName setText:strName];
    NSInteger lang = [StringTable getCurrentLangage];
    if (lang == LANGAUGE_EGN) {
        langangeEng = YES;
        [self onOption:self.btnOption];
    }
    else if(lang == LANGAUGE_CN){
        langangeEng = FALSE;
        [self onOption:self.btnOption];
    }
    
}


@end
