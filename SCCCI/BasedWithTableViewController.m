//
//  BasedWithTableViewController.m
//  SCCCI
//
//  Created by Zayar on 6/25/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "BasedWithTableViewController.h"
#import "StringTable.h"
#import "Utility.h"
@interface BasedWithTableViewController ()

@end

@implementation BasedWithTableViewController

- (UITableView *)tbl{
    if (!_tbl) {
        _tbl = [[UITableView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.topBannerSilder.frame.origin.y + self.topBannerSilder.frame.size.height, self.view.frame.size.width, self.view.frame.size.height -(self.topBannerSilder.frame.origin.y + self.topBannerSilder.frame.size.height) - STATUS_BAR_NAV_BAR_HEIGHT)];
    }
    return _tbl;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //if([self respondsToSelector:@selector(edgesForExtendedLayout)])
        //[self setEdgesForExtendedLayout:UIRectEdgeBottom];
    
    // Do any additional setup after loading the view.
    [self.view addSubview:self.tbl];
    
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        if ([self.tbl respondsToSelector:@selector(separatorInset)]) {
            [self.tbl setSeparatorInset:UIEdgeInsetsZero];
        }
    }
}

- (void) hideTopBanner{
    self.topBannerSilder.hidden = YES;
    [self.tbl setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y , self.view.frame.size.width, self.view.frame.size.height - STATUS_BAR_NAV_BAR_HEIGHT)];
    [self hideBasedTopBanner];
}

@end
