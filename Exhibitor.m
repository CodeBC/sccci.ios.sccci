//
//  SCCCI.m
//  SCCCI
//
//  Created by Zayar on 7/13/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

//
//  Exhibitor.m
//  SCCCI
//
//  Created by Zayar on 7/12/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Exhibitor.h"
@implementation Exhibitor

#pragma mark - JSON serialization
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"exhibitor_id" : @"exhibitor_id",
             @"exhibitor_type" : @"exhibitor_type",
             @"exhibitor_name" : @"exhibitor_name",
             @"exhibitor_name_cn" : @"exhibitor_name_cn",
             @"exhibitor_logo" : @"exhibitor_logo",
             @"exhibitor_booth" : @"exhibitor_booth",
             @"exhibitor_contact" : @"exhibitor_contact",
             @"exhibitor_contact_cn" : @"exhibitor_contact_cn",
             @"exhibitor_about" : @"exhibitor_about",
             @"exhibitor_about_cn" : @"exhibitor_about_cn",
             @"exhibitor_category" : @"exhibitor_category",
             @"exhibitor_booth_location" : @"exhibitor_booth_location"
             };
}

#pragma mark - Managed object serialization
+ (NSString *)managedObjectEntityName {
    return @"Exhibitor";
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    return @{
             @"exhibitor_id" : @"exhibitor_id",
             @"exhibitor_type" : @"exhibitor_type",
             @"exhibitor_name" : @"exhibitor_name",
             @"exhibitor_name_cn" : @"exhibitor_name_cn",
             @"exhibitor_logo" : @"exhibitor_logo",
             @"exhibitor_booth" : @"exhibitor_booth",
             @"exhibitor_contact" : @"exhibitor_contact",
             @"exhibitor_contact_cn" : @"exhibitor_contact_cn",
             @"exhibitor_about" : @"exhibitor_about",
             @"exhibitor_about_cn" : @"exhibitor_about_cn",
             @"exhibitor_category" : @"exhibitor_category",
             @"exhibitor_booth_location" : @"exhibitor_booth_location"
             };
}

+ (NSSet *)propertyKeysForManagedObjectUniquing {
    return [NSSet setWithObject:@"exhibitor_id"];
}

+ (void) deleteAllWithContext: (NSManagedObjectContext *) context {
    NSFetchRequest * fectchRequest = [[NSFetchRequest alloc] init];
    [fectchRequest setEntity:[NSEntityDescription entityForName:@"Exhibitor" inManagedObjectContext:context]];
    [fectchRequest setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * objArr = [context executeFetchRequest:fectchRequest error:&error];
    //error handling goes here
    for (NSManagedObject * managedObj in objArr) {
        [context deleteObject:managedObj];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //more error handling here
}


@end

