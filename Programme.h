//
//  SCCCI.h
//  SCCCI
//
//  Created by Zayar on 7/13/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"
@interface Programme : MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (nonatomic, retain) NSNumber * day_id;
@property (nonatomic, retain) NSString * end_time;
@property (nonatomic, retain) NSString * event_description_cn;
@property (nonatomic, retain) NSNumber * event_id;
@property (nonatomic, retain) NSString * event_name_cn;
@property (nonatomic, retain) NSString * event_name;
@property (nonatomic, retain) NSString * event_language;
@property (nonatomic, retain) NSString * event_location;
@property (nonatomic, retain) NSString * start_time;
@property (nonatomic, retain) NSString * time_name;
@property (nonatomic, retain) NSString * event_description;
@property (nonatomic, retain) NSNumber * is_fav;
@property (nonatomic, retain) NSDate * start_time_stamp;
+ (void) deleteAllWithContext: (NSManagedObjectContext *) context;
@end
