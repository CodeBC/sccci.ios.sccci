//
//  SCCCI.m
//  SCCCI
//
//  Created by Zayar on 7/13/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "Programme.h"
@implementation Programme

#pragma mark - JSON serialization
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"day_id" : @"day_id",
             @"end_time" : @"end_time",
             @"event_description_cn" : @"event_description_cn",
             @"event_description" : @"event_description",
             @"event_id" : @"event_id",
             @"event_name_cn" : @"event_name_cn",
             @"event_name" : @"event_name",
             @"event_language" : @"event_language",
             @"event_location" : @"event_location",
             @"start_time" : @"start_time",
             @"time_name" : @"time_name",
             @"start_time_stamp": @"start_time_stamp"
             };
}

#pragma mark - Managed object serialization
+ (NSString *)managedObjectEntityName {
    return @"Programme";
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    return @{
             @"day_id" : @"day_id",
             @"end_time" : @"end_time",
             @"event_description_cn" : @"event_description_cn",
             @"event_description" : @"event_description",
             @"event_id" : @"event_id",
             @"event_name_cn" : @"event_name_cn",
             @"event_name" : @"event_name",
             @"event_language" : @"event_language",
             @"event_location" : @"event_location",
             @"start_time" : @"start_time",
             @"time_name" : @"time_name",
              @"start_time_stamp": @"start_time_stamp"
             };
}

+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
    return dateFormatter;
}

+ (NSValueTransformer *)start_time_stampJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^(NSString *str) {
        
        NSString * strDate = [str stringByReplacingOccurrencesOfString:@".000Z" withString:@""];
        NSLog(@"date %@  and string date %@",[self.dateFormatter dateFromString:strDate],strDate);
        return [self.dateFormatter dateFromString:strDate];
    } reverseBlock:^(NSDate *date) {
        return [self.dateFormatter stringFromDate:date];
    }];
}

+ (NSSet *)propertyKeysForManagedObjectUniquing {
    return [NSSet setWithObject:@"event_id"];
}

+ (void) deleteAllWithContext: (NSManagedObjectContext *) context {
    NSFetchRequest * fectchRequest = [[NSFetchRequest alloc] init];
    [fectchRequest setEntity:[NSEntityDescription entityForName:@"Programme" inManagedObjectContext:context]];
    [fectchRequest setIncludesPropertyValues:NO];
    
    NSError * error = nil;
    NSArray * objArr = [context executeFetchRequest:fectchRequest error:&error];
    //error handling goes here
    for (NSManagedObject * managedObj in objArr) {
        [context deleteObject:managedObj];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    //more error handling here
}

@end
