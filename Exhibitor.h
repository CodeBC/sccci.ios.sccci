//
//  SCCCI.h
//  SCCCI
//
//  Created by Zayar on 7/13/14.
//  Copyright (c) 2014 nex. All rights reserved.
//

#import "MTLModel.h"
@interface Exhibitor : MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (nonatomic, retain) NSString * exhibitor_about;
@property (nonatomic, retain) NSString * exhibitor_about_cn;
@property (nonatomic, retain) NSString * exhibitor_booth;
@property (nonatomic, retain) NSString * exhibitor_booth_location;
@property (nonatomic, retain) NSString * exhibitor_contact;
@property (nonatomic, retain) NSString * exhibitor_contact_cn;
@property (nonatomic, retain) NSNumber * exhibitor_id;
@property (nonatomic, retain) NSString * exhibitor_logo;
@property (nonatomic, retain) NSString * exhibitor_name;
@property (nonatomic, retain) NSString * exhibitor_name_cn;
@property (nonatomic, retain) NSString * exhibitor_type;
@property (nonatomic, retain) NSString * exhibitor_category;

@end
